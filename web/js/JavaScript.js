(function ($) {
    $(function () {
        $("[data-toggle='tooltip']").tooltip();
        $("[data-toggle='popover']").popover();
        $("#dogovor-date_public").mask("99.99.9999", {placeholder: "дд.мм.гггг"});
        $("#dogovor-date_begin").mask("99.99.9999", {placeholder: "дд.мм.гггг"});
        $("#dogovor-date_end").mask("99.99.9999", {placeholder: "дд.мм.гггг"});
        $("#schools-date").mask("99.99.9999", {placeholder: "дд.мм.гггг"});
        $("#schools-date_create").mask("99.99.9999", {placeholder: "дд.мм.гггг"});
        $("#projects-data_doc").mask("99.99.9999", {placeholder: "дд.мм.гггг"});
        $("#projects-data_start_project").mask("99.99.9999", {placeholder: "дд.мм.гггг"});
        $("#projects-data_end_project").mask("99.99.9999", {placeholder: "дд.мм.гггг"});
        $("#otcc-mac_address").mask("AA-AA-AA-AA-AA-AA");
        $("#fisotcc-mac_address").mask("AA-AA-AA-AA-AA-AA");
        $("#schools-inn").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message
                // $("#errmsg").html("Digits Only").show().fadeOut("slow");
                return false;
            }
        });
        $("#fisschools-inn").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message
                // $("#errmsg").html("Digits Only").show().fadeOut("slow");
                return false;
            }
        });
        if ($("#data").val()) {
            var json_data = $("#data").val();
            var obj = jQuery.parseJSON(json_data);
            $(obj).each(function (i, val) {
                $.each(val, function (k, v) {
                    $("#otcc-data-" + i + "-" + k + "").val(v);
                    $("#fisotcc-data-" + i + "-" + k + "").val(v);
                    $("#vtcc-data-" + i + "-" + k + "").val(v);
                });
            });
        }
        if ($("#software").val()) {
            var json_data = $("#software").val();
            var obj = jQuery.parseJSON(json_data);
            $(obj).each(function (i, val) {
                $.each(val, function (k, v) {
                    $("#otcc-data_software-" + i + "-" + k + "").val(v);
                    $("#fisotcc-data_software-" + i + "-" + k + "").val(v);
                });
            });
        }
        if ($("#iis").val()) {
            var json_data = $("#iis").val();
            var obj = jQuery.parseJSON(json_data);
            $(obj).each(function (i, val) {
                $.each(val, function (k, v) {
                    $("#otcc-data_iis-" + i + "-" + k + "").val(v);
                    $("#fisotcc-data_iis-" + i + "-" + k + "").val(v);
                });
            });
        }
        if ($("#os").val()) {
            console.log($("#os").val());
            var json_data = $("#os").val();
            var obj = jQuery.parseJSON(json_data);
            var count = 1;
            $(obj).each(function (i, val) {
                $.each(val, function (k, v) {
                    $("#os-" + count + "").val(v);
                    count++;
                });
            });
        }
        $('#btn_next').click(function () {
            $('#next').val("1");
        });
        $('#btn_stop').click(function () {
            $('#next').val("2");
        });
        var availableTags = [
            "Windows 10 Professional x32",
            "Windows 10 Professional x64",
            "Windows 10 Home Single Language x32",
            "Windows 10 Home Single Language x64",
            "Windows 10 Home x32",
            "Windows 10 Home x64",
            "Windows 10 Enterprise x32",
            "Windows 10 Enterprise x64",
            "Windows 8.1 Professional x32",
            "Windows 8.1 Professional x64",
            "Windows 8.1 Single Language x32",
            "Windows 8.1 Single Language x64",
            "Windows 8.1 Enterprise x32",
            "Windows 8.1 Enterprise x64",
            "Windows 8 Professional x32",
            "Windows 8 Professional x64",
            "Windows 8 Single Language x32",
            "Windows 8 Single Language x64",
            "Windows 8 Enterprise x32",
            "Windows 8 Enterprise x64",
            "Windows 7 Ultimate x32",
            "Windows 7 Ultimate x64",
            "Windows 7 Professional x32",
            "Windows 7 Professional x64",
            "Windows 7 Home Basic x32",
            "Windows 7 Home Basic x64",
            "Windows 7 Home Premium x32",
            "Windows 7 Home Premium x64",
            "Windows 7 Enterprise x32",
            "Windows 7 Enterprise x64",
            "Windows 7 Starter x32",
            "Windows 7 Starter x64",
            "Windows Vista Starter x32",
            "Windows Vista Starter x64",
            "Windows Vista Home Basic x32",
            "Windows Vista Home Basic x64",
            "Windows Vista Home Premium x32",
            "Windows Vista Home Premium x64",
            "Windows Vista Enterprise x32",
            "Windows Vista Enterprise x64",
            "Windows Vista Ultimate x32",
            "Windows Vista Ultimate x64",
            "Windows XP Starter Edition x32",
            "Windows XP Starter Edition x64",
            "Windows XP Professional Edition x32",
            "Windows XP Professional Edition x64",
            "Windows XP Home Edition x32",
            "Windows XP Home Edition x64"
        ];

        // if($("#otcc-data_software-0-type_soft").val()==1){
        //     $("#otcc-data_software-0-mark_number").attr('type', 'hidden');
        //     $("#otcc-data_software-0-mark_number").val("0");
        // }
        //
        // $("#otcc-data_software-0-type_soft").click(function () {
        //     console.log($(this).val());
        //     if($("#otcc-data_software-0-type_soft").val()==1){
        //         $("#otcc-data_software-0-mark_number").attr('type', 'hidden');
        //         $("#otcc-data_software-0-mark_number").val("0");
        //     } else {
        //         $("#otcc-data_software-0-mark_number").prop('type', null);
        //         $("#otcc-data_software-0-mark_number").val("");
        //     }
        // });
        //
        // if($("#vtcc-data_software-0-type_soft").val()==1){
        //     $("#vtcc-data_software-0-mark_number").attr('type', 'hidden');
        //     $("#vtcc-data_software-0-mark_number").val("0");
        // }
        //
        // $("#vtcc-data_software-0-type_soft").click(function () {
        //     console.log($(this).val());
        //     if($("#vtcc-data_software-0-type_soft").val()==1){
        //         $("#vtcc-data_software-0-mark_number").attr('type', 'hidden');
        //         $("#vtcc-data_software-0-mark_number").val("0");
        //     } else {
        //         $("#vtcc-data_software-0-mark_number").prop('type', null);
        //         $("#vtcc-data_software-0-mark_number").val("");
        //     }
        // });

        $("#os-1").autocomplete({
            source: availableTags
        });
        $("#vtcc-data_software-0-name").autocomplete({
            source: availableTags
        });

        // removeMe = function (id) {
        //
        //     if (i > 2) {
        //         $('#myInput_' + id).parents('p').remove();
        //
        //         i--;
        //     }
        //     return false;
        // }

        // $('#addInput').click(function() {
        //
        //     // $('<div class="form-group field-vtcc-pc_name required">'+
        //     //     '<label class="control-label" for="vtcc-pc_name">Сетевое имя компьютера</label>'+
        //     // '<input id="vtcc-pc_name" class="form-control" name="Vtcc[pc_name][' + i + ']" aria-required="true" type="text">'+
        //     //     '<div class="help-block"></div>'+
        //     //     '</div>'+
        //     // '<div class="form-group field-vtcc-pc_type required">'+
        //     //     '<label class="control-label" for="vtcc-pc_type">Тип АРМ</label>'+
        //     // '<select id="vtcc-pc_type" class="form-control" name="Vtcc[pc_type]" aria-required="true">'+
        //     //     '<option value="0">ПК</option>'+
        //     //     '<option value="1">Ноутбук</option>'+
        //     //     '<option value="2">Моноблок</option>'+
        //     //     '</select>'+
        //     //
        //     //     '<div class="help-block"></div>'+
        //     //     '</div>'+
        //     // '<div class="form-group field-vtcc-data required">\n' +
        //     //     '<label class="control-label" for="vtcc-data">Данные</label>\n' +
        //     //     '<input name="Vtcc[data]" type="hidden"><div id="w'+2+'" class="multiple-input"><table class="multiple-input-list table table-condensed table-renderer"><thead><tr><th class="list-cell__peripheral">Переферийное устройство</th>\n' +
        //     //     '<th class="list-cell__model">Модель</th>\n' +
        //     //     '<th class="list-cell__serial_number">Заводской номер</th>\n' +
        //     //     '<th class="list-cell__button"></th></tr></thead>\n' +
        //     //     '<tbody><tr class="multiple-input-list__item"><td class="list-cell__peripheral"><div class="form-group field-vtcc-data-'+i+'-peripheral"><select id="vtcc-data-'+i+'-peripheral" class="form-control" name="Vtcc[data]['+i+'][peripheral]">\n' +
        //     //     '<option value="1" selected="">Монитор</option>\n' +
        //     //     '<option value="2">Клавиатура</option>\n' +
        //     //     '<option value="3">Манипулятор «мышь»</option>\n' +
        //     //     '<option value="4">Системный блок</option>\n' +
        //     //     '<option value="5">Ноутбук</option>\n' +
        //     //     '<option value="6">Моноблок</option>\n' +
        //     //     '<option value="7">МФУ</option>\n' +
        //     //     '<option value="8">Сканер</option>\n' +
        //     //     '<option value="9">Принтер</option>\n' +
        //     //     '<option value="10">Телефон</option>\n' +
        //     //     '</select></div></td>\n' +
        //     //     '<td class="list-cell__model"><div class="form-group field-vtcc-data-'+i+'-model"><input id="vtcc-data-'+i+'-model" class="input-priority form-control" name="Vtcc[data]['+i+'][model]" type="text">\n' +
        //     //     '<div class="help-block help-block-error"></div></div></td>\n' +
        //     //     '<td class="list-cell__serial_number"><div class="form-group field-vtcc-data-'+i+'-serial_number"><input id="vtcc-data-'+i+'-serial_number" class="input-priority form-control" name="Vtcc[data]['+i+'][serial_number]" type="text">\n' +
        //     //     '<div class="help-block help-block-error"></div></div></td>\n' +
        //     //     '<td class="list-cell__button"><div class="btn multiple-input-list__btn js-input-plus btn btn-default"><i class="glyphicon glyphicon-plus"></i></div></td></tr></tbody>\n' +
        //     //     '</table></div>\n' +
        //     //     '\n' +
        //     //     '<div class="help-block"></div>\n' +
        //     //     '</div>').appendTo(myDiv);
        //
        //     // $('div.vtcc-form').append('<div class="form-group field-vtcc-pc_name required">'+
        //     //     '<label class="control-label" for="vtcc-pc_name">Сетевое имя компьютера</label>'+
        //     //     '<input id="vtcc-pc_name" class="form-control" name="Vtcc[pc_name][' + i + ']" aria-required="true" type="text">'+
        //     //     '<div class="help-block"></div>'+
        //     //     '</div>'+
        //     //     '<div class="form-group field-vtcc-pc_type required">'+
        //     //     '<label class="control-label" for="vtcc-pc_type">Тип АРМ</label>'+
        //     //     '<select id="vtcc-pc_type" class="form-control" name="Vtcc[pc_type]" aria-required="true">'+
        //     //     '<option value="0">ПК</option>'+
        //     //     '<option value="1">Ноутбук</option>'+
        //     //     '<option value="2">Моноблок</option>'+
        //     //     '</select>'+
        //     //
        //     //     '<div class="help-block"></div>'+
        //     //     '</div>'+
        //     //     '<div class="form-group field-vtcc-data required">\n' +
        //     //     '<label class="control-label" for="vtcc-data">Данные</label>\n' +
        //     //     '<input name="Vtcc[data]" type="hidden"><div id="w'+2+'" class="multiple-input"><table class="multiple-input-list table table-condensed table-renderer"><thead><tr><th class="list-cell__peripheral">Переферийное устройство</th>\n' +
        //     //     '<th class="list-cell__model">Модель</th>\n' +
        //     //     '<th class="list-cell__serial_number">Заводской номер</th>\n' +
        //     //     '<th class="list-cell__button"></th></tr></thead>\n' +
        //     //     '<tbody><tr class="multiple-input-list__item"><td class="list-cell__peripheral"><div class="form-group field-vtcc-data-'+i+'-peripheral"><select id="vtcc-data-'+i+'-peripheral" class="form-control" name="Vtcc[data]['+i+'][peripheral]">\n' +
        //     //     '<option value="1" selected="">Монитор</option>\n' +
        //     //     '<option value="2">Клавиатура</option>\n' +
        //     //     '<option value="3">Манипулятор «мышь»</option>\n' +
        //     //     '<option value="4">Системный блок</option>\n' +
        //     //     '<option value="5">Ноутбук</option>\n' +
        //     //     '<option value="6">Моноблок</option>\n' +
        //     //     '<option value="7">МФУ</option>\n' +
        //     //     '<option value="8">Сканер</option>\n' +
        //     //     '<option value="9">Принтер</option>\n' +
        //     //     '<option value="10">Телефон</option>\n' +
        //     //     '</select></div></td>\n' +
        //     //     '<td class="list-cell__model"><div class="form-group field-vtcc-data-'+i+'-model"><input id="vtcc-data-'+i+'-model" class="input-priority form-control" name="Vtcc[data]['+i+'][model]" type="text">\n' +
        //     //     '<div class="help-block help-block-error"></div></div></td>\n' +
        //     //     '<td class="list-cell__serial_number"><div class="form-group field-vtcc-data-'+i+'-serial_number"><input id="vtcc-data-'+i+'-serial_number" class="input-priority form-control" name="Vtcc[data]['+i+'][serial_number]" type="text">\n' +
        //     //     '<div class="help-block help-block-error"></div></div></td>\n' +
        //     //     '<td class="list-cell__button"><div class="btn multiple-input-list__btn js-input-plus btn btn-default"><i class="glyphicon glyphicon-plus"></i></div></td></tr></tbody>\n' +
        //     //     '</table></div>\n' +
        //     //     '\n' +
        //     //     '<div class="help-block"></div>\n' +
        //     //     '</div>');
        //
        //     // $('<?php $form->field($model, "pc_type")->textInput() ?>').appendTo(myDiv);
        //     // $('#myDiv').append(result);
        //     // console.log(result);
        //     // $('<p>' +
        //     //     '<label for="myInput">' +
        //     //     '<input type="text" id="myInput_' + i +'" size="20" name="myInput' + i +'" value="" placeholder="Input Value" />' +
        //     //
        //     //     '<span href="#" id="remInput"  onclick="removeMe('+ i +')">Remove</span>'+'</label> ').appendTo(myDiv);
        //
        //     // var result = '<?php php_func(); ?>';
        //     // console.log(result);
        //     // $('#myDiv').append(result);
        //     i++;
        //     return false;
        // });
        // $('#addInput').click(function() {
        //     console.log("yes click");
        //     $.ajax({
        //         url: "http://192.168.10.94/vtcc/create",
        //         type: "POST",
        //         data: "php_func",
        //         dataType: "JSON",
        //         success: function (data) {
        //             console.log(data);
        //             // $("#directioninfo").append(data);
        //         }
        //     });
        // });
    });
})(jQuery);