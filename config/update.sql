CREATE TABLE `doc_schools` (
  `id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name_org` varchar(255) NOT NULL,
  `name_org_rod` varchar(255) NULL,
  `short_name_org` varchar(255) NOT NULL,
  `school_director` varchar(255) NOT NULL,
  `position_director` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `number_protokol` varchar(255)  NULL,
  `number_techpassport` varchar(255)  NULL,
  `number_certificate` varchar(255)  NULL,
  `number_finale` varchar(255)  NULL,
  `spec` varchar(255) NOT NULL,
  `spec_fio` varchar(255) NOT NULL,
  `spec_nord_id` int(10) NULL,
  `project_id` int NULL,
  `user_id` int NULL
);

CREATE TABLE `doc_nord` (
  `id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `fio` varchar(255) NOT NULL,
  `spec` varchar(255) NOT NULL
);

CREATE TABLE `doc_security` (
  `id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(255) NOT NULL,
  `certificate` varchar(1000) NOT NULL,
  `type` int NOT NULL
);

CREATE TABLE `doc_peripheral` (
  `id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(255) NOT NULL
);

CREATE TABLE `doc_software` (
  `id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(255) NOT NULL
);

CREATE TABLE `doc_dogovor` (
  `id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `date_public` date NOT NULL,
  `full_name_org` varchar(255) NOT NULL,
  `position_id` int(10) NULL,
  `fio` varchar(255) NULL,
  `osnovanie` varchar(255) NOT NULL,
  `date_begin` date NOT NULL,
  `date_end` date NOT NULL,
  `short_name_org` varchar(255) NOT NULL,
  `inn` int(15) NOT NULL,
  `kpp` int(30) NOT NULL,
  `address_org` varchar(255) NOT NULL,
  `pc` int(50) NOT NULL,
  `bank` varchar(255) NOT NULL,
  `kc` int(50) NULL,
  `bik` int(50) NOT NULL,
  `tel` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
);

CREATE TABLE `doc_nord_dogovor` (
  `id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `fio` varchar(255) NOT NULL,
  `spec` varchar(255) NOT NULL,
  `note` varchar(255) NOT NULL
);

ALTER TABLE `doc_dogovor` ADD `spec_nord` int NOT NULL;

ALTER TABLE `doc_dogovor` ADD `ending_name_org` int NOT NULL AFTER `full_name_org`, ADD `ending_fio` int NULL AFTER `fio`;

ALTER TABLE `doc_dogovor` ADD `dogovor_id` int NOT NULL AFTER `id`;

CREATE TABLE `doc_vtcc` (
  `id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `org_id` int(10) NULL,
  `pc_name` BLOB NOT NULL,
  `pc_type` BLOB NOT NULL,
  `number_room` varchar(255) NOT NULL,
  `data_software` BLOB NOT NULL,
  `data` BLOB NOT NULL,
  `user_id` int NULL
);

CREATE TABLE `doc_otcc` (
  `id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `org_id` int(10) NULL,
  `pc_name` varchar(255) NOT NULL,
  `pc_type` int(10) NOT NULL,
  `number_room` varchar(255) NOT NULL,
  `data_software` BLOB NOT NULL,
  `data` BLOB NOT NULL,
  `data_iis` BLOB NOT NULL,
  `user_id` int NULL
);

CREATE TABLE `doc_operation_system` (
  `id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(255) NOT NULL
);

CREATE TABLE `doc_projects` (
  `id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `ministr_name` varchar(255) NOT NULL,
  `address_name` varchar(255) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `short_name` varchar(255) NOT NULL,
  `name_doc` varchar(255) NOT NULL,
  `date_doc` date NOT NULL,
  `date_start_project` date NOT NULL,
  `date_end_project` date NOT NULL
);

CREATE TABLE `doc_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) unsigned NOT NULL,
  `username` varchar(255) NOT NULL,
  `role_code` int(10) unsigned NOT NULL,
  `org_id` int(10) unsigned DEFAULT NULL,
  `hash` varchar(255) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `force_logout` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `note` varchar(255) NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `auth_key` (`auth_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `doc_otcc` ADD `data_iis` BLOB NOT NULL;

ALTER TABLE `doc_otcc` ADD `ip_address` varchar(255) NULL, ADD `mac_address` varchar(255) NULL;

ALTER TABLE `doc_schools` ADD `region_id` int(5) NULL AFTER `id`;

#29-03-2018
ALTER TABLE `doc_schools` ADD `inn` varchar(255) NULL;
#25-04-2018
ALTER TABLE `doc_otcc` ADD `os` BLOB NULL;

#10-05-2018
CREATE TABLE `fis_schools` (
  `id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `region_id` int(5) NULL,
  `name_org` varchar(255) NOT NULL,
  `name_org_rod` varchar(255) NULL,
  `short_name_org` varchar(255) NOT NULL,
  `inn` varchar(255) NULL,
  `school_director` varchar(255) NOT NULL,
  `position_director` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `number_protokol` varchar(255)  NULL,
  `number_techpassport` varchar(255)  NULL,
  `number_certificate` varchar(255)  NULL,
  `number_finale` varchar(255)  NULL,
  `spec` varchar(255) NOT NULL,
  `spec_fio` varchar(255) NOT NULL,
  `project_id` int NULL,
  `user_id` int NULL
);

ALTER TABLE `doc_schools` ADD `date` date NULL, ADD `date_create` date NULL;

ALTER TABLE `fis_schools` ADD `number_contract` varchar(255) NULL, ADD `date_contract` date NULL;

ALTER TABLE `fis_schools` ADD `date` date NULL, ADD `date_create` date NULL;

CREATE TABLE `fis_otcc` (
  `id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `org_id` int(10) NULL,
  `pc_name` varchar(255) NOT NULL,
  `pc_type` int(10) NOT NULL,
  `number_room` varchar(255) NOT NULL,
  `ip_address` varchar(255) NULL,
  `mac_address` varchar(255) NULL,
  `data_software` BLOB NOT NULL,
  `data` BLOB NOT NULL,
  `data_iis` BLOB NOT NULL,
  `os` BLOB NULL,
  `user_id` int NULL
);

ALTER TABLE `fis_schools` ADD `note` varchar(500) NULL;

ALTER TABLE `fis_otcc` ADD `note` varchar(500) NULL;

ALTER TABLE `doc_schools` ADD `note` varchar(500) NULL;

ALTER TABLE `doc_otcc` ADD `note` varchar(500) NULL;

ALTER TABLE `doc_schools` ADD `plan_room` Boolean NULL, ADD `plan_building` Boolean NULL;

ALTER TABLE `fis_schools` ADD `plan_room` Boolean NULL, ADD `plan_building` Boolean NULL;

ALTER TABLE `fis_schools` ADD `reg_number` varchar(255) NULL, ADD `date_certificate_vipnet` date NULL;