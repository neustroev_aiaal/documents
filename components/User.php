<?php

namespace app\components;

use app\models\Log;
use Yii;

/**
 * @inheritdoc
 *
 * @property \app\models\User|\yii\web\IdentityInterface|null $identity The identity object associated with the currently logged-in user. null is returned if the user is not logged in (not authenticated).
 */
class User extends \yii\web\User {

    protected function beforeLogin($identity, $cookieBased, $duration) {
        if (parent::beforeLogin($identity, $cookieBased, $duration)) {
            Log::addLogin($identity, Log::TYPE_LOGIN);
            return true;
        } else {
            return false;
        }
    }

    protected function afterLogout($identity) {
        parent::afterLogout($identity);
        Log::addLogin($identity, Log::TYPE_LOGOUT);
    }

}