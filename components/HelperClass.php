<?php
/**
 * Created by PhpStorm.
 * User: user3
 * Date: 05.03.2018
 * Time: 12:16
 */

namespace app\components;


class HelperClass
{
    public static $OPENSSL_KEY = "dde3867a36f88c93a0c3f7c9bc9ab6";

    public function encryptModel($model){
        foreach ($model as $key=>$item) {
//            var_dump($model);
//            die;
//            var_dump(gettype($key));
//            var_dump(strpos($key,"id"));
//            die;
            if(strpos($key,"id") == 0) {
                $model[$key] = HelperClass::encrypt($item);
            }
//            else {
////                var_dump($model);
////                die;
//                $model[$key] = $item;
//            }
        }
        var_dump($model);
        die;
        return $model;
    }

    public function decryptModel($model){
        foreach ($model as $key=>$item) {
            if($key!="id") {
                $model[$key] = HelperClass::decrypt($item);
            } else {
                $model[$key] = $item;
            }
        }
        return $model;
    }

    private function encrypt($plaintext)
    {
        $key = HelperClass::$OPENSSL_KEY;
        $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
        $iv = openssl_random_pseudo_bytes($ivlen);
        $ciphertext_raw = openssl_encrypt($plaintext, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
        $hmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
        $ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );

        return $ciphertext;
    }

    private function decrypt($ciphertext)
    {
        $key = HelperClass::$OPENSSL_KEY;
        $c = base64_decode($ciphertext);
        $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
        $iv = substr($c, 0, $ivlen);
        $hmac = substr($c, $ivlen, $sha2len=32);
        $ciphertext_raw = substr($c, $ivlen+$sha2len);
        $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
        $calcmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
        $text = "False";
        if (hash_equals($hmac, $calcmac))//с PHP 5.6+ сравнение, не подверженное атаке по времени
        {
            $text = $original_plaintext;
        }

        return $text;
    }

}