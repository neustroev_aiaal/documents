<?php
namespace app\components;

use Yii;

class Y {

    /**
     * @return \WebApplication
     */
    public static function app() {
        return Yii::$app;
    }

    /**
     * @return \yii\web\Request
     */
    public static function request() {
        return Yii::$app->request;
    }

    /**
     * @return \yii\web\Response
     */
    public static function response() {
        return Yii::$app->response;
    }

    /**
     * @return \yii\web\User
     */
    public static function webUser() {
        return Yii::$app->user;
    }

    /**
     * @return \app\models\User|null
     */
    public static function user() {
        return Yii::$app->user->identity;
    }

    /**
     * @return array
     */
    public static function params() {
        return Yii::$app->params;
    }

    /**
     * @return \yii\i18n\Formatter
     */
    public static function formatter() {
        return Yii::$app->formatter;
    }

}