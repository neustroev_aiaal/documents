<?php
return [
    'user' => [
        'type' => 1,
        'ruleName' => 'userRoleCode',
    ],
    'admin' => [
        'type' => 1,
        'ruleName' => 'userRoleCode',
    ],
    'operator' => [
        'type' => 1,
        'ruleName' => 'userRoleCode',
    ],
    'manager' => [
        'type' => 1,
        'ruleName' => 'userRoleCode',
    ],
];
