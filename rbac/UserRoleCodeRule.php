<?php

namespace app\rbac;

use app\models\User;
use yii\rbac\Rule;

class UserRoleCodeRule extends Rule {

    public $name = 'userRoleCode';

    public function execute($user, $item, $params) {

        if (!\Yii::$app->user->isGuest) {
            $code = \Yii::$app->user->identity->role_code;
            $requiredCode = User::$ROLES_CODES[$item->name];
            return $code >= $requiredCode;
//            var_dump($code);
//            var_dump($requiredCode);
//            die();

//            if ($item->name === 'admin') {
//                return $group == 'admin';
//            } elseif ($item->name === 'BRAND') {
//                return $group == 'admin' || $group == 'BRAND';
//            } elseif ($item->name === 'TALENT') {
//                return $group == 'admin' || $group == 'TALENT';
//            }
//            return $item->name == $role;
        }
        //return true;
//        $user = ArrayHelper::getValue($params, 'user', User::findOne($user));
//        if ($user) {
//            $role = $user->role; //Значение из поля role базы данных
//            var_dump($item);
//            var_dump($user);
//            die('1');
//            switch ($item->name) {
//                case 'admin':
////                    $r = $role == User::ROLE_ADMIN;
//                    break;
//                case 'manager':
////                    $r = $role == User::ROLE_MANAGER || $role == User::ROLE_ADMIN;
//                    break;
//                case 'operator':
//                    $r = $role == User::ROLE_MANAGER || $role == User::ROLE_ADMIN;
//                    break;
//                case 'viewer':
//                    $r = $role == User::ROLE_MANAGER || $role == User::ROLE_ADMIN;
//                    break;
//                case 'user':
//                    $r = $role == User::ROLE_MANAGER || $role == User::ROLE_ADMIN;
//                    break;
//            }
//        }
        return false;
    }


}