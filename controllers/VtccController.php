<?php

namespace app\controllers;

use app\models\Log;
use app\models\Schools;
use Yii;
use app\models\Vtcc;
use app\models\VtccSearch;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
 * VtccController implements the CRUD actions for Vtcc model.
 */
class VtccController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Vtcc models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VtccSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $user_id = \Yii::$app->user->id;

        if(\Yii::$app->user->can("admin")) {
            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel
            ]);
        } else {
            return $this->render('index', [
                'dataProvider' => new ActiveDataProvider([
                    'query' => Vtcc::find()->where(['user_id'=>$user_id]),
                ]),
                'searchModel' => $searchModel
            ]);
        }
    }

    /**
     * Displays a single Vtcc model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Vtcc model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $user_id = \Yii::$app->user->id;
        $model = new Vtcc();
        if(!Schools::canBeEdited(\app\components\Y::user())) {
            if (isset($_GET['id'])) {
                $schools = Schools::get($_GET['id']);
                $model->org_id = $schools->id;
            } else {
                $schools = Schools::find()->where(['user_id' => $user_id])->one();
                if ($schools != null) {
                    $model->org_id = $schools->id;
                } else {
                    $this->accessDenied();
                }
            }
        }
        $model->user_id = $user_id;
        if ($model->load(Yii::$app->request->post())) {
            $post_data = Yii::$app->request->post('Vtcc')['data'];
            $post_data_software = Yii::$app->request->post('Vtcc')['data_software'];
            $array = array();
            $array_soft = array();
            foreach ($post_data as $item){
                array_push($array, $item);
            }
            foreach ($post_data_software as $item){
                array_push($array_soft, $item);
            }
            $encode = json_encode($array);
            $encode_soft=JSON::encode($array_soft);
            $model->data = $encode;
            $model->data_software = $encode_soft;
            $model->save();
            Log::addVtcc($model, true);
            if(Yii::$app->request->post('next')=="1"){
                return $this->redirect(Url::to(['schools/view?id='.$model->org_id.''], true));
            }
            if(Yii::$app->request->post('save_and_add')=="1"){
                return $this->redirect('create');
            }else {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

//    function php_func(){
//        $model = new Vtcc();
//        $form = ActiveForm::begin();
//        $json_data = \yii\helpers\Json::encode($form->field($model, 'pc_type')->textInput());
//        echo $json_data;
//    }

    /**
     * Updates an existing Vtcc model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $data = $model->data;
        $model->data = json_decode($model->data);
        echo '<input id="data" value='.str_replace(" ", "&nbsp;", $data).' type="hidden">';
        if ($model->load(Yii::$app->request->post())) {
            $post_data = Yii::$app->request->post('Vtcc')['data'];
            $post_data_software = Yii::$app->request->post('Vtcc')['data_software'];
            $array = array();
            $array_soft = array();
            foreach ($post_data as $item){
                array_push($array, $item);
            }
            foreach ($post_data_software as $item){
                array_push($array_soft, $item);
            }
            $encode = json_encode($array);
            $encode_soft=JSON::encode($array_soft);
            $model->data = $encode;
            $model->data_software = $encode_soft;
            $model->save();
            Log::addVtcc($model, false);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Vtcc model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Vtcc model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Vtcc the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Vtcc::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    private function accessDenied() {
        throw new HttpException(403, 'Вы не добавили данные о школы');
    }
}
