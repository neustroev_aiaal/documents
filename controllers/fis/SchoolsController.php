<?php

namespace app\controllers\fis;

use app\models\fis\FisOtcc;
use app\models\Log;
use app\models\Projects;
use app\models\Schools;
use app\models\User;
use PhpOffice\PhpWord\TemplateProcessor;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Yii;
use app\models\fis\FisSchools;
use app\models\fis\FisSchoolsSearch;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use ZipArchive;

/**
 * SchoolsController implements the CRUD actions for FisSchools model.
 */
class SchoolsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FisSchools models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FisSchoolsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FisSchools model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FisSchools model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FisSchools();
        $model->user_id = \Yii::$app->user->id;
        $model->project_id = 1;
        $model->region_id = User::findOne(\Yii::$app->user->id)->region_id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Log::addFisSchools($model, true);
            if (Yii::$app->request->post('next') == "1") {
                return $this->redirect(Url::to(['/fis/otcc/create?id=' . $model->id], true));
            } else {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing FisSchools model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Log::addFisSchools($model, false);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionGenerate($id)
    {
        $model = $this->findModel($id);

        if (!is_dir($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org))) {
            mkdir($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org), 0777, true);
        }
//        if ($model->load(Yii::$app->request->post())) {
//            $model->save();
//            return $this->redirect(['generate-success', 'id' => $model->id]);
//        }
        return $this->render('generate', [
            'model' => $model,
        ]);
    }

    public function actionGenerateSuccess($id, $type)
    {
        $model = $this->findModel($id);
        return $this->render('success_generate', [
            'model' => $model, 'type' => $type,
        ]);
    }

    public function actionProtokol($id)
    {
        FisSchools::protokol($id);
        Log::generateFIS($this->findModel($id), "Протокол");
        return $this->redirect(['generate-success', 'id' => $id, 'type' => 1]);
    }

    public function actionPrikazy($id)
    {
        FisSchools::prikazy($id, true);
        Log::generateFIS($this->findModel($id), "Приказы");
        return $this->redirect(['generate-success', 'id' => $id, 'type' => 7]);
    }

    public function actionManual($id)
    {
        FisSchools::manuals($id, true);
        Log::generateFIS($this->findModel($id), "Инструкция");
        return $this->redirect(['generate-success', 'id' => $id, 'type' => 8]);
    }

    public function actionOtherFisDoc($id)
    {
        FisSchools::other_docs($id, true);
        Log::generateFIS($this->findModel($id), "Другие документы");
        return $this->redirect(['generate-success', 'id' => $id, 'type' => 9]);
    }

    public function actionTechPassport($id)
    {
        FisSchools::tech_passport($id);
        Log::generateFIS($this->findModel($id), "Тех паспорт");
        return $this->redirect(['generate-success', 'id' => $id, 'type' => 2]);
    }

    public function actionAkt($id)
    {
        FisSchools::akt($id);
        Log::generateFIS($this->findModel($id), "Акт установки");
        return $this->redirect(['generate-success', 'id' => $id, 'type' => 5]);
    }

    public function actionLicenseVipnet($id)
    {
        FisSchools::license_vipnet($id);
        Log::generateFIS($this->findModel($id), "Лицензии випнета");
        return $this->redirect(['generate-success', 'id' => $id, 'type' => 10]);
    }

    public function actionCertificate($id)
    {
        FisSchools::certificate($id);
        Log::generateFIS($this->findModel($id), "Аттестат");
        return $this->redirect(['generate-success', 'id' => $id, 'type' => 4]);
    }

    public function actionFinale($id)
    {
        FisSchools::finale($id);
        Log::generateFIS($this->findModel($id), "Заключение");
        return $this->redirect(['generate-success', 'id' => $id, 'type' => 3]);
    }

    public function actionZip($id)
    {
        FisSchools::tech_passport($id);
        FisSchools::protokol($id);
        FisSchools::certificate($id);
        FisSchools::finale($id);
        FisSchools::akt($id);
        FisSchools::prikazy($id, false);
        FisSchools::manuals($id, false);
        FisSchools::other_docs($id, false);
        FisSchools::license_vipnet($id);

        $model = $this->findModel($id);
        $rootPath = realpath($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "");
        $zip = new ZipArchive();
        $zip->open($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . ".zip", ZipArchive::CREATE | ZipArchive::OVERWRITE);

        // Create recursive directory iterator
        /** @var SplFileInfo[] $files */
        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($rootPath),
            RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach ($files as $name => $file) {
            // Skip directories (they would be added automatically)
            if (!$file->isDir()) {
                // Get real and relative path for current file
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($rootPath) + 1);

                // Add current file to archive
                $zip->addFile($filePath, $relativePath);
            }
        }

        // Zip archive will be created only after closing object
        $zip->close();
        Log::generateFIS($this->findModel($id), "Генерировал все в zip");
        return $this->redirect(['generate-success', 'id' => $id, 'type' => 6]);
    }

    /**
     * Deletes an existing FisSchools model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FisSchools model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return FisSchools the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FisSchools::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionDownload($id, $type)
    {
        $model = FisSchools::findOne($id);
        $path = Yii::getAlias('@webroot') . '/documents/fis/' . Schools::translit($model->short_name_org) . '/';
        switch ($type) {
            case 1:
                $type_name = 'protokol_';
                break;
            case 2:
                $type_name = 'tech_passport_';
                break;
            case 3:
                $type_name = 'finale_';
                break;
            case 4:
                $type_name = 'certificate_';
                break;
            case 5:
                $type_name = 'akt_';
                break;
            case 6:
                $file = Yii::getAlias('@webroot') . '/documents/fis/' . Schools::translit($model->short_name_org) . '.zip';
                header("Cache-Control: no-store, no-cache, must-revalidate");
                header("Cache-Control: post-check=0, pre-check=0", false);
                header("Pragma: no-cache");
                header("Content-type: application/zip");
                header('Content-length: ' . filesize($file));
                header("Content-disposition: attachment; filename=" . basename($file));
                readfile($file);
                break;
            case 7:
                $file = Yii::getAlias('@webroot') . '/documents/fis/' . Schools::translit($model->short_name_org) . '/prikazy.zip';
                header("Cache-Control: no-store, no-cache, must-revalidate");
                header("Cache-Control: post-check=0, pre-check=0", false);
                header("Pragma: no-cache");
                header("Content-type: application/zip");
                header('Content-length: ' . filesize($file));
                header("Content-disposition: attachment; filename=" . basename($file));
                readfile($file);
                break;
            case 8:
                $file = Yii::getAlias('@webroot') . '/documents/fis/' . Schools::translit($model->short_name_org) . '/manuals.zip';
                header("Cache-Control: no-store, no-cache, must-revalidate");
                header("Cache-Control: post-check=0, pre-check=0", false);
                header("Pragma: no-cache");
                header("Content-type: application/zip");
                header('Content-length: ' . filesize($file));
                header("Content-disposition: attachment; filename=" . basename($file));
                readfile($file);
                break;
            case 9:
                $file = Yii::getAlias('@webroot') . '/documents/fis/' . Schools::translit($model->short_name_org) . '/other_docs.zip';
                header("Cache-Control: no-store, no-cache, must-revalidate");
                header("Cache-Control: post-check=0, pre-check=0", false);
                header("Pragma: no-cache");
                header("Content-type: application/zip");
                header('Content-length: ' . filesize($file));
                header("Content-disposition: attachment; filename=" . basename($file));
                readfile($file);
                break;
            case 10:
                $type_name = 'license_vipnet_';
                break;
        }
        if ($type_name != null) {
            $file = $path . $type_name . Schools::translit($model->short_name_org) . '.docx';
            if (file_exists($file)) {
                Yii::$app->response->sendFile($file);
            }
        }
    }

    public function actionDocument()
    {
        $id = Yii::$app->request->post('id');
        $project = Projects::findOne($id);
        if ($project != null) {
            echo $project->name_doc;
        } else {
            echo "";
        }
    }

    function getDir()
    {
        return $_SERVER["DOCUMENT_ROOT"];
    }

}
