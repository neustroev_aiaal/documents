<?php

namespace app\controllers\fis;

use app\models\fis\FisSchools;
use app\models\Log;
use app\models\Schools;
use Yii;
use app\models\fis\FisOtcc;
use app\models\fis\FisOtccSearch;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OtccController implements the CRUD actions for FisOtcc model.
 */
class OtccController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FisOtcc models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FisOtccSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FisOtcc model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FisOtcc model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $user_id = \Yii::$app->user->id;
        $model = new FisOtcc();
        if(!Schools::canBeEdited(\app\components\Y::user())) {
            if (isset($_GET['id'])) {
                $schools = FisSchools::findOne($_GET['id']);
                $model->org_id = $schools->id;
            } else {
                $schools = FisSchools::find()->where(['user_id' => $user_id])->one();
                if ($schools != null) {
                    $model->org_id = $schools->id;
                } else {
                    $this->accessDenied();
                }
            }
        }
        $model->user_id = $user_id;
        if ($model->load(Yii::$app->request->post()) ) {
            $post_data = Yii::$app->request->post('FisOtcc')['data'];
            $post_data_software = Yii::$app->request->post('FisOtcc')['data_software'];
            $post_data_iis = Yii::$app->request->post('FisOtcc')['data_iis'];
            $os_name = Yii::$app->request->post('os_name');
            $os_version = Yii::$app->request->post('os_version');
            $os_build = Yii::$app->request->post('os_build');
            $os_serial = Yii::$app->request->post('os_serial');
            $array_os = array(['os_name'=>$os_name, 'os_version'=>$os_version, 'os_build'=>$os_build, 'os_serial'=>$os_serial]);
            $array_per = array();
            $array_soft = array();
            $array_iis = array();
            foreach ($post_data as $item){
                array_push($array_per, $item);
            }
            foreach ($post_data_software as $item){
                array_push($array_soft, $item);
            }
            foreach ($post_data_iis as $item){
                array_push($array_iis, $item);
            }
            $encode_per=JSON::encode($array_per);
            $encode_soft=JSON::encode($array_soft);
            $encode_iis=JSON::encode($array_iis);
            $model->data = $encode_per;
            $model->data_software = $encode_soft;
            $model->data_iis = $encode_iis;
            $model->os = JSON::encode($array_os);
            $model->save();
            Log::addFisOtcc($model, true);
            if(Yii::$app->request->post('next')=="1"){
                return $this->redirect(Url::to(['vtcc/create'], true));
            } if (Yii::$app->request->post('next')=="2"){
                return $this->redirect(Url::to(['/fis/schools/view?id='.$model->org_id.''], true));
            } else {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing FisOtcc model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $data = $model->data;
        $model->data = json_decode($model->data);
        echo '<input id="data" value='.str_replace(" ", "&nbsp;", $data).' type="hidden">';
        $software = $model->data_software;
        $model->data_software = json_decode($model->data_software);
        echo '<input id="software" value='.str_replace(" ", "&nbsp;", $software).' type="hidden">';
        $iis = $model->data_iis;
        $model->data_iis = json_decode($model->data_iis);
        echo '<input id="iis" value='.str_replace(" ", "&nbsp;", $iis).' type="hidden">';
        $os = $model->os;
        $model->os = json_decode($model->os);
        if($os)
            echo '<input id="os" value='.str_replace(" ", "&nbsp;", $os).' type="hidden">';
        if ($model->load(Yii::$app->request->post())) {
            $post_data = Yii::$app->request->post('FisOtcc')['data'];
            $post_data_software = Yii::$app->request->post('FisOtcc')['data_software'];
            $post_data_iis = Yii::$app->request->post('FisOtcc')['data_iis'];
            $os_name = Yii::$app->request->post('os_name');
            $os_version = Yii::$app->request->post('os_version');
            $os_build = Yii::$app->request->post('os_build');
            $os_serial = Yii::$app->request->post('os_serial');
            $array_os = array(['os_name'=>$os_name, 'os_version'=>$os_version, 'os_build'=>$os_build, 'os_serial'=>$os_serial]);
            $array_per = array();
            $array_soft = array();
            $array_iis = array();
            foreach ($post_data as $item){
                array_push($array_per, $item);
            }
            foreach ($post_data_software as $item){
                array_push($array_soft, $item);
            }
            foreach ($post_data_iis as $item){
                array_push($array_iis, $item);
            }
            $encode_per=JSON::encode($array_per);
            $encode_soft=JSON::encode($array_soft);
            $encode_iis=JSON::encode($array_iis);
            $model->data = $encode_per;
            $model->data_software = $encode_soft;
            $model->data_iis = $encode_iis;
            $model->os = JSON::encode($array_os);
            $model->save();
            Log::addFisOtcc($model, false);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing FisOtcc model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FisOtcc model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return FisOtcc the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FisOtcc::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    private function accessDenied() {
        throw new HttpException(403, 'Вы не добавили данные о школы');
    }
}
