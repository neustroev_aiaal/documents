<?php
/**
 * Created by PhpStorm.
 * User: user3
 * Date: 20.03.2018
 * Time: 12:56
 */

namespace app\controllers;


use app\models\Log;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class AdminController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [User::ROLE_ADMIN],
                    ],
                ],
            ],
        ];
    }

    public function actionLogs() {
        $searchModel = new Log();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('logs', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }
}