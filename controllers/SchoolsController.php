<?php

namespace app\controllers;

require $_SERVER["DOCUMENT_ROOT"] . '/Library/NCLNameCaseRu.php';

use app\models\Log;
use app\models\Nord;
use app\models\Otcc;
use app\models\Peripheral;
use app\models\Projects;
use app\models\SchoolsSearch;
use app\models\Security;
use app\models\Software;
use app\models\User;
use app\models\Vtcc;
use PhpOffice\PhpWord\TemplateProcessor;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SimpleXMLElement;
use Yii;
use app\models\Schools;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use ZipArchive;

/**
 * SchoolsController implements the CRUD actions for Schools model.
 */
class SchoolsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [User::ROLE_ADMIN, User::ROLE_MANAGER],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'index', 'update', 'view'],
                        'roles' => [User::ROLE_USER],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'index', 'update', 'view', 'document'],
                        'roles' => [User::ROLE_OPERATOR],
                    ]
                ],
            ],
        ];
    }

    /**
     * Lists all Schools models.
     * @return mixed
     */
    public function actionIndex()
    {
//        if (!\Yii::$app->user->can("admin")) {
//            throw new ForbiddenHttpException('Access denied');
//        }
        $searchModel = new SchoolsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $user_id = \Yii::$app->user->id;
        $dir = $this->getDir();
        if (!is_dir($dir . "/documents/templates/")) {
            mkdir($dir . "/documents/templates/", 0777, true);
        }
        if (!is_dir($dir . "/documents/schools/")) {
            mkdir($dir . "/documents/schools/", 0777, true);
        }
        if (\Yii::$app->user->can("admin")) {
            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel
            ]);
        } else {
            return $this->render('index', [
                'dataProvider' => new ActiveDataProvider([
                    'query' => Schools::find()->where(['user_id' => $user_id]),
                ]),
                'searchModel' => $searchModel
            ]);
        }
    }

    /**
     * Displays a single Schools model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Schools model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
//        var_dump(bin2hex(openssl_random_pseudo_bytes(15)));
//        die;
//        $t = HelperClass::encrypt("\"Муниципальное бюджетное общеобразовательное учреждение\" средняя общеобразовательная школа города Покровск № 1");
//        var_dump($t);
//        var_dump(HelperClass::decrypt($t));
//        die;
        $model = new Schools();
        $model->user_id = \Yii::$app->user->id;
        $model->project_id = 1;
        $model->region_id = User::findOne(\Yii::$app->user->id)->region_id;

        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            Log::addSchools($model, true);
            if (Yii::$app->request->post('next') == "1") {
                return $this->redirect(Url::to(['otcc/create?id=' . $model->id], true));
            } else {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Schools model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Log::addSchools($model, false);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionGenerate($id)
    {
        $model = $this->findModel($id);

        if (!is_dir($this->getDir() . "/documents/schools/" . Schools::translit($model->short_name_org))) {
            mkdir($this->getDir() . "/documents/schools/" . Schools::translit($model->short_name_org), 0777, true);
        }
//        if ($model->load(Yii::$app->request->post())) {
//
//            $model->save();
//            return $this->redirect(['generate-success', 'id' => $model->id]);
//        }

        return $this->render('generate', [
            'model' => $model,
        ]);
    }

    public function actionGenerateSuccess($id, $type)
    {
        $model = $this->findModel($id);
        return $this->render('success_generate', [
            'model' => $model, 'type' => $type,
        ]);
    }

    public function actionTechPassport($id)
    {
        $this->tech_passport($id);
        Log::generateRCOI($this->findModel($id), "Тех паспорт");
        return $this->redirect(['generate-success', 'id' => $id, 'type' => 2]);
    }

    public function actionProtokol($id)
    {
        $this->protokol($id);
        Log::generateRCOI($this->findModel($id), "Протокол");
        return $this->redirect(['generate-success', 'id' => $id, 'type' => 1]);
    }

    public function actionCertificate($id)
    {
        $this->certificate($id);
        Log::generateRCOI($this->findModel($id), "Аттестат");
        return $this->redirect(['generate-success', 'id' => $id, 'type' => 4]);
    }

    public function actionFinale($id)
    {
        $this->finale($id);
        Log::generateRCOI($this->findModel($id), "Заключение");
        return $this->redirect(['generate-success', 'id' => $id, 'type' => 3]);
    }

    public function actionAkt($id)
    {
        $this->akt($id, false);
        Log::generateRCOI($this->findModel($id), "Акт установки");
        return $this->redirect(['generate-success', 'id' => $id, 'type' => 5]);
    }

    private function protokol($id)
    {
        $model = $this->findModel($id);
        $otcc = Otcc::getOtcc($id);
        $project = Projects::findOne($model->project_id);
        $bool = false;
        $comp_name = "";
        $arrIpAddress = array();
        $arrMacAddress = array();
        $arrNumberRoom = array();
        foreach ($otcc as $item1) {
            $comp_name = $item1->pc_name;
            array_push($arrIpAddress, $item1->ip_address);
            array_push($arrMacAddress, $item1->mac_address);
            array_push($arrNumberRoom, $item1->number_room);
            $json_soft_data = json_decode($item1->data_iis, true);
            foreach ($json_soft_data as $json_item) {
                if ($json_item['type_soft'] == 4) {
                    $bool = true;
                }
            }
        }
        if ($bool) {
            $templateProcessorProtokol = new TemplateProcessor($this->getDir() . "/documents/templates/protokol_web.docx");
        } else {
            $templateProcessorProtokol = new TemplateProcessor($this->getDir() . "/documents/templates/protokol_kasp.docx");
        }
        $date = date_parse($model->date);
        $date_create = date_parse($model->date_create);
        $templateProcessorProtokol->setValue('date_public', htmlspecialchars("«" . $date['day'] . "» " . Schools::getMonth($date['month']) . " " . $date['year']));
        $templateProcessorProtokol->setValue('date_public#1', htmlspecialchars($date['day'] . "/" . $date['month'] . "/" . $date['year']));
        $templateProcessorProtokol->setValue('date_create', htmlspecialchars("«" . $date_create['day'] . "» " . Schools::getMonth($date_create['month']) . " " . $date_create['year']));
        $templateProcessorProtokol->setValue('name_org', htmlspecialchars($model->name_org_rod)); //Наименование организации в родительном падеже
        $templateProcessorProtokol->setValue('short_name', htmlspecialchars($model->short_name_org)); // Краткое имя организации
        $templateProcessorProtokol->setValue('number_protokol', htmlspecialchars($model->number_protokol)); //Номер документа протокола
//        $templateProcessorProtokol->setValue('fio_nord', htmlspecialchars(preg_replace('#(.*)\s+(.).*\s+(.).*#usi', '$1', Nord::get($model->spec_nord_id)->fio))); // Фамилия кто составил техпаспорт
        $templateProcessorProtokol->setValue('address_org', htmlspecialchars($model->address)); // Адрес организации
//        $templateProcessorProtokol->setValue('full_name', htmlspecialchars($project->full_name)); // Полное наименование заказчика
//        $templateProcessorProtokol->setValue('short_name', htmlspecialchars($project->short_name)); // Краткое наименование заказчика
        $templateProcessorProtokol->setValue('doc_name', htmlspecialchars($project->name_doc)); // Номер документа
        $templateProcessorProtokol->setValue('comp_name', htmlspecialchars($comp_name)); // Имя компьютера
        $templateProcessorProtokol->setValue('number_room', htmlspecialchars(implode(", ", $arrNumberRoom)));
        $this->eshkere($otcc, $templateProcessorProtokol);
        $templateProcessorProtokol->setValue('ip_address', htmlspecialchars(implode(", ", $arrIpAddress)));
        $templateProcessorProtokol->setValue('mac_address', htmlspecialchars(implode(", ", $arrMacAddress)));
//        $templateProcessorProtokol->setValue('fio_nord#1', htmlspecialchars(preg_replace('#(.*)\s+(.).*\s+(.).*#usi', '$2.$3. $1', Nord::get($model->spec_nord_id)->fio))); // ФИО кто составил техпаспорт

        $filename = $this->getDir() . "/documents/schools/" . Schools::translit($model->short_name_org) . "/protokol_" . Schools::translit($model->short_name_org) . ".docx";
        $templateProcessorProtokol->saveAs($filename);
//        $page = $this->get_num_pages_docx($filename);
//        $templateProcessorProtokol_1 = new TemplateProcessor($filename);
//        $templateProcessorProtokol_1->setValue('page', htmlspecialchars($page)); // Количество страниц
//        $templateProcessorProtokol_1->saveAs($filename);
    }

    private function tech_passport($id)
    {
        $model = $this->findModel($id);
        $otcc = Otcc::getOtcc($id);
        $project = Projects::findOne($model->project_id);
        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/tech_passport.docx");
        // Variables on different parts of document
        $date = date_parse($model->date);
        $date_create = date_parse($model->date_create);
        $templateProcessor->setValue('date_public', htmlspecialchars("«" . $date['day'] . "» " . Schools::getMonth($date['month']) . " " . $date['year']));
        $templateProcessor->setValue('date_create', htmlspecialchars("«" . $date_create['day'] . "» " . Schools::getMonth($date_create['month']) . " " . $date_create['year']));
        $templateProcessor->setValue('position_org', htmlspecialchars($model->position_director)); //Должность руководителя
        $templateProcessor->setValue('fio_org', htmlspecialchars(preg_replace('#(.*)\s+(.).*\s+(.).*#usi', '$2.$3. $1', $model->school_director))); //ФИО руководителя
        $templateProcessor->setValue('date_public', htmlspecialchars($model->date)); //Дата подписания
        $templateProcessor->setValue('name_org', htmlspecialchars($model->name_org_rod)); //Наименование организации в родительном падеже
        $templateProcessor->setValue('number_passport', htmlspecialchars($model->number_techpassport)); //Номер документа техпаспорта
        $templateProcessor->setValue('name_org#1', htmlspecialchars($model->name_org)); //Наименование организации
        $templateProcessor->setValue('fio_spec_org', htmlspecialchars(preg_replace('#(.*)\s+(.).*\s+(.).*#usi', '$1 $2.$3.', $model->spec_fio))); // ФИО специалиста
//        $templateProcessor->setValue('fio_nord', htmlspecialchars(preg_replace('#(.*)\s+(.).*\s+(.).*#usi', '$1', Nord::get($model->spec_nord_id)->fio))); // Фамилия кто составил техпаспорт
        $templateProcessor->setValue('short_name_org', htmlspecialchars($model->short_name_org)); // Краткое имя организации
        $templateProcessor->setValue('address_org', htmlspecialchars($model->address)); // Адрес организации
        $templateProcessor->setValue('count_comp', htmlspecialchars(count($otcc))); // Количество компов
        $templateProcessor->setValue('start_project', htmlspecialchars($project->date_start_project)); // Номер документа
        $templateProcessor->setValue('end_project', htmlspecialchars($project->date_end_project)); // Дата документа
        //Таблица ОТСС
        $arrNumberRoom = array();
        $templateProcessor->cloneRow('comp_name_otcc', count($otcc));
        $a = 1;
        foreach ($otcc as $item1) {
            $templateProcessor->setValue('comp_name_otcc#' . $a . '', htmlspecialchars($item1->pc_name)); // Имя компа (ОТСС)
            $templateProcessor->setValue('number_room_otcc#' . $a . '', htmlspecialchars($item1->number_room));
            array_push($arrNumberRoom, $item1->number_room);
            $json_ottc_data = json_decode($item1->data, true);
            $templateProcessor->cloneRow('peripheral_otcc#' . $a . '', count($json_ottc_data));
            $i = 1;
            foreach ($json_ottc_data as $json_item) {
                $templateProcessor->setValue('count_otcc#' . $a . '#' . $i . '', htmlspecialchars($i));
                $templateProcessor->setValue('peripheral_otcc#' . $a . '#' . $i . '', htmlspecialchars(Peripheral::getPeripheralName($json_item['peripheral'])));
                $templateProcessor->setValue('model_otcc#' . $a . '#' . $i . '', htmlspecialchars($json_item['model']));
                $templateProcessor->setValue('serial_number_otcc#' . $a . '#' . $i . '', htmlspecialchars($json_item['serial_number']));
                $i++;
            }
            $a++;
        }
        //Таблица ВТСС
//        $templateProcessor->cloneRow('comp_name_vtcc', count($vtcc));
//        $j = 1;
//        foreach ($vtcc as $item) {
//            $templateProcessor->setValue('comp_name_vtcc#' . $j . '', htmlspecialchars($item->pc_name));
//            $templateProcessor->setValue('number_room_vtcc#' . $j . '', htmlspecialchars($item->number_room)); // Номер кабинета
//            $json_vttc_data = json_decode($item->data, true);
//            $templateProcessor->cloneRow('peripheral_vtcc#' . $j . '', count($json_vttc_data));
//            $o = 1;
//            foreach ($json_vttc_data as $json_item) {
//                $templateProcessor->setValue('count_vtcc#' . $j . '#' . $o . '', htmlspecialchars($o));
//                $templateProcessor->setValue('peripheral_vtcc#' . $j . '#' . $o . '', htmlspecialchars(Peripheral::getPeripheralName($json_item['peripheral'])));
//                $templateProcessor->setValue('model_vtcc#' . $j . '#' . $o . '', htmlspecialchars($json_item['model']));
//                $templateProcessor->setValue('serial_number_vtcc#' . $j . '#' . $o . '', htmlspecialchars($json_item['serial_number']));
//                $o++;
//            }
//            $j++;
//        }

        //software
//        $countRowSoftware = 0;
//        foreach ($otcc as $item1) {
//            $json_soft_data = json_decode($item1->data_software, true);
//            foreach ($json_soft_data as $json_item) {
//                if ($json_item['type_soft'] == 3) {
//                    $countRowSoftware++;
//                }
//            }
//        }
//        $templateProcessor->cloneRow('type_soft', $countRowSoftware);
//        $s = 1;
        foreach ($otcc as $item1) {
            if($item1->os!=null) {
                $json_os = json_decode($item1->os, true);
                foreach ($json_os as $json_item) {
                    $templateProcessor->setValue('name_os', htmlspecialchars($json_item['os_name']));
                    $templateProcessor->setValue('version_os', htmlspecialchars($json_item['os_version']));
                    $templateProcessor->setValue('build_os', htmlspecialchars($json_item['os_build']));
                }
            }
        }
//            $json_soft_data = json_decode($item1->data_software, true);
//            foreach ($json_soft_data as $json_item) {
//                if ($json_item['type_soft'] == 3) {
//                    $templateProcessor->setValue('count_soft#' . $s . '', htmlspecialchars($s+1)); // Имя компа (ОТСС)
//                    $templateProcessor->setValue('type_soft#' . $s . '', htmlspecialchars(Otcc::$SOFT[$json_item['type_soft']]));
//                    $templateProcessor->setValue('name_soft#' . $s . '', htmlspecialchars($json_item['name']));
//                    $templateProcessor->setValue('serial_number_soft#' . $s . '', htmlspecialchars($json_item['serial_number']));
//                    $templateProcessor->setValue('name_comp_soft#' . $s . '', htmlspecialchars($item1->pc_name));
//                    $s++;
//                }
//            }
//        }
        $templateProcessor->setValue('number_room', htmlspecialchars(implode(", ", $arrNumberRoom)));

        //СЗИ
        $this->eshkere($otcc, $templateProcessor);


//        //antivirus
//        $antivirus = Security::getSecurity($model->antivirus_id);
//        $templateProcessor->setValue('antivirus_name', htmlspecialchars($antivirus->name)); // Наименование антивируса
//        $templateProcessor->setValue('antivirus_count', htmlspecialchars($model->count_antiviruses)); // Количество антивируса
//        $templateProcessor->setValue('antivirus_serial_number', htmlspecialchars($model->serial_number_antiviruses)); // Серийный номер антивируса
//        $templateProcessor->setValue('antivirus_mark', htmlspecialchars($model->mark_num_antiviruses)); // Знак соответсвия антивируса
//        $templateProcessor->setValue('antivirus_certificate', htmlspecialchars($antivirus->certificate)); // Сертификат антивируса
//        //vipnet
//        $vipnet = Security::getType(0);
//        $templateProcessor->setValue('vipnet_name', htmlspecialchars($vipnet->name)); // Наименование випнета
//        $templateProcessor->setValue('vipnet_count', htmlspecialchars($model->count_vipnets)); // Количество випнета
//        $templateProcessor->setValue('vipnet_serial_number', htmlspecialchars($model->serial_number_vipnets)); // Серийный номер випнета
//        $templateProcessor->setValue('vipnet_mark', htmlspecialchars($model->mark_num_vipnets)); // Знак соответсвия випнета
//        $templateProcessor->setValue('vipnet_certificate', htmlspecialchars($vipnet->certificate)); // Сертификат випнета

        if (!is_dir($this->getDir() . "/documents/schools/" . Schools::translit($model->short_name_org) . "")) {
            mkdir($this->getDir() . "/documents/schools/" . Schools::translit($model->short_name_org) . "", 0777, true);
        }

        $templateProcessor->saveAs($this->getDir() . "/documents/schools/" . Schools::translit($model->short_name_org) . "/tech_passport_" . Schools::translit($model->short_name_org) . ".docx");
    }

    private function eshkere($otcc, $templateProcessor)
    {
        $count_dallas = 0;
        $count_vipnet = 0;
        $count_antivirus = 0;
        $count_vipnet_ids = 0;
        $count_vipnet_safeboot = 0;
        foreach ($otcc as $item1) {
            $json_soft_data = json_decode($item1->data_iis, true);
            if ($json_soft_data != null) {
                foreach ($json_soft_data as $json_item) {
                    if (Security::getSecurityType($json_item['type_soft']) == 2) {
                        $count_dallas++;
                    }
                    if (Security::getSecurityType($json_item['type_soft']) == 1) {
                        $count_antivirus++;
                    }
                    if (Security::getSecurityType($json_item['type_soft']) == 0) {
                        $count_vipnet++;
                    }
                    if (Security::getSecurityType($json_item['type_soft']) == 3) {
                        $count_vipnet_safeboot++;
                    }
                    if (Security::getSecurityType($json_item['type_soft']) == 4) {
                        $count_vipnet_ids++;
                    }
                }
                if ($count_antivirus > 1)
                    $templateProcessor->cloneRow('antivirus_name', $count_antivirus);
                if ($count_dallas > 1)
                    $templateProcessor->cloneRow('dallas_name', $count_dallas);
                if ($count_vipnet > 1)
                    $templateProcessor->cloneRow('vipnet_name', $count_vipnet);
                if ($count_vipnet_safeboot > 1)
                    $templateProcessor->cloneRow('vipnet_name', $count_vipnet_safeboot);
                if ($count_vipnet_ids > 1)
                    $templateProcessor->cloneRow('vipnet_name', $count_vipnet_ids);
            }
        }
        $ant = 1;
        $dal = 1;
        $vip = 1;
        foreach ($otcc as $item1) {
            $json_soft_data = json_decode($item1->data_iis, true);
            if (count($json_soft_data) > 0)
                foreach ($json_soft_data as $json_item) {
                    if (Security::getSecurityType($json_item['type_soft']) == 2) {
                        if ($count_dallas > 1) {
                            $templateProcessor->setValue('dallas_name#' . $dal, htmlspecialchars(Security::getSecurityName($json_item['type_soft']))); // Наименование далласа
                            $templateProcessor->setValue('dallas_count#' . $dal, htmlspecialchars(1)); // Количество далласа
                            $templateProcessor->setValue('dallas_serial_number#' . $dal, htmlspecialchars($json_item['serial_number'])); // Серийный номер далласа
                            $templateProcessor->setValue('dallas_mark#' . $dal, htmlspecialchars($json_item['mark_number'])); // Знак соответсвия далласа
                            $templateProcessor->setValue('dallas_certificate#' . $dal, htmlspecialchars(Security::getSecurityCertificate($json_item['type_soft']))); // Сертификат далласа
                            $templateProcessor->setValue('dallas_number_room#' . $dal, htmlspecialchars($item1->number_room)); // Номер кабинета
                            $dal++;
                        } else {
                            $templateProcessor->setValue('dallas_name', htmlspecialchars(Security::getSecurityName($json_item['type_soft']))); // Наименование далласа
                            $templateProcessor->setValue('dallas_count', htmlspecialchars(1)); // Количество далласа
                            $templateProcessor->setValue('dallas_serial_number', htmlspecialchars($json_item['serial_number'])); // Серийный номер далласа
                            $templateProcessor->setValue('dallas_mark', htmlspecialchars($json_item['mark_number'])); // Знак соответсвия далласа
                            $templateProcessor->setValue('dallas_certificate', htmlspecialchars(Security::getSecurityCertificate($json_item['type_soft']))); // Сертификат далласа
                            $templateProcessor->setValue('dallas_number_room', htmlspecialchars($item1->number_room)); // Номер кабинета
                        }
                    }
                    if (Security::getSecurityType($json_item['type_soft']) == 1) {
                        if ($count_antivirus > 1) {
                            $templateProcessor->setValue('antivirus_name#' . $ant, htmlspecialchars(Security::getSecurityName($json_item['type_soft']))); // Наименование антивируса
                            $templateProcessor->setValue('antivirus_count#' . $ant, htmlspecialchars(1)); // Количество антивируса
                            $templateProcessor->setValue('antivirus_serial_number#' . $ant, htmlspecialchars($json_item['serial_number'])); // Серийный номер антивируса
                            $templateProcessor->setValue('antivirus_mark#' . $ant, htmlspecialchars($json_item['mark_number'])); // Знак соответсвия антивируса
                            $templateProcessor->setValue('antivirus_certificate#' . $ant, htmlspecialchars(Security::getSecurityCertificate($json_item['type_soft']))); // Сертификат антивируса
                            $templateProcessor->setValue('antivirus_number_room#' . $ant, htmlspecialchars($item1->number_room)); // Номер кабинета
                            $ant++;
                        } else {
                            $templateProcessor->setValue('antivirus_name', htmlspecialchars(Security::getSecurityName($json_item['type_soft']))); // Наименование антивируса
                            $templateProcessor->setValue('antivirus_count', htmlspecialchars(1)); // Количество антивируса
                            $templateProcessor->setValue('antivirus_serial_number', htmlspecialchars($json_item['serial_number'])); // Серийный номер антивируса
                            $templateProcessor->setValue('antivirus_mark', htmlspecialchars($json_item['mark_number'])); // Знак соответсвия антивируса
                            $templateProcessor->setValue('antivirus_certificate', htmlspecialchars(Security::getSecurityCertificate($json_item['type_soft']))); // Сертификат антивируса
                            $templateProcessor->setValue('antivirus_number_room', htmlspecialchars($item1->number_room)); // Номер кабинета
                        }
                    }
                    if (Security::getSecurityType($json_item['type_soft']) == 0) {
                        if ($count_vipnet > 1) {
                            $templateProcessor->setValue('vipnet_name#' . $vip, htmlspecialchars(Security::getSecurityName($json_item['type_soft']))); // Наименование антивируса
                            $templateProcessor->setValue('vipnet_count#' . $vip, htmlspecialchars(1)); // Количество антивируса
                            $templateProcessor->setValue('vipnet_serial_number#' . $vip, htmlspecialchars($json_item['serial_number'])); // Серийный номер антивируса
                            $templateProcessor->setValue('vipnet_mark#' . $vip, htmlspecialchars($json_item['mark_number'])); // Знак соответсвия антивируса
                            $templateProcessor->setValue('vipnet_certificate#' . $vip, htmlspecialchars(Security::getSecurityCertificate($json_item['type_soft']))); // Сертификат антивируса
                            $templateProcessor->setValue('vipnet_number_room#' . $vip, htmlspecialchars($item1->number_room)); // Номер кабинета
                            $vip++;
                        } else {
                            $templateProcessor->setValue('vipnet_name', htmlspecialchars(Security::getSecurityName($json_item['type_soft']))); // Наименование антивируса
                            $templateProcessor->setValue('vipnet_count', htmlspecialchars(1)); // Количество антивируса
                            $templateProcessor->setValue('vipnet_serial_number', htmlspecialchars($json_item['serial_number'])); // Серийный номер антивируса
                            $templateProcessor->setValue('vipnet_mark', htmlspecialchars($json_item['mark_number'])); // Знак соответсвия антивируса
                            $templateProcessor->setValue('vipnet_certificate', htmlspecialchars(Security::getSecurityCertificate($json_item['type_soft']))); // Сертификат антивируса
                            $templateProcessor->setValue('vipnet_number_room', htmlspecialchars($item1->number_room)); // Номер кабинета
                        }
                    }
                    if (Security::getSecurityType($json_item['type_soft']) == 3) {
                        if ($count_vipnet_safeboot > 1) {
                            $templateProcessor->setValue('vipnet_safeboot_name#' . $vip, htmlspecialchars(Security::getSecurityName($json_item['type_soft']))); // Наименование антивируса
                            $templateProcessor->setValue('vipnet_safeboot_count#' . $vip, htmlspecialchars(1)); // Количество антивируса
                            $templateProcessor->setValue('vipnet_safeboot_serial_number#' . $vip, htmlspecialchars($json_item['serial_number'])); // Серийный номер антивируса
                            $templateProcessor->setValue('vipnet_safeboot_mark#' . $vip, htmlspecialchars($json_item['mark_number'])); // Знак соответсвия антивируса
                            $templateProcessor->setValue('vipnet_safeboot_certificate#' . $vip, htmlspecialchars(Security::getSecurityCertificate($json_item['type_soft']))); // Сертификат антивируса
                            $templateProcessor->setValue('vipnet_safeboot_number_room#' . $vip, htmlspecialchars($item1->number_room)); // Номер кабинета
                            $vip++;
                        } else {
                            $templateProcessor->setValue('vipnet_safeboot_name', htmlspecialchars(Security::getSecurityName($json_item['type_soft']))); // Наименование антивируса
                            $templateProcessor->setValue('vipnet_safeboot_count', htmlspecialchars(1)); // Количество антивируса
                            $templateProcessor->setValue('vipnet_safeboot_serial_number', htmlspecialchars($json_item['serial_number'])); // Серийный номер антивируса
                            $templateProcessor->setValue('vipnet_safeboot_mark', htmlspecialchars($json_item['mark_number'])); // Знак соответсвия антивируса
                            $templateProcessor->setValue('vipnet_safeboot_certificate', htmlspecialchars(Security::getSecurityCertificate($json_item['type_soft']))); // Сертификат антивируса
                            $templateProcessor->setValue('vipnet_safeboot_number_room', htmlspecialchars($item1->number_room)); // Номер кабинета
                        }
                    }
                    if (Security::getSecurityType($json_item['type_soft']) == 4) {
                        if ($count_vipnet_ids > 1) {
                            $templateProcessor->setValue('vipnet_ids_name#' . $vip, htmlspecialchars(Security::getSecurityName($json_item['type_soft']))); // Наименование антивируса
                            $templateProcessor->setValue('vipnet_ids_count#' . $vip, htmlspecialchars(1)); // Количество антивируса
                            $templateProcessor->setValue('vipnet_ids_serial_number#' . $vip, htmlspecialchars($json_item['serial_number'])); // Серийный номер антивируса
                            $templateProcessor->setValue('vipnet_ids_mark#' . $vip, htmlspecialchars($json_item['mark_number'])); // Знак соответсвия антивируса
                            $templateProcessor->setValue('vipnet_ids_certificate#' . $vip, htmlspecialchars(Security::getSecurityCertificate($json_item['type_soft']))); // Сертификат антивируса
                            $templateProcessor->setValue('vipnet_ids_number_room#' . $vip, htmlspecialchars($item1->number_room)); // Номер кабинета
                            $vip++;
                        } else {
                            $templateProcessor->setValue('vipnet_ids_name', htmlspecialchars(Security::getSecurityName($json_item['type_soft']))); // Наименование антивируса
                            $templateProcessor->setValue('vipnet_ids_count', htmlspecialchars(1)); // Количество антивируса
                            $templateProcessor->setValue('vipnet_ids_serial_number', htmlspecialchars($json_item['serial_number'])); // Серийный номер антивируса
                            $templateProcessor->setValue('vipnet_ids_mark', htmlspecialchars($json_item['mark_number'])); // Знак соответсвия антивируса
                            $templateProcessor->setValue('vipnet_ids_certificate', htmlspecialchars(Security::getSecurityCertificate($json_item['type_soft']))); // Сертификат антивируса
                            $templateProcessor->setValue('vipnet_ids_number_room', htmlspecialchars($item1->number_room)); // Номер кабинета
                        }
                    }
                }
        }
    }

    private function finale($id)
    {
        $model = $this->findModel($id);
        $otcc = Otcc::getOtcc($id);
        $project = Projects::findOne($model->project_id);
        $bool = false;
        $arrNumberRoom = array();
        foreach ($otcc as $item1) {
            $json_soft_data = json_decode($item1->data_iis, true);
            array_push($arrNumberRoom, $item1->number_room);
            foreach ($json_soft_data as $json_item) {
                if ($json_item['type_soft'] == 7) {
                    $bool = true;
                }
            }
        }
        if ($bool) {
            $templateProcessorFinale = new TemplateProcessor($this->getDir() . "/documents/templates/finale.docx");
        } else {
            $templateProcessorFinale = new TemplateProcessor($this->getDir() . "/documents/templates/finale_safeboot.docx");
        }
        $date = date_parse($model->date);
        $date_create = date_parse($model->date_create);
        $templateProcessorFinale->setValue('date_public', htmlspecialchars("«" . $date['day'] . "» " . Schools::getMonth($date['month']) . " " . $date['year']));
        $templateProcessorFinale->setValue('date_create', htmlspecialchars("«" . $date_create['day'] . "» " . Schools::getMonth($date_create['month']) . " " . $date_create['year']));
        $templateProcessorFinale->setValue('name_org', htmlspecialchars($model->name_org_rod)); //Наименование организации в родительном падеже
        $templateProcessorFinale->setValue('short_name', htmlspecialchars($model->short_name_org)); // Краткое имя организации
        $templateProcessorFinale->setValue('number_finale', htmlspecialchars($model->number_finale)); //Номер документа заключения
        $templateProcessorFinale->setValue('number_protokol', htmlspecialchars($model->number_protokol)); //Номер документа протокола
//        $templateProcessorFinale->setValue('fio_nord', htmlspecialchars(preg_replace('#(.*)\s+(.).*\s+(.).*#usi', '$1', Nord::get($model->spec_nord_id)->fio))); // Фамилия кто составил техпаспорт
        $templateProcessorFinale->setValue('address_org', htmlspecialchars($model->address)); // Адрес организации
        $templateProcessorFinale->setValue('doc_name', htmlspecialchars($project->name_doc)); // Номер документа
        $templateProcessorFinale->setValue('doc_date', htmlspecialchars($model->date)); // Дата документа
        $templateProcessorFinale->setValue('number_room', htmlspecialchars(implode(", ", $arrNumberRoom)));

        $this->eshkere($otcc, $templateProcessorFinale);
//        $templateProcessorFinale->setValue('fio_nord#1', htmlspecialchars(preg_replace('#(.*)\s+(.).*\s+(.).*#usi', '$2.$3. $1', Nord::get($model->spec_nord_id)->fio))); // ФИО кто составил техпаспорт
        $templateProcessorFinale->saveAs($this->getDir() . "/documents/schools/" . Schools::translit($model->short_name_org) . "/finale_" . Schools::translit($model->short_name_org) . ".docx");
    }

    private function certificate($id)
    {
        //TODO доделать дату документа ЦЗИ
        $model = $this->findModel($id);
        $otcc = Otcc::getOtcc($id);
        $project = Projects::findOne($model->project_id);
        $templateProcessorCertificate = new TemplateProcessor($this->getDir() . "/documents/templates/certificate.docx");
        $templateProcessorCertificate->setValue('name_org', htmlspecialchars($model->name_org_rod)); //Наименование организации в родительном падеже
        $templateProcessorCertificate->setValue('number_certificate', htmlspecialchars($model->number_certificate)); //Номер документа аттестата
        $templateProcessorCertificate->setValue('number_protokol', htmlspecialchars($model->number_protokol)); //Номер документа протокола
        $templateProcessorCertificate->setValue('number_finale', htmlspecialchars($model->number_finale)); //Номер документа заключения
//        $templateProcessorCertificate->setValue('fio_nord', htmlspecialchars(preg_replace('#(.*)\s+(.).*\s+(.).*#usi', '$1', Nord::get($model->spec_nord_id)->fio))); // Фамилия кто составил техпаспорт
        $templateProcessorCertificate->setValue('address_org', htmlspecialchars($model->address)); // Адрес организации
        $templateProcessorCertificate->setValue('doc_name', htmlspecialchars($project->name_doc)); // Номер документа
        $templateProcessorCertificate->setValue('doc_date', htmlspecialchars($model->date)); // Дата документа
        $this->eshkere($otcc, $templateProcessorCertificate);

        $arrNumberRoom = array();
        $templateProcessorCertificate->cloneRow('count_otcc', count($otcc));
        $a = 1;
        foreach ($otcc as $item1) {
            $json_ottc_data = json_decode($item1->data, true);
            array_push($arrNumberRoom, $item1->number_room);
            $templateProcessorCertificate->cloneRow('peripheral_otcc#' . $a . '', count($json_ottc_data));
            $i = 1;
            foreach ($json_ottc_data as $json_item) {
                $templateProcessorCertificate->setValue('count_otcc#' . $a . '#' . $i . '', htmlspecialchars($i));
                $templateProcessorCertificate->setValue('peripheral_otcc#' . $a . '#' . $i . '', htmlspecialchars(Peripheral::getPeripheralName($json_item['peripheral'])));
                $templateProcessorCertificate->setValue('model_otcc#' . $a . '#' . $i . '', htmlspecialchars($json_item['model']));
                $templateProcessorCertificate->setValue('serial_number_otcc#' . $a . '#' . $i . '', htmlspecialchars($json_item['serial_number']));
                $i++;
            }
            $a++;
        }
        $templateProcessorCertificate->setValue('number_room', htmlspecialchars(implode(", ", $arrNumberRoom)));
//        $countRowSoftware = 0;
//        foreach ($otcc as $item1) {
//            $json_soft_data = json_decode($item1->data_software, true);
//            foreach ($json_soft_data as $json_item) {
//                if ($json_item['type_soft'] != 2) {
//                    $countRowSoftware++;
//                }
//            }
//        }
        foreach ($otcc as $item1) {
            if($item1->os!=null) {
                $json_os = json_decode($item1->os, true);
                foreach ($json_os as $json_item) {
                    $templateProcessorCertificate->setValue('name_os', htmlspecialchars($json_item['os_name']));
                    $templateProcessorCertificate->setValue('version_os', htmlspecialchars($json_item['os_version']));
                    $templateProcessorCertificate->setValue('build_os', htmlspecialchars($json_item['os_build']));
                }
            }
        }
//        $templateProcessorCertificate->cloneRow('type_soft', $countRowSoftware);
//        $s = 1;
//        foreach ($otcc as $item1) {
//            $json_soft_data = json_decode($item1->data_software, true);
//            foreach ($json_soft_data as $json_item) {
//                if ($json_item['type_soft'] != 2) {
//                    $templateProcessorCertificate->setValue('count_soft#' . $s . '', htmlspecialchars($s)); // Имя компа (ОТСС)
//                    $templateProcessorCertificate->setValue('type_soft#' . $s . '', htmlspecialchars(Otcc::$SOFT[$json_item['type_soft']]));
//                    $templateProcessorCertificate->setValue('name_soft#' . $s . '', htmlspecialchars($json_item['name']));
////                    $templateProcessorCertificate->setValue('serial_number_soft#' . $s . '', htmlspecialchars($json_item['serial_number']));
////                    $templateProcessorCertificate->setValue('name_comp_soft#' . $s . '', htmlspecialchars($item1->pc_name));
//                    $s++;
//                }
//            }
//        }

        $date = date_parse($model->date);
        $templateProcessorCertificate->setValue('date_public', htmlspecialchars("«" . $date['day'] . "» " . Schools::getMonth($date['month']) . " " . $date['year']));
        $year = $date['year'] + 3;
        $templateProcessorCertificate->setValue('date_public#1', htmlspecialchars("«" . $date['day'] . "» " . Schools::getMonth($date['month']) . " " . $year));
        $templateProcessorCertificate->saveAs($this->getDir() . "/documents/schools/" . Schools::translit($model->short_name_org) . "/certificate_" . Schools::translit($model->short_name_org) . ".docx");
    }

    private function akt($id, $negative)
    {
        $model = $this->findModel($id);
        $otcc = Otcc::getOtcc($id);
        $templateProcessorAkt = new TemplateProcessor($this->getDir() . "/documents/templates/akt.docx");
        $templateProcessorAkt->setValue('name_org', htmlspecialchars($model->name_org_rod)); //Наименование организации в родительном падеже
        $templateProcessorAkt->setValue('name_org#1', htmlspecialchars($model->name_org)); //Наименование организации
        $templateProcessorAkt->setValue('address', htmlspecialchars($model->address)); //Наименование организации
        $date = date_parse($model->date);
        $templateProcessorAkt->setValue('date', htmlspecialchars(sprintf("%02d", $date['day']) . "." . sprintf("%02d", $date['month']) . "." . $date['year']));
        $templateProcessorAkt->setValue('name_spec', htmlspecialchars(preg_replace('#(.*)\s+(.).*\s+(.).*#usi', '$1 $2.$3.', $model->spec_fio))); // Фамилия кто составил техпаспорт
        $templateProcessorAkt->setValue('position', htmlspecialchars($model->spec));
        $arrNumberRoom = array();
        foreach ($otcc as $item1) {
            array_push($arrNumberRoom, $item1->number_room);
        }
        $templateProcessorAkt->setValue('number_room', htmlspecialchars(implode(", ", $arrNumberRoom)));
        $this->eshkere($otcc, $templateProcessorAkt);
        if($negative)
            $templateProcessorAkt->saveAs($this->getDir() . "/documents/schools/" . Schools::translit($model->short_name_org) . "/negative/akt_" . Schools::translit($model->short_name_org) . ".docx");
        else
            $templateProcessorAkt->saveAs($this->getDir() . "/documents/schools/" . Schools::translit($model->short_name_org) . "/akt_" . Schools::translit($model->short_name_org) . ".docx");
    }

    public function actionZip($id)
    {
        $this->tech_passport($id);
        $this->protokol($id);
        $this->certificate($id);
        $this->finale($id);
        $this->akt($id, false);

        $model = $this->findModel($id);
        $rootPath = realpath($this->getDir() . "/documents/schools/" . Schools::translit($model->short_name_org) . "");
        $zip = new ZipArchive();
        $zip->open($this->getDir() . "/documents/schools/" . Schools::translit($model->short_name_org) . ".zip", ZipArchive::CREATE | ZipArchive::OVERWRITE);

        // Create recursive directory iterator
        /** @var SplFileInfo[] $files */
        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($rootPath),
            RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach ($files as $name => $file) {
            // Skip directories (they would be added automatically)
            if (!$file->isDir()) {
                // Get real and relative path for current file
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($rootPath) + 1);

                // Add current file to archive
                $zip->addFile($filePath, $relativePath);
            }
        }

        // Zip archive will be created only after closing object
        $zip->close();
        Log::generateRCOI($this->findModel($id), "Генерировал все в zip");
        return $this->redirect(['generate-success', 'id' => $id, 'type' => 6]);
    }

    public function actionNegative($id, $type){
        $model = $this->findModel($id);
        $otcc = Otcc::getOtcc($id);
        if (!is_dir($this->getDir() . "/documents/schools/" . Schools::translit($model->short_name_org) . "/negative")) {
            mkdir($this->getDir() .  "/documents/schools/" . Schools::translit($model->short_name_org) . "/negative", 0777, true);
        }
        $this->akt($id, true);
        $project = Projects::findOne($model->project_id);
        $arrNumberRoom = array();
        foreach ($otcc as $item1) {
            array_push($arrNumberRoom, $item1->number_room);
        }
        switch ($type){
            case 1:
                $templateProcessorFinale = new TemplateProcessor($this->getDir() . "/documents/templates/finale_negative_dallas.docx");
                break;
            case 2:
                $templateProcessorFinale = new TemplateProcessor($this->getDir() . "/documents/templates/finale_negative_antivirus.docx");
                break;
            default:
                $templateProcessorFinale = new TemplateProcessor($this->getDir() . "/documents/templates/finale_negative_full.docx");
                break;
        }
        $date = date_parse($model->date);
        $date_create = date_parse($model->date_create);
        $templateProcessorFinale->setValue('date_public', htmlspecialchars("«" . $date['day'] . "» " . Schools::getMonth($date['month']) . " " . $date['year']));
        $templateProcessorFinale->setValue('date_create', htmlspecialchars("«" . $date_create['day'] . "» " . Schools::getMonth($date_create['month']) . " " . $date_create['year']));
        $templateProcessorFinale->setValue('name_org', htmlspecialchars($model->name_org_rod)); //Наименование организации в родительном падеже
        $templateProcessorFinale->setValue('short_name', htmlspecialchars($model->short_name_org)); // Краткое имя организации
        $templateProcessorFinale->setValue('number_finale', htmlspecialchars($model->number_finale)); //Номер документа заключения
        $templateProcessorFinale->setValue('number_protokol', htmlspecialchars($model->number_protokol)); //Номер документа протокола
//        $templateProcessorFinale->setValue('fio_nord', htmlspecialchars(preg_replace('#(.*)\s+(.).*\s+(.).*#usi', '$1', Nord::get($model->spec_nord_id)->fio))); // Фамилия кто составил техпаспорт
        $templateProcessorFinale->setValue('address_org', htmlspecialchars($model->address)); // Адрес организации
        $templateProcessorFinale->setValue('doc_name', htmlspecialchars($project->name_doc)); // Номер документа
        $templateProcessorFinale->setValue('doc_date', htmlspecialchars($model->date)); // Дата документа
        $templateProcessorFinale->setValue('number_room', htmlspecialchars(implode(", ", $arrNumberRoom)));

        $this->eshkere($otcc, $templateProcessorFinale);
        $templateProcessorFinale->saveAs($this->getDir() . "/documents/schools/" . Schools::translit($model->short_name_org) . "/negative/finale_" . Schools::translit($model->short_name_org) . ".docx");

        $rootPath = realpath($this->getDir() . "/documents/schools/" . Schools::translit($model->short_name_org) . "/negative");
        $zip = new ZipArchive();
        $zip->open($this->getDir() . "/documents/schools/" . Schools::translit($model->short_name_org) . "/negative.zip", ZipArchive::CREATE | ZipArchive::OVERWRITE);

        // Create recursive directory iterator
        /** @var SplFileInfo[] $files */
        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($rootPath),
            RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach ($files as $name => $file) {
            // Skip directories (they would be added automatically)
            if (!$file->isDir()) {
                // Get real and relative path for current file
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($rootPath) + 1);

                // Add current file to archive
                $zip->addFile($filePath, $relativePath);
            }
        }

        // Zip archive will be created only after closing object
        $zip->close();
        Log::generateRCOI($this->findModel($id), "Отрицательное заключение");
        return $this->redirect(['generate-success', 'id' => $id, 'type' => 7]);
    }

    /**
     * Deletes an existing Schools model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if (is_dir($this->getDir() . "/documents/schools/" . Schools::translit($model->short_name_org))) {
            rmdir($this->getDir() . "/documents/schools/" . Schools::translit($model->short_name_org));
        }
        $model->delete();
        foreach (Otcc::find()->where(['org_id' => $id])->all() as $otcc) {
            $otcc->delete();
        }
        foreach (Vtcc::find()->where(['org_id' => $id])->all() as $vtcc) {
            $vtcc->delete();
        }
        return $this->redirect(['index']);
    }

    public function actionDownload($id, $type)
    {
        $model = $this->findModel($id);
        $path = Yii::getAlias('@webroot') . '/documents/schools/' . Schools::translit($model->short_name_org) . '/';
        switch ($type) {
            case 1:
                $type_name = 'protokol_';
                break;
            case 2:
                $type_name = 'tech_passport_';
                break;
            case 3:
                $type_name = 'finale_';
                break;
            case 4:
                $type_name = 'certificate_';
                break;
            case 5:
                $type_name = 'akt_';
                break;
            case 6:
                $file = Yii::getAlias('@webroot') . '/documents/schools/' . Schools::translit($model->short_name_org) . '.zip';
                header("Cache-Control: no-store, no-cache, must-revalidate");
                header("Cache-Control: post-check=0, pre-check=0", false);
                header("Pragma: no-cache");
                header("Content-type: application/zip");
                header('Content-length: ' . filesize($file));
                header("Content-disposition: attachment; filename=" . basename($file));
                readfile($file);
                break;
            case 7:
                $file = Yii::getAlias('@webroot') . '/documents/schools/' . Schools::translit($model->short_name_org) . '/negative.zip';
                header("Cache-Control: no-store, no-cache, must-revalidate");
                header("Cache-Control: post-check=0, pre-check=0", false);
                header("Pragma: no-cache");
                header("Content-type: application/zip");
                header('Content-length: ' . filesize($file));
                header("Content-disposition: attachment; filename=" . basename($file));
                readfile($file);
                break;
        }
        if ($type_name != null) {
            $file = $path . $type_name . Schools::translit($model->short_name_org) . '.docx';
            if (file_exists($file)) {
                Yii::$app->response->sendFile($file);
            }
        }
    }

    /**
     * Finds the Schools model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Schools the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Schools::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionDocument()
    {
        $id = Yii::$app->request->post('id');
        $project = Projects::findOne($id);
        if ($project != null) {
            echo $project->name_doc;
        } else {
            echo "";
        }
//        echo $this->get_num_pages_doc($this->getDir() . "/documents/templates/protokol_web.docx");
    }

    function get_num_pages_docx($filename)
    {
        $zip = new ZipArchive();

        if($zip->open($filename) === true)
        {
            if(($index = $zip->locateName('docProps/app.xml')) !== false)
            {
                $data = $zip->getFromIndex($index);
                $zip->close();

                $xml = new SimpleXMLElement($data);
                return $xml->Pages;
            }

            $zip->close();
        }

        return false;
    }

    function getDir()
    {
        return $_SERVER["DOCUMENT_ROOT"];
    }
}
