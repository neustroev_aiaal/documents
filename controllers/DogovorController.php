<?php

namespace app\controllers;

use app\models\NordDogovor;
use app\models\User;
use NCL;
use NCLNameCaseRu;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Settings;
use PhpOffice\PhpWord\TemplateProcessorTest;
use Yii;
use app\models\Dogovor;
use app\models\DogovorSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use PhpOffice\PhpWord\TemplateProcessor;
use yii\filters\VerbFilter;

/**
 * Подключаем необходимый язык
 */

/**
 * DogovorController implements the CRUD actions for Dogovor model.
 */
class DogovorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [User::ROLE_ADMIN],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Dogovor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DogovorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Dogovor model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Dogovor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Dogovor();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Dogovor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Dogovor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function actionGenerateSmart($id)
    {
        require $_SERVER["DOCUMENT_ROOT"].'/Library/NCLNameCaseru.php';
        $case = new NCLNameCaseRu();
        $model = $this->findModel($id);
        if($model->spec_nord==1) {
            $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/smart.docx");
        }else{
            $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/smart_1.docx");
        }

        $templateProcessor->setValue('id', htmlspecialchars($model->dogovor_id)); // On section/content
        $date_public = strtotime($model->date_public);
        $date_begin = strtotime($model->date_begin);
        $date_end = strtotime($model->date_end);
//        var_dump(preg_replace('#(.*)\s+(.).*\s+(.).*#usi', '$1 $2.$3.', $model->fio));
//            die;
        $templateProcessor->setValue('date_public', htmlspecialchars(date("j", $date_public)." ".Dogovor::$MONTHES[date("n", $date_public)] ." ". date("Y", $date_public))); // On section/content
        $templateProcessor->setValue('full_name_org', htmlspecialchars($model->full_name_org)); // On section/content
        $templateProcessor->setValue('ending_name_org', htmlspecialchars(Dogovor::$ENDING_NAME_ORG[$model->ending_name_org]));
        if($model->fio==null) {
            $templateProcessor->setValue('fio', htmlspecialchars("______________________________________________________"));
            $templateProcessor->setValue('ending_fio', htmlspecialchars("____"));
            $templateProcessor->setValue('fio#1', htmlspecialchars("/_________"));
        } else {
            $templateProcessor->setValue('fio', htmlspecialchars($case->q($model->fio, NCL::$RODITLN))); // On section/content
            $templateProcessor->setValue('ending_fio', htmlspecialchars(Dogovor::$ENDING_FIO[$model->ending_fio]));
            $templateProcessor->setValue('fio#1', htmlspecialchars(preg_replace('#(.*)\s+(.).*\s+(.).*#usi', '$1 $2.$3.', $model->fio)));
        }
        if($model->osnovanie==3){
            $templateProcessor->setValue('osnovanie', htmlspecialchars("___________"));
        }else {
            $templateProcessor->setValue('osnovanie', htmlspecialchars(Dogovor::$OSNOVANIE[$model->osnovanie])); // On section/content
        }
        $templateProcessor->setValue('date_begin', htmlspecialchars(date("j", $date_begin)." ".Dogovor::$MONTHES[date("n", $date_begin)] ." ". date("Y", $date_begin)));
        $templateProcessor->setValue('date_end', htmlspecialchars(date("j", $date_end)." ".Dogovor::$MONTHES[date("n", $date_end)] ." ". date("Y", $date_end)));
        $templateProcessor->setValue('short_name_org', htmlspecialchars($model->short_name_org));
        $templateProcessor->setValue('nord_spec', htmlspecialchars(NordDogovor::get($model->spec_nord)->spec));
        $templateProcessor->setValue('nord_fio', htmlspecialchars(NordDogovor::get($model->spec_nord)->fio));
        $templateProcessor->setValue('nord_note', htmlspecialchars(NordDogovor::get($model->spec_nord)->note));
        $templateProcessor->setValue('inn', htmlspecialchars($model->inn));// On section/content
        $templateProcessor->setValue('kpp', htmlspecialchars($model->kpp)); // On section/content
        $templateProcessor->setValue('address', htmlspecialchars($model->address_org)); // On section/content
        $templateProcessor->setValue('pc', htmlspecialchars($model->pc)); // On section/content
        $templateProcessor->setValue('bank', htmlspecialchars($model->bank));
        if($model->kc==null) {
            $templateProcessor->setValue('kc', htmlspecialchars("__________________"));
        }else{
            $templateProcessor->setValue('kc', htmlspecialchars($model->kc));
        }
        $templateProcessor->setValue('bik', htmlspecialchars($model->bik));
        $templateProcessor->setValue('tel', htmlspecialchars($model->tel));
        $templateProcessor->setValue('email', htmlspecialchars($model->email));
        if($model->position_id==3){
            $templateProcessor->setValue('position', htmlspecialchars("___________"));
        }else {
            $templateProcessor->setValue('position', htmlspecialchars(Dogovor::$TYPES[$model->position_id]));
        }
        if($model->position_id==0) {
            $templateProcessor->setValue('position#1', htmlspecialchars("директора"));
        }
        if($model->position_id==1){
            $templateProcessor->setValue('position#1', htmlspecialchars("руководителя"));
        }
        if($model->position_id==2){
            $templateProcessor->setValue('position#1', htmlspecialchars("главы"));
        }
        if($model->position_id==3){
            $templateProcessor->setValue('position#1', htmlspecialchars("___________"));
        }
//
//        $phpword = new \PhpOffice\PhpWord\PhpWord();
//        $section = $phpword->te ($this->getDir() . "/documents/dog/dog".$model->id.".docx");
//        $section->addImage();
//        $templateProcessor->setValue('caption', image(Yii::getAlias('@webroot').'/image/caption/zam.png'));
        $rendererName = Settings::PDF_RENDERER_DOMPDF;
        $rendererLibraryPath = $this->getDir() . '/../vendor/dompdf/dompdf/';
        Settings::setPdfRenderer($rendererName, $rendererLibraryPath);
        $templateProcessor->saveAs($this->getDir() . "/documents/dog/dogovor_smart_".$model->dogovor_id.".docx");
        $temp = IOFactory::load($this->getDir() .'/documents/dog/dogovor_smart_'.$model->dogovor_id.'.docx');
        $xmlWriter = IOFactory::createWriter($temp , 'PDF');
        $xmlWriter->save($this->getDir() .'/documents/dog/dogovor_smart_'.$model->dogovor_id.'.pdf', TRUE);

        return $this->render('generate', [
            'model' => $model,
        ]);
    }

    public function actionGenerateEge($id)
    {
        require $_SERVER["DOCUMENT_ROOT"].'/Library/NCLNameCaseru.php';
        $case = new NCLNameCaseRu();
        $model = $this->findModel($id);
        if($model->spec_nord==1) {
            $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/ege.docx");
        }else{
            $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/ege_1.docx");
        }

        $templateProcessor->setValue('id', htmlspecialchars($model->dogovor_id)); // On section/content
        $date_public = strtotime($model->date_public);
        $date_begin = strtotime($model->date_begin);
        $date_end = strtotime($model->date_end);
        $templateProcessor->setValue('date_public', htmlspecialchars(date("j", $date_public)." ".Dogovor::$MONTHES[date("n", $date_public)] ." ". date("Y", $date_public))); // On section/content
        $templateProcessor->setValue('full_name_org', htmlspecialchars($model->full_name_org)); // On section/content
        $templateProcessor->setValue('ending_name_org', htmlspecialchars(Dogovor::$ENDING_NAME_ORG[$model->ending_name_org]));
        if($model->fio==null) {
            $templateProcessor->setValue('fio', htmlspecialchars("______________________________________________________"));
            $templateProcessor->setValue('ending_fio', htmlspecialchars("____"));
            $templateProcessor->setValue('fio#1', htmlspecialchars("/_________"));
        } else {
            $templateProcessor->setValue('fio', htmlspecialchars($case->q($model->fio, NCL::$RODITLN))); // On section/content
            $templateProcessor->setValue('ending_fio', htmlspecialchars(Dogovor::$ENDING_FIO[$model->ending_fio]));
            $templateProcessor->setValue('fio#1', htmlspecialchars(preg_replace('#(.*)\s+(.).*\s+(.).*#usi', '$1 $2.$3.', $model->fio)));
        }
        if($model->osnovanie==3){
            $templateProcessor->setValue('osnovanie', htmlspecialchars("___________"));
        }else {
            $templateProcessor->setValue('osnovanie', htmlspecialchars(Dogovor::$OSNOVANIE[$model->osnovanie])); // On section/content
        }
        $templateProcessor->setValue('date_begin', htmlspecialchars(date("j", $date_begin)." ".Dogovor::$MONTHES[date("n", $date_begin)] ." ". date("Y", $date_begin)));
        $templateProcessor->setValue('date_end', htmlspecialchars(date("j", $date_end)." ".Dogovor::$MONTHES[date("n", $date_end)] ." ". date("Y", $date_end)));
        $templateProcessor->setValue('short_name_org', htmlspecialchars($model->short_name_org));
        $templateProcessor->setValue('nord_spec', htmlspecialchars(NordDogovor::get($model->spec_nord)->spec));
        $templateProcessor->setValue('nord_fio', htmlspecialchars(NordDogovor::get($model->spec_nord)->fio));
        $templateProcessor->setValue('nord_note', htmlspecialchars(NordDogovor::get($model->spec_nord)->note));
        $templateProcessor->setValue('inn', htmlspecialchars($model->inn));// On section/content
        $templateProcessor->setValue('kpp', htmlspecialchars($model->kpp)); // On section/content
        $templateProcessor->setValue('address', htmlspecialchars($model->address_org)); // On section/content
        $templateProcessor->setValue('pc', htmlspecialchars($model->pc)); // On section/content
        $templateProcessor->setValue('bank', htmlspecialchars($model->bank));
        if($model->kc==null) {
            $templateProcessor->setValue('kc', htmlspecialchars("__________________"));
        }else{
            $templateProcessor->setValue('kc', htmlspecialchars($model->kc));
        }
        $templateProcessor->setValue('bik', htmlspecialchars($model->bik));
        $templateProcessor->setValue('tel', htmlspecialchars($model->tel));
        $templateProcessor->setValue('email', htmlspecialchars($model->email));
        if($model->position_id==3){
            $templateProcessor->setValue('position', htmlspecialchars("___________"));
        }else {
            $templateProcessor->setValue('position', htmlspecialchars(Dogovor::$TYPES[$model->position_id]));
        }
        if($model->position_id==0) {
            $templateProcessor->setValue('position#1', htmlspecialchars("директора"));
        }
        if($model->position_id==1){
            $templateProcessor->setValue('position#1', htmlspecialchars("руководителя"));
        }
        if($model->position_id==2){
            $templateProcessor->setValue('position#1', htmlspecialchars("главы"));
        }
        if($model->position_id==3){
            $templateProcessor->setValue('position#1', htmlspecialchars("___________"));
        }
//
//        $phpword = new \PhpOffice\PhpWord\PhpWord();
//        $section = $phpword->te ($this->getDir() . "/documents/dog/dog".$model->id.".docx");
//        $section->addImage();
//        $templateProcessor->setValue('caption', image(Yii::getAlias('@webroot').'/image/caption/zam.png'));
        $rendererName = Settings::PDF_RENDERER_DOMPDF;
        $rendererLibraryPath = $this->getDir() . '/../vendor/dompdf/dompdf/';
        Settings::setPdfRenderer($rendererName, $rendererLibraryPath);
        $templateProcessor->saveAs($this->getDir() . "/documents/dog/dogovor_ege_".$model->dogovor_id.".docx");
        $temp = IOFactory::load($this->getDir() .'/documents/dog/dogovor_ege_'.$model->dogovor_id.'.docx');
        $xmlWriter = IOFactory::createWriter($temp , 'PDF');
        $xmlWriter->save($this->getDir() .'/documents/dog/dogovor_ege_'.$model->dogovor_id.'.pdf', TRUE);

        return $this->render('generate', [
            'model' => $model,
        ]);
    }

    public function actionDownload($id)
    {
        $path = Yii::getAlias('@webroot') . '/documents/dog';

        $file = $path . '/dog' . $id . '.docx';

        if (file_exists($file)) {

            Yii::$app->response->sendFile($file);

        }
    }

        /**
     * Finds the Dogovor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Dogovor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Dogovor::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    function getDir()
    {
        return $_SERVER["DOCUMENT_ROOT"];
    }
}
