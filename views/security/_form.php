<?php

use app\models\Security;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Security */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="security-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'certificate')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList(Security::$TYPES,['prompt' => 'Выберите тип']); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
