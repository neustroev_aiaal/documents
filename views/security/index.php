<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Security;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \app\models\SecuritySearch */

$this->title = 'Securities';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="security-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Security', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            //'certificate',
            [
                'attribute' => 'type',
                'value' => function(Security $model, $key, $index, $column) {
                    return $model->getTypeName();
                },
                'filter' => Security::$TYPES
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
