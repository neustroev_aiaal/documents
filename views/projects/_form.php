<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Projects */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="projects-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ministr_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'short_name')->textInput(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-xs-8">
            <?= $form->field($model, 'name_doc')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-4">
            <?= $form->field($model, 'date_doc', ['template'=>"{label}\n<div class=\"input-group\"><span class=\"input-group-addon\">от</span>{input}\n</div>\n{hint}\n{error}"])->widget(DatePicker::className(), [
                'options' => ['placeholder' => 'Введите дату разработки документа'],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true
                ]
            ]);
            ?>
        </div>
    </div>

    <?= $form->field($model, 'date_start_project')->widget(DatePicker::className(), [
        'options' => ['placeholder' => 'Введите дату'],
        'pluginOptions' => [
            'autoclose'=>true
        ]
    ]);
    ?>

    <?= $form->field($model, 'date_end_project')->widget(DatePicker::className(), [
        'options' => ['placeholder' => 'Введите дату'],
        'pluginOptions' => [
            'autoclose'=>true
        ]
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
