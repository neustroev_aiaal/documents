<?php

use app\models\Schools;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Schools */

$this->title = 'Добавить школу';
$this->params['breadcrumbs'][] = ['label' => 'Школы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="schools-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model, 'create' => true,
    ]) ?>

</div>
