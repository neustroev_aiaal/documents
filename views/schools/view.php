<?php

use app\components\Region;
use app\models\Otcc;
use app\models\Schools;
use app\models\User;
use app\models\Vtcc;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Schools */

$this->title = $model->short_name_org;
$canEdit = Schools::canBeEdited(\app\components\Y::user());
$canEditOperator = User::canBeEditedOperator(\app\components\Y::user());
$this->params['breadcrumbs'][] = ['label' => 'Школы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="schools-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if ($canEdit): ?>
            <?= Html::a('Генерировать', ['generate', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?php endif ?>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Добавить ОТСС', ['/otcc/create', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php if ($canEdit): ?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif ?>
    </p>
    <div class="row">
        <div class="col-md-9">
            <?php if ($canEditOperator): ?>
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'project_id',
//                [
//                    'attribute' => 'project_id',
//                    'value' => $model->projects->short_name,
//                    'format' => 'raw',
//                ],
                        [
                            'attribute' => 'region_id',
                            'value' => function (Schools $model) {
                                if ($model->region_id == null)
                                    return '(Не задан)';
                                return Region::getById($model->region_id)->name;
                            },
                            'format' => 'raw',
                        ],
                        'inn',
                        'name_org',
                        'name_org_rod',
                        'short_name_org',
                        'address',
                        'position_director',
                        'school_director',
                        'date',
                        'date_create',
//                        'number_techpassport',
//                        'number_protokol',
//                        'number_finale',
                        'number_certificate',
                        'spec_fio',
                        'spec',
                        [
                            'attribute' => 'user_id',
                            'value' => $model->user->username,
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'plan_room',
                            'value' => $model->plan_room ? 'есть':'нету',
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'plan_building',
                            'value' => $model->plan_building ? 'есть':'нету',
                            'format' => 'raw',
                        ],
                        'note'
//                [
//                    'attribute' => 'spec_nord_id',
//                    'value' => $model->nord->fio,
//                    'format' => 'raw',
//                ]
                    ],
                ]) ?>
            <?php endif ?>

            <?php if (!$canEditOperator): ?>
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        [
                            'attribute' => 'region_id',
                            'value' => function (Schools $model) {
                                if ($model->region_id == null)
                                    return '(Не задан)';
                                return Region::getById($model->region_id)->name;
                            },
                            'format' => 'raw',
                        ],
                        'inn',
                        'name_org',
                        'name_org_rod',
                        'short_name_org',
                        'address',
                        'position_director',
                        'school_director',
                        'spec_fio',
                        'spec',
                        [
                            'attribute' => 'plan_room',
                            'value' => $model->plan_room ? 'есть':'нету',
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'plan_building',
                            'value' => $model->plan_building ? 'есть':'нету',
                            'format' => 'raw',
                        ],
                        'note'
                    ],
                ])
                ?>
            <?php endif ?>
        </div>
        <div class="col-md-3">
            <?php if (Otcc::getOtcc($model->id)): ?>
                <h4>ОТСС:</h4>
                <ul>
                    <?php foreach (Otcc::getOtcc($model->id) as $school): ?>
                        <li>
                            <a style="font-size: medium" href="<?= Url::to(['otcc/view', 'id' => $school->id]) ?>">
                                <?= Html::encode($school->pc_name) ?>
                            </a>
                        </li>
                    <?php endforeach ?>
                </ul>
            <?php endif ?>
            <?php if (Vtcc::getVtccOrg($model->id)): ?>
                <h4>ВТСС:</h4>
                <ul>
                    <?php foreach (Vtcc::getVtccOrg($model->id) as $school): ?>
                        <li>
                            <a style="font-size: medium" href="<?= Url::to(['vtcc/view', 'id' => $school->id]) ?>">
                                <?= Html::encode($school->pc_name) ?>
                            </a>
                        </li>
                    <?php endforeach ?>
                </ul>
            <?php endif ?>
        </div>
    </div>

</div>
