<?php

use app\models\Nord;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\Schools */
/* @var $form yii\widgets\ActiveForm */

$this->title = $model->short_name_org;
$this->params['breadcrumbs'][] = ['label' => 'Организации', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['schools/view?id='.$model->id]];
$this->params['breadcrumbs'][] = ['label' => 'генерировать', 'url' => ['schools/generate?id='.$model->id]];
$this->params['type'] = $type;
$types = ['protokol', 'tech-passport', 'finale', 'certificate', 'akt', 'zip', 'negative'];
?>
<div class="schools-view">
    <h3>Файл успешно генерирован</h3>
    <?= Html::a('Скачать', ['/schools/download?id='.$model->id.'&type='.$type.''], ['class'=>'btn btn-success btn-lg']) ?>
    <?php if($type!=7):?>
    <?= Html::a('Повторно генерировать', ['/schools/'.$types[$type-1].'?id='.$model->id], ['class'=>'btn btn-info btn-lg']) ?>
    <?php endif; ?>
</div>