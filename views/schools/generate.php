<?php

use app\models\Nord;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\models\Schools */
/* @var $form yii\widgets\ActiveForm */

$this->title = $model->short_name_org;
$this->params['breadcrumbs'][] = ['label' => 'Организации', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['schools/view?id=' . $model->id]];


?>
<div class="schools-view">
    <div class="row">
        <div class="col-md-4">
            <a href="/schools/protokol?id=<?php echo $model->id ?>">
                <button style="margin: 4px" class="btn btn-primary btn-block btn-lg">Протокол</button>
            </a>
            <a href="/schools/tech-passport?id=<?php echo $model->id ?>">
                <button style="margin: 4px" class="btn btn-primary btn-block btn-lg">Тех паспорт</button>
            </a>
            <a href="/schools/finale?id=<?php echo $model->id ?>">
                <button style="margin: 4px" class="btn btn-primary btn-block btn-lg">Заключение</button>
            </a>
        </div>
        <div class="col-md-4">
            <a href="/schools/certificate?id=<?php echo $model->id ?>">
                <button style="margin: 4px" class="btn btn-primary btn-block btn-lg">Аттестат</button>
            </a>
            <a href="/schools/akt?id=<?php echo $model->id ?>">
                <button style="margin: 4px" class="btn btn-primary btn-block btn-lg">Акт</button>
            </a>
            <a href="/schools/zip?id=<?php echo $model->id ?>">
                <button style="margin: 4px" class="btn btn-primary btn-block btn-lg">Генерировать все в zip</button>
            </a>
        </div>
        <div class="col-md-4">

            <?php
            Modal::begin([
                'header' => '<h2>Отрицательный аттестат</h2>',
                'toggleButton' => [
                    'label' => 'Отрицательный аттестат',
                    'tag' => 'button',
                    'style' => 'margin: 4px',
                    'class' => 'btn btn-danger btn-block btn-lg',
                ],
            ]);

            echo '
    <form method="get" action="/schools/negative">
    <input type="hidden" name="id" value="' . $model->id . '">
    <input type="radio" name="type" checked value="1">Нету Dallas Lock <Br>
    <input type="radio" name="type" value="2">Нету антивируса<Br> 
    <input type="radio" name="type" value="3">Нету далласа и антивируса<Br> <br>
    <button type="submit" class="btn btn-success">Генерировать документ</button>
    </form>';

            Modal::end(); ?>
        </div>
    </div>
</div>
