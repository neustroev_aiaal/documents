<?php

use app\components\Region;
use app\models\Projects;
use app\models\User;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Schools */
/* @var $form yii\widgets\ActiveForm */

$this->params['create'] = $create;
$canEdit = User::canBeEditedOperator(\app\components\Y::user());
?>

<div class="schools-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php if ($canEdit): ?>
        <?= $form->field($model, 'project_id')->dropDownList(ArrayHelper::map(Projects::getList(), 'id', 'short_name'),
            [
                'prompt' => 'Выберите проект',
                'onchange' => '$.post("' . Url::toRoute('schools/document') . '", {id: $(this).val()}, 
            function(data){
                $("input#schools-number_protokol").val(data);
                $("input#schools-number_techpassport").val(data);
                $("input#schools-number_certificate").val(data);
                $("input#schools-number_finale").val(data); 
            })'
            ]); ?>
    <?php endif ?>

    <?= $form->field($model, 'region_id')->dropDownList(ArrayHelper::map(Region::$LIST, 'id', 'name'), ['prompt' => 'Выберите район (улус)']); ?>

    <?= $form->field($model, 'inn', ['template' => "{label}\n<div class=\"input-group\">{input}\n<span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-question-sign\" data-container=\"body\" data-toggle=\"popover\" style='cursor: pointer' data-placement=\"right\" data-content=\"ИНН организации в соответствии с учредительными документами.\"></span></span></div>\n{hint}\n{error}"])->textInput(['maxlength' => true, 'placeholder' => 'ИНН организации', 'inputTemplate' => '<div class="input-group"><span class="input-group-addon">@</span>{input}</div>']) ?>

    <?= $form->field($model, 'name_org', ['template' => "{label}\n<div class=\"input-group\">{input}\n<span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-question-sign\" data-container=\"body\" data-toggle=\"popover\" style='cursor: pointer' data-placement=\"right\" data-content=\"Наименование школы в соответствии с учредительными документами.\"></span></span></div>\n{hint}\n{error}"])->textInput(['maxlength' => true, 'placeholder' => 'Муниципальное бюджетное общеобразовательное учреждение "средняя общеобразовательная школа города А №1"', 'inputTemplate' => '<div class="input-group"><span class="input-group-addon">@</span>{input}</div>']) ?>

    <?= $form->field($model, 'name_org_rod', ['template' => "{label}\n<div class=\"input-group\">{input}\n<span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-question-sign\" data-container=\"body\" data-toggle=\"popover\" style='cursor: pointer' data-placement=\"right\" data-content=\"Наименование школы в соответствии с учредительными документами в родительном падеже\"></span></span></div>\n{hint}\n{error}"])->textInput(['maxlength' => true, 'placeholder' => 'Муниципального бюджетного общеобразовательного учреждения "средней общеобразовательной школы города А №1"', 'inputTemplate' => '<div class="input-group"><span class="input-group-addon">@</span>{input}</div>']) ?>

    <?= $form->field($model, 'short_name_org', ['template' => "{label}\n<div class=\"input-group\">{input}\n<span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-question-sign\" data-container=\"body\" data-toggle=\"popover\" style='cursor: pointer' data-placement=\"right\" data-content=\"Краткое наименование школы в соответствии с учредительными документами.\"></span></span></div>\n{hint}\n{error}"])->textInput(['maxlength' => true, 'placeholder' => 'ЕГЭ А СОШ №1']) ?>

    <?= $form->field($model, 'address', ['template' => "{label}\n<div class=\"input-group\">{input}\n<span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-question-sign\" data-container=\"body\" data-toggle=\"popover\" style='cursor: pointer' data-placement=\"right\"  data-content=\"Адрес с индексом\"></span></span></div>\n{hint}\n{error}"])->textInput(['maxlength' => true, 'placeholder' => '677080, Республика Саха (Якутия), E улус, г. A, ул. Дзержинского, д. 46.']) ?>

    <h3>Руководитель</h3>
    <?= $form->field($model, 'position_director', ['template' => "{label}\n<div class=\"input-group\">{input}\n<span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-question-sign\" data-container=\"body\" data-toggle=\"popover\" style='cursor: pointer' data-placement=\"right\"  data-content=\"Должность руководителя школы\"></span></span></div>\n{hint}\n{error}"])->textInput(['maxlength' => true, 'placeholder' => 'Директор']) ?>

    <?= $form->field($model, 'school_director', ['template' => "{label}\n<div class=\"input-group\">{input}\n<span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-question-sign\" data-container=\"body\" data-toggle=\"popover\" style='cursor: pointer' data-placement=\"right\" data-content=\"ФИО руководителя школы\"></span></span></div>\n{hint}\n{error}"])->textInput(['maxlength' => true, 'placeholder' => 'Иванов Иван Иванович']) ?>

    <h3>Ответственный за ЕГЭ/ОГЭ</h3>

    <?= $form->field($model, 'spec', ['template' => "{label}\n<div class=\"input-group\">{input}\n<span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-question-sign\" data-container=\"body\" data-toggle=\"popover\" style='cursor: pointer' data-placement=\"right\" data-content=\"Должность специалиста согласно документов о назначении (трудовой договор, приказ и т.д.).\"></span></span></div>\n{hint}\n{error}"])->textInput(['maxlength' => true, 'placeholder' => 'Должность ответсвенного за ЕГЭ/ОГЭ']) ?>

    <?= $form->field($model, 'spec_fio', ['template' => "{label}\n<div class=\"input-group\">{input}\n<span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-question-sign\" data-container=\"body\" data-toggle=\"popover\" style='cursor: pointer' data-placement=\"right\" data-content=\"ФИО специалиста согласно документов о назначении (трудовой договор, приказ и т.д.).\"></span></span></div>\n{hint}\n{error}"])->textInput(['maxlength' => true, 'placeholder' => 'Иванов Иван Иванович']) ?>

    <?php if ($canEdit): ?>

        <h3>Даты</h3>

        <?= $form->field($model, 'date')->widget(DatePicker::className(), [
            'options' => ['placeholder' => 'Введите дату утверждения документа'],
            'pluginOptions' => [
                'autoclose' => true
            ]
        ]);
        ?>

        <?= $form->field($model, 'date_create')->widget(DatePicker::className(), [
            'options' => ['placeholder' => 'Введите дату разработки документа'],
            'pluginOptions' => [
                'autoclose' => true
            ]
        ]);
        ?>

        <h3>Документы</h3>

        <?= $form->field($model, 'number_techpassport')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'number_protokol')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'number_finale')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'number_certificate')->textInput(['maxlength' => true]) ?>

    <?php endif ?>

    <?= $form->field($model, 'plan_room')->checkbox() ?>

    <?= $form->field($model, 'plan_building')->checkbox() ?>

    <?= $form->field($model, 'note')->textarea(['maxlength' => true, 'placeholder' => 'Примечание']) ?>

    <div class="form-group">
        <input type='hidden' id="next" name='next' value=''>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        <?php if ($create) { ?>
            <?= Html::submitButton('Далее', ['class' => 'btn btn-primary', 'id' => 'btn_next']) ?>
        <?php } ?>
    </div>


    <?php ActiveForm::end(); ?>

</div>
