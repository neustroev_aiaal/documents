<?php


use app\models\Log;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Логи';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['logs']];
?>
<div class="row">
    <div class="col-md-10">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        Идентификатор - ID пользователя/файла.
        <?php
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [

                /*[
                    'attribute' => 'region_id',
                    'value' => function(Organization $model, $key, $index, $column) {
                        return Region::getById($model->region_id)->name;
                    },
                    'filter' => ArrayHelper::map(Region::$LIST, 'id', 'name')
                ],*/
                [
                    'attribute' => 'type',
                    'value' => 'typeName',
                    'filter' => Log::$TYPES
                ],
                'info_id',
                'info',
                'created',
//                'ip',
                'user_agent'
            ],
        ]);
        ?>
    </div>
</div>