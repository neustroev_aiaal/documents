<?php

use app\models\fis\FisOtcc;
use app\models\fis\FisSchools;
use app\models\Schools;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\fis\FisOtccSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ОТСС (ФИС ФРДО)';
$canEdit = Schools::canBeEdited(\app\components\Y::user());
$this->params['breadcrumbs'][] = ['label' => 'ФИС ФРДО', 'url' => ['/fis']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fis-otcc-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php
    if ($canEdit):
        ?>
        <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute'=>'org_id',
                'value'=>'schools.short_name_org',
                'filter'=>ArrayHelper::map(FisSchools::getList(), 'id', 'short_name_org'),
            ],
            [
                'attribute' => 'pc_name',
                'format' => 'raw',
                'value' => function (FisOtcc $model) {
                    return Html::a($model->pc_name, ['view', 'id' => $model->id]);
                }
            ],
            [
                'attribute' => 'pc_type',
                'value' => function(FisOtcc $model, $key, $index, $column) {
                    return $model->getTypeName();
                },
                'filter' => FisOtcc::$TYPES
            ],
//            'data',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php endif ?>

    <?php if (!$canEdit):?>
        <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'pc_name',
                'format' => 'raw',
                'value' => function (FisOtcc $model) {
                    return Html::a($model->pc_name, ['view', 'id' => $model->id]);
                }
            ],
            [
                'attribute' => 'pc_type',
                'value' => function(FisOtcc $model, $key, $index, $column) {
                    return $model->getTypeName();
                },
                'filter' => FisOtcc::$TYPES
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}'],
        ],
    ]); ?>
    <?php endif ?>
</div>
