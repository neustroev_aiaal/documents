<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\fis\FisOtcc */

$this->title = 'Изменение ОТСС:'.$model->pc_name.'';
$this->params['breadcrumbs'][] = ['label' => 'ФИС ФРДО', 'url' => ['/fis']];
$this->params['breadcrumbs'][] = ['label' => 'ОТСС', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменение';
?>
<div class="fis-otcc-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model, 'create' => false,
    ]) ?>

</div>
