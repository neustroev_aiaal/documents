<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\fis\FisOtccSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fis-otcc-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'org_id') ?>

    <?= $form->field($model, 'pc_name') ?>

    <?= $form->field($model, 'pc_type') ?>

    <?= $form->field($model, 'number_room') ?>

    <?php // echo $form->field($model, 'data_software') ?>

    <?php // echo $form->field($model, 'data') ?>

    <?php // echo $form->field($model, 'data_iis') ?>

    <?php // echo $form->field($model, 'ip_address') ?>

    <?php // echo $form->field($model, 'mac_address') ?>

    <?php // echo $form->field($model, 'os') ?>

    <?php // echo $form->field($model, 'user_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
