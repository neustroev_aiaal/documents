<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\fis\FisOtcc */

$this->title = 'Добавить ОТСС (ФИС ФРДО)';
$this->params['breadcrumbs'][] = ['label' => 'ФИС ФРДО', 'url' => ['/fis']];
$this->params['breadcrumbs'][] = ['label' => 'ОТСС', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fis-otcc-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model, 'create' => true,
    ]) ?>

</div>
