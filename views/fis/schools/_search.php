<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\fis\FisSchoolsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fis-schools-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'region_id') ?>

    <?= $form->field($model, 'name_org') ?>

    <?= $form->field($model, 'name_org_rod') ?>

    <?= $form->field($model, 'short_name_org') ?>

    <?php // echo $form->field($model, 'inn') ?>

    <?php // echo $form->field($model, 'school_director') ?>

    <?php // echo $form->field($model, 'position_director') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'number_protokol') ?>

    <?php // echo $form->field($model, 'number_techpassport') ?>

    <?php // echo $form->field($model, 'number_certificate') ?>

    <?php // echo $form->field($model, 'number_finale') ?>

    <?php // echo $form->field($model, 'spec') ?>

    <?php // echo $form->field($model, 'spec_fio') ?>

    <?php // echo $form->field($model, 'project_id') ?>

    <?php // echo $form->field($model, 'user_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
