<?php

use app\models\Nord;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Schools */
/* @var $form yii\widgets\ActiveForm */

$this->title = $model->short_name_org;
$this->params['breadcrumbs'][] = ['label' => 'Организации', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['/fis/schools/view?id=' . $model->id]];


?>
<div class="schools-view">
    <div class="row">
        <div class="col-md-4">
            <a href="/fis/schools/protokol?id=<?php echo $model->id ?>">
                <button style="margin: 4px" class="btn btn-primary btn-block btn-lg">Протокол</button>
            </a>
            <a href="/fis/schools/tech-passport?id=<?php echo $model->id ?>">
                <button style="margin: 4px" class="btn btn-primary btn-block btn-lg">Тех паспорт</button>
            </a>
            <a href="/fis/schools/finale?id=<?php echo $model->id ?>">
                <button style="margin: 4px" class="btn btn-primary btn-block btn-lg">Заключение</button>
            </a>
        </div>
        <div class="col-md-4">
            <a href="/fis/schools/certificate?id=<?php echo $model->id ?>">
                <button style="margin: 4px" class="btn btn-primary btn-block btn-lg">Аттестат</button>
            </a>
            <a href="/fis/schools/akt?id=<?php echo $model->id ?>">
                <button style="margin: 4px" class="btn btn-primary btn-block btn-lg">Акт</button>
            </a>
            <a href="/fis/schools/prikazy?id=<?php echo $model->id ?>">
                <button style="margin: 4px" class="btn btn-primary btn-block btn-lg">Приказы</button>
            </a>
        </div>
        <div class="col-md-4">
            <a href="/fis/schools/manual?id=<?php echo $model->id ?>">
                <button style="margin: 4px" class="btn btn-primary btn-block btn-lg">Руководство</button>
            </a>
            <a href="/fis/schools/other-fis-doc?id=<?php echo $model->id ?>">
                <button style="margin: 4px" class="btn btn-primary btn-block btn-lg">Другие документы(правила, акт, техпроцесс)</button>
            </a>
            <a href="/fis/schools/zip?id=<?php echo $model->id ?>">
                <button style="margin: 4px" class="btn btn-primary btn-block btn-lg">Генерировать все в zip</button>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <a href="/fis/schools/license-vipnet?id=<?php echo $model->id ?>">
                <button style="margin: 4px" class="btn btn-primary btn-block btn-lg">Лицензия VipNet Client</button>
            </a>
        </div>
    </div>
</div>
