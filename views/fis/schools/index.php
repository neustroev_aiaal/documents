<?php

use app\components\Region;
use app\models\fis\FisSchools;
use app\models\Schools;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\fis\FisSchoolsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Школы/уо ФИС ФРДО';
$canEdit = Schools::canBeEdited(\app\components\Y::user());
$this->params['breadcrumbs'][] = ['label' => 'ФИС ФРДО', 'url' => ['fis/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fis-schools-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php
    if ($canEdit):
        ?>
        <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute'=>'region_id',
                'value'=>function (FisSchools $model) {
                    if($model->region_id == null)
                        return '(не задано)';
                    return Region::getById($model->region_id)->name;
                },
                'filter'=>ArrayHelper::map(Region::$LIST, 'id', 'name'),
            ],
            'inn',
            [
                'attribute' => 'name_org',
                'format' => 'raw',
                'value' => function (FisSchools $model) {
                    return Html::a($model->name_org, ['view', 'id' => $model->id]);
                }
            ],
            'short_name_org',
            'address',
            [
                'class' => 'yii\grid\ActionColumn'
            ],
        ],
    ]); ?>
    <?php
    endif
    ?>

    <?php
    if (!$canEdit):
        ?>
        <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'inn',
            [
                'attribute' => 'name_org',
                'format' => 'raw',
                'value' => function (FisSchools $model) {
                    return Html::a($model->name_org, ['view', 'id' => $model->id]);
                }
            ],
            'short_name_org',
            'address',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}'
            ],
        ],
    ]); ?>
    <?php
    endif
    ?>
</div>
