<?php

use app\components\Region;
use app\models\fis\FisOtcc;
use app\models\fis\FisSchools;
use app\models\Schools;
use app\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\fis\FisSchools */

$this->title = $model->short_name_org;
$canEdit = Schools::canBeEdited(\app\components\Y::user());
$canEditOperator = User::canBeEditedOperator(\app\components\Y::user());
$this->params['breadcrumbs'][] = ['label' => 'Школы/уо ФИС ФРДО', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fis-schools-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if ($canEdit): ?>
            <?= Html::a('Генерировать', ['generate', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?php endif ?>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Добавить ОТСС', ['/fis/otcc/create', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php if ($canEdit): ?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif ?>
    </p>
    <div class="row">
        <div class="col-md-9">
            <?php if ($canEditOperator): ?>
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'project_id',
//                [
//                    'attribute' => 'project_id',
//                    'value' => $model->projects->short_name,
//                    'format' => 'raw',
//                ],
                        [
                            'attribute' => 'region_id',
                            'value' => function (FisSchools $model) {
                                if ($model->region_id == null)
                                    return '(Не задан)';
                                return Region::getById($model->region_id)->name;
                            },
                            'format' => 'raw',
                        ],
                        'number_contract',
                        'date_contract',
                        'inn',
                        'name_org',
                        'name_org_rod',
                        'short_name_org',
                        'address',
                        'position_director',
                        'school_director',
                        'date',
                        'date_create',
//                        'number_techpassport',
//                        'number_protokol',
//                        'number_finale',
                        'number_certificate',
                        'spec_fio',
                        'spec',
                        [
                            'attribute' => 'user_id',
                            'value' => $model->user->username,
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'plan_room',
                            'value' => $model->plan_room ? 'есть':'нету',
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'plan_building',
                            'value' => $model->plan_building ? 'есть':'нету',
                            'format' => 'raw',
                        ],
                        'reg_number',
                        'date_certificate_vipnet',
                        'note'
//                [
//                    'attribute' => 'spec_nord_id',
//                    'value' => $model->nord->fio,
//                    'format' => 'raw',
//                ]
                    ],
                ]) ?>
            <?php endif ?>

            <?php if (!$canEditOperator): ?>
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        [
                            'attribute' => 'region_id',
                            'value' => function (FisSchools $model) {
                                if ($model->region_id == null)
                                    return '(Не задан)';
                                return Region::getById($model->region_id)->name;
                            },
                            'format' => 'raw',
                        ],
                        'number_contract',
                        'date_contract',
                        'inn',
                        'name_org',
                        'name_org_rod',
                        'short_name_org',
                        'address',
                        'position_director',
                        'school_director',
                        'spec_fio',
                        'spec',
                        [
                            'attribute' => 'plan_room',
                            'value' => $model->plan_room ? 'есть':'нету',
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'plan_building',
                            'value' => $model->plan_building ? 'есть':'нету',
                            'format' => 'raw',
                        ],
                        'note'
                    ],
                ]) ?>
            <?php endif ?>
        </div>
        <div class="col-md-3">
            <?php if (FisOtcc::getOtcc($model->id)): ?>
                <h4>ОТСС:</h4>
                <ul>
                    <?php foreach (FisOtcc::getOtcc($model->id) as $school): ?>
                        <li>
                            <a style="font-size: medium" href="<?= Url::to(['/fis/otcc/view', 'id' => $school->id]) ?>">
                                <?= Html::encode($school->pc_name) ?>
                            </a>
                        </li>
                    <?php endforeach ?>
                </ul>
            <?php endif ?>
        </div>
    </div>

</div>
