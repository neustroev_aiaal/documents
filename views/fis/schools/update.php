<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\fis\FisSchools */

$this->title = 'Изменение: '.$model->short_name_org;
$this->params['breadcrumbs'][] = ['label' => 'Школы/уо ФИС ФРДО', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->short_name_org, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="fis-schools-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,'create' => false,
    ]) ?>

</div>
