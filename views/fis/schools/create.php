<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\fis\FisSchools */

$this->title = 'Добавить школу/уо ФИС ФРДО';
$this->params['breadcrumbs'][] = ['label' => 'Школы/уо ФИС ФРДО', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fis-schools-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model, 'create' => true,
    ]) ?>

</div>
