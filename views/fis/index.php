<?php

/* @var $this yii\web\View */

use app\models\fis\FisSchools;
use app\models\Schools;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Аттестация';
?>
<div class="site-index">

    <div style="margin: 20px;">

        <p class="lead">Сайт для заполнения данных</p>

    </div>


    <div class="row">
        <div class="col-md-8">
            <div class="body-content">
                <a href="/fis/schools/create">
                    <div style="
                width: 300px; /* Ширина слоя */
                margin: 7px; /* Значение отступов */
                padding: 10px; /* Поля вокруг текста */
                background:  #4da6ff;
                 border-radius: 8px;/* Цвет фона */
">
                        <p style="color: black; text-align: center;"><span class="glyphicon glyphicon-pencil"></span> Аттестация рабочих мест для ФИС ФРДО</p>
                    </div>
                </a>
            </div>
            <?php if(\app\components\Y::user()): ?>
                <?php if(FisSchools::getListUs(\app\components\Y::user()->id)!=null): ?>
                    <div>
                        <h4>Добавленная вами школа:</h4>
                        <ul>
                            <?php foreach (FisSchools::getListUs(\app\components\Y::user()->id) as $school): ?>
                                <li>
                                    <a style="font-size: medium" href="<?= Url::to(['/fis/schools/view', 'id' => $school->id]) ?>">
                                        <?= Html::encode($school->short_name_org) ?>
                                    </a>
                                </li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                <?php endif ?>
            <?php endif ?>
        </div>
        <div class="col-md-4">
            <h1>ФИС ФРДО</h1>
<!--            <a href="/fis/schools/index"><h1>Школы</h1></a>-->
<!--            <a href="/fis/otcc/index"><h1>ОТСС</h1></a>-->
        </div>
    </div>
</div>
