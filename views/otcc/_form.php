<?php

use app\models\Otcc;
use app\models\Peripheral;
use app\models\Schools;
use app\models\Security;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use unclead\multipleinput\MultipleInput;

/* @var $this yii\web\View */
/* @var $model app\models\Otcc */
/* @var $form yii\widgets\ActiveForm */
$this->params['create'] = $create;
$canEdit = Schools::canBeEdited(\app\components\Y::user());
$user_id = \Yii::$app->user->id;
?>

<div class="otcc-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php if (!$canEdit): ?>
        <?= $form->field($model, 'org_id')->widget(Select2::className(), ['data' => ArrayHelper::map(Schools::getListUs(\Yii::$app->user->id), 'id', 'short_name_org'),
            'options' => ['placeholder' => 'Выберите школу'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    <?php endif ?>
    <?php if ($canEdit): ?>
        <?= $form->field($model, 'org_id')->widget(Select2::className(), ['data' => ArrayHelper::map(Schools::getList(), 'id', 'short_name_org'),
            'options' => ['placeholder' => 'Выберите школу'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    <?php endif ?>

    <?= $form->field($model, 'pc_name', ['template' => "{label}\n<div class=\"input-group\">{input}\n<span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-question-sign\" data-container=\"body\" data-toggle=\"popover\" style='cursor: pointer' data-placement=\"right\" data-content=\"Наименование компьютера. Мой компьютер->Свойства->Имя компьютера\"></span></span></div>\n{hint}\n{error}"])->textInput(['maxlength' => true, 'placeholder' => 'Имя компьютера']) ?>

    <?= $form->field($model, 'ip_address', ['template' => "{label}\n<div class=\"input-group\">{input}\n<span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-question-sign\" data-container=\"body\" data-toggle=\"popover\" style='cursor: pointer' data-placement=\"right\" data-content=\"IP адрес. Командная строка->ipconfig\"></span></span></div>\n{hint}\n{error}"])->textInput(['maxlength' => true, 'placeholder' => 'IP адрес']) ?>

    <?= $form->field($model, 'mac_address', ['template' => "{label}\n<div class=\"input-group\">{input}\n<span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-question-sign\" data-container=\"body\" data-toggle=\"popover\" style='cursor: pointer' data-placement=\"right\" data-content=\"MAC адрес. Командная строка->getmac\"></span></span></div>\n{hint}\n{error}"])->textInput(['maxlength' => true, 'placeholder' => 'MAC адрес']) ?>

    <?= $form->field($model, 'pc_type', ['template' => "{label}\n<div class=\"input-group\">{input}\n<span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-question-sign\" data-container=\"body\" data-toggle=\"popover\" style='cursor: pointer' data-placement=\"right\" data-content=\"Тип вашего компьютера: Персональный компьютер, Ноутбук, Моноблок\"></span></span></div>\n{hint}\n{error}"])->dropDownList(Otcc::$TYPES, ['prompt' => 'Выберите тип']); ?>

    <?= $form->field($model, 'number_room', ['template' => "{label}\n<div class=\"input-group\">{input}\n<span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-question-sign\" data-container=\"body\" data-toggle=\"popover\" style='cursor: pointer' data-placement=\"right\" data-content=\"Кабинет, где находиться компьютер для ЕГЭ/ОГЭ\"></span></span></div>\n{hint}\n{error}"])->textInput(['maxlength' => true, 'placeholder' => '123']) ?>

    <?= $form->field($model, 'data')->widget(MultipleInput::className(), [
        'max' => 15,
        'columns' => [
            [
                'name' => 'peripheral',
                'type' => 'dropDownList',
                'title' => 'Переферийное устройство',
                'defaultValue' => 1,
                'items' => ArrayHelper::map(Peripheral::getList(), 'id', 'name')
            ],
            [
                'name' => 'model',
                'title' => 'Модель',
                'enableError' => true,
                'options' => [
                    'class' => 'input-priority'
                ]
            ],
            [
                'name' => 'serial_number',
                'title' => 'Заводской номер',
                'enableError' => true,
                'options' => [
                    'class' => 'input-priority'
                ]
            ]
        ]
    ]);
    ?>
    <label>Операционная система</label>
    <div class="row">
        <div class="col-md-3">
            <label>Наименование</label>
            <input class="form-control" id="os-1" required name="os_name">
        </div>
        <div class="col-md-3">
            <label>Версия</label>
            <input class="form-control" id="os-2" required name="os_version">
        </div>
        <div class="col-md-3">
            <label>Сборка</label>
            <input class="form-control" id="os-3" required name="os_build">
        </div>
        <div class="col-md-3">
            <label>Номер лицензии</label>
            <input class="form-control" id="os-4" name="os_serial">
        </div>
    </div>
    <p>
    <?= $form->field($model, 'data_software')->widget(MultipleInput::className(), [
        'max' => 6,
        'columns' => [
            [
                'name' => 'type_soft',
                'type' => 'dropDownList',
                'title' => 'Тип программного обеспечения',
                'defaultValue' => 2,
                'items' => Otcc::$SOFT
            ],
            [
                'name' => 'name',
                'title' => 'Наименование программ',
                'enableError' => true,
                'options' => [
                    'class' => 'input-priority'
                ]
            ],
            [
                'name' => 'serial_number',
                'title' => 'Номер лицензии',
                'enableError' => true,
                'options' => [
                    'class' => 'input-priority'
                ]
            ]
        ]
    ]);
    ?>

    <?= $form->field($model, 'data_iis')->widget(MultipleInput::className(), [
        'max' => 6,
        'columns' => [
            [
                'name' => 'type_soft',
                'type' => 'dropDownList',
                'title' => 'Тип программного обеспечения',
                'defaultValue' => 1,
                'items' => ArrayHelper::map(Security::getAllList(), 'id', 'name')
            ],
            [
                'name' => 'serial_number',
                'title' => 'Номер лицензии',
                'enableError' => true,
                'options' => [
                    'class' => 'input-priority'
                ]
            ],
            [
                'name' => 'mark_number',
                'title' => 'Номер знака соответствия',
                'enableError' => true,
                'options' => [
                    'class' => 'input-priority'
                ]
            ]
        ]
    ]);
    ?>

    <?= $form->field($model, 'note')->textarea(['maxlength' => true, 'placeholder' => 'Примечание']) ?>

    <div class="form-group">
        <input type='hidden' id="next" name='next' value=''>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        <?php if ($create) { ?>
            <?= Html::submitButton('Далее', ['class' => 'btn btn-primary', 'id' => 'btn_next']) ?>
            <?= Html::submitButton('Завершить', ['class' => 'btn btn-danger', 'id' => 'btn_stop']) ?>
        <?php } ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
