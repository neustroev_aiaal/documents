<?php

use app\models\Schools;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Otcc */

$this->title = $model->pc_name;
$canEdit = Schools::canBeEdited(\app\components\Y::user());
$this->params['breadcrumbs'][] = ['label' => 'ОТСС', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="otcc-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Добавить ВТСС', ['/vtcc/create', 'id' => $model->schools->id], ['class' => 'btn btn-primary']) ?>
        <?php
        if ($canEdit):
        ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?php
        endif
        ?>
    </p>
    <?php
    if ($canEdit):
    ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'org_id',
                'value' => Html::a($model->schools->short_name_org, ['schools/view', 'id' => $model->schools->id]),
                'format' => 'raw',
            ],
            'pc_name',
            'ip_address',
            'mac_address',
            'number_room',
            [
                'attribute' => 'pc_type',
                'value' => $model->getTypeName()
            ],
            [
                'attribute' => 'data',
                'format' => 'raw',
            ],
            [
                'attribute' => 'data_software',
                'format' => 'raw',
            ],
            [
                'attribute' => 'data_iis',
                'format' => 'raw',
            ],
            [
                'attribute' => 'os',
                'format' => 'raw',
            ],
            'note'
//            'data',
//            'data_software'
        ],
    ]) ?>
    <?php
    endif
    ?>
    <?php
    if (!$canEdit):
    ?>
        <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'org_id',
                'value' => Html::a($model->schools->short_name_org, ['schools/view', 'id' => $model->schools->id]),
                'format' => 'raw',
            ],
            'pc_name',
            'ip_address',
            'mac_address',
            'number_room',
            [
                'attribute' => 'pc_type',
                'value' => $model->getTypeName()
            ],
            'note'
        ],
    ]) ?>
    <?php
    endif
    ?>

</div>
