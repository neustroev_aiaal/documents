<?php

use app\models\Schools;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Otcc */

$this->title = 'Добавить ОТСС (компьютер для ЕГЭ/ОГЭ)';
$this->params['breadcrumbs'][] = ['label' => 'ОТСС', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="otcc-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model, 'create' => true,
    ]) ?>

</div>
