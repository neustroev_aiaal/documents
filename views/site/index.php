    <?php

/* @var $this yii\web\View */

    use app\models\Schools;
    use yii\helpers\Html;
    use yii\helpers\Url;

    $this->title = 'Аттестация';
?>
<div class="site-index">

    <div style="margin: 20px;">

        <p class="lead">Сайт для заполнения данных</p>

    </div>


    <div class="row">
        <div class="col-md-8">
            <div class="body-content">
                <a href="/schools/create">
                    <div style="
                width: 300px; /* Ширина слоя */
                margin: 7px; /* Значение отступов */
                padding: 10px; /* Поля вокруг текста */
                background:  #4da6ff;
                 border-radius: 8px;/* Цвет фона */
">
                        <p style="color: black; text-align: center;"><span class="glyphicon glyphicon-pencil"></span> Аттестация рабочих мест для ЗСПД РЦОИ</p>
                    </div>
                </a>
            </div>
            <?php if(\app\components\Y::user()): ?>
                <?php if(Schools::getListUs(\app\components\Y::user()->id)!=null): ?>
                    <div>
                        <h4>Добавленная вами школа:</h4>
                        <ul>
                            <?php foreach (Schools::getListUs(\app\components\Y::user()->id) as $school): ?>
                                <li>
                                    <a style="font-size: medium" href="<?= Url::to(['schools/view', 'id' => $school->id]) ?>">
                                        <?= Html::encode($school->short_name_org) ?>
                                    </a>
                                </li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                <?php endif ?>
            <?php endif ?>
        </div>
        <div class="col-md-4">
            <h3>ЗСПД РЦОИ</h3>
            <p>Защищенная сеть передачи данных Регионального центра обработки информации Республики Саха (Якутия)</p>
            <p>Согласно приказа Министерства образования и науки Республики Саха (Якутия) от 06.02.2018 № 01-09/180 органы управления образованием и образовательные
                учреждения должны провести организационные и технические мероприятия, подтверждающие соответствие системы защиты информации ИС требованиям безопасности.</p>
            <p>Аттестация проводится в целях обеспечения безопасного информационного взаимодействия между подключаемыми организациями и Центром мониторинга качества
                образования Республики Саха (Якутия) при предоставлении услуг, обеспечивающей доступ к единой регистрационной базе данных (ЕРБД) в Региональный центр
                обработки информации Республики Саха (Якутия).</p>
        </div>
    </div>
</div>
