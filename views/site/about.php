<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Контакты';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-about">
    <h1>Контакты</h1>
    <div class="row">
        <div class="col-md-4" style="font-size:18px;padding-left: 30px;">Адрес<br>
            677000, г. Якутск<br> ул. Ларионова, 10</div>
        <div class="col-md-4" style="font-size:18px;padding-left: 30px;">Телефон<br>
            +7 (4112) 39-00-95<br>+7 (4112) 21-93-93
        </div>
        <div class="col-md-4" style="font-size:18px;padding-left: 30px;">E-mail: <a href="mailto:mail@itykt.ru">mail@itykt.ru</a><br>
            Telegram: <a href="https://t.me/nordykt_bot">@nordykt_bot</a>
        </div>
    </div>
    <div class="row">
        <a class="dg-widget-link" href="http://2gis.ru/yakutsk/firm/7037402698802108/center/129.76267,62.030497/zoom/16?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть на карте Якутска</a><div class="dg-widget-link"><a href="http://2gis.ru/yakutsk/center/129.76267,62.030497/zoom/16/routeTab/rsType/bus/to/129.76267,62.030497╎Север, центр защиты информации?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=route">Найти проезд до Север, центр защиты информации</a></div><script charset="utf-8" src="https://widgets.2gis.com/js/DGWidgetLoader.js"></script><script charset="utf-8">new DGWidgetLoader({"width":640,"height":600,"borderColor":"#a3a3a3","pos":{"lat":62.030497,"lon":129.76267,"zoom":16},"opt":{"city":"yakutsk"},"org":[{"id":"7037402698802108"}]});</script><noscript style="color:#c00;font-size:16px;font-weight:bold;">Виджет карты использует JavaScript. Включите его в настройках вашего браузера.</noscript>
    </div>


</div>

