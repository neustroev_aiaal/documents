<?php
/**
 * Created by PhpStorm.
 * User: user3
 * Date: 10.01.2018
 * Time: 15:33
 */

class RTF_Template
{
    private $content;
    /* functions */
    /**
     * RTF_Template::__construct()
     *
     * @param mixed $filename
     * @return
     */
    public function __construct($filename)
    {
        $this->content = file_get_contents($filename);
    }//construct
    /*************************************************************************/
    /**
     * RTF_Template::parse()
     *
     * @param mixed $block_name
     * @param mixed $value
     * @param string $start_tag
     * @param string $end_tag
     * @return
     */
    public function parse($block_name, $value, $start_tag = '\{', $end_tag = '\}')
    {
        $this->content = str_ireplace($start_tag . $block_name . $end_tag, $value, $this->content);
    }//
    /*************************************************************************/
    /**
     * RTF_Template::out_f()
     *
     * @param mixed $filename
     * @return
     */
    public function out_f($filename)
    {
        file_put_contents($filename, $this->content);
    }//
    /*************************************************************************/
    /**
     * RTF_Template::out_h()
     *
     * @param mixed $filename
     * @return
     */
    public function out_h($filename)
    {
        ob_clean();
        header("Content-type: plaintext/rtf");
        header("Content-Disposition: attachment; filename=$filename");
        echo $this->content;
    }//
    /*************************************************************************/
    /**
     * RTF_Template::out()
     *
     * @param mixed $filename
     * @return
     */
    public function out()
    {
        return $this->content;
    }//
}//class