<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
$webUser = Yii::$app->user;
$user = Yii::$app->user->identity;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Аттестация',
        'id' => 'header',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $canAdmin = Yii::$app->user->can('admin');
    $canUser = Yii::$app->user->can('user');
    if(strpos($_SERVER['REQUEST_URI'],"/fis") === false) {
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav'],
            'items' => [

                ['label' => 'Школы', 'visible' => $canUser, 'url' => ['/schools']],
                ['label' => 'ОТСС', 'visible' => $canUser, 'url' => ['/otcc']],
//                ['label' => 'ВТСС', 'visible' => $canUser, 'url' => ['/vtcc']],
                ['label' => 'Контакты', 'visible' => $canUser, 'url' => ['/site/about']],
                ['label' => 'Пользователи', 'visible' => $canAdmin, 'url' => ['/user']],
                ['label' => 'Настройки', 'visible' => $canAdmin, 'items' => [
                    ['label' => 'Переферийные устройства', 'url' => ['/peripheral']],
                    ['label' => 'ПО', 'url' => ['/software']],
                    ['label' => 'Проекты', 'url' => ['/projects']],
                    ['label' => 'Сотрудники', 'url' => ['/nord']],
                    ['label' => 'Школы', 'url' => ['/schools']],
                    ['label' => 'CЗИ', 'url' => ['/security']],
                    ['label' => 'ОТСС', 'url' => ['/otcc']],
                    ['label' => 'ВТСС', 'url' => ['/otcc']],
                    ['label' => 'Договоры', 'url' => ['/dogovor']],
                ]],
                ['label' => 'Админ', 'visible' => $canAdmin, 'items' => [
                    ['label' => 'Логи', 'url' => ['/admin/logs']],
                ]],
                ['label' => 'ФИС ФРДО', 'visible' => $canAdmin, 'url' => ['/fis']],
            ]
        ]);
    } else {
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav'],
            'items' => [

                ['label' => 'Школы', 'visible' => $canUser, 'url' => ['/fis/schools/index']],
                ['label' => 'ОТСС', 'visible' => $canUser, 'url' => ['/fis/otcc/index']],
//                ['label' => 'ВТСС', 'visible' => $canUser, 'url' => ['/vtcc']],
                ['label' => 'Контакты', 'visible' => $canUser, 'url' => ['/site/about']],
                ['label' => 'Настройки', 'visible' => $canAdmin, 'items' => [
                    ['label' => 'Пользователи', 'visible' => $canAdmin, 'url' => ['/user']],
                    ['label' => 'Переферийные устройства', 'url' => ['/peripheral']],
                    ['label' => 'ПО', 'url' => ['/software']],
                    ['label' => 'Проекты', 'url' => ['/projects']],
                    ['label' => 'Сотрудники', 'url' => ['/nord']],
                    ['label' => 'Школы', 'url' => ['/schools']],
                    ['label' => 'CЗИ', 'url' => ['/security']],
                    ['label' => 'ОТСС', 'url' => ['/otcc']],
                    ['label' => 'ВТСС', 'url' => ['/otcc']],
                    ['label' => 'Договоры', 'url' => ['/dogovor']],
                ]],
                ['label' => 'Админ', 'visible' => $canAdmin, 'items' => [
                    ['label' => 'Логи', 'url' => ['/admin/logs']],
                ]],
                ['label' => 'ЗСПД РЦОИ', 'visible' => $canAdmin, 'url' => ['/']],
            ]
        ]);
        echo "<style>#header {
    background-color: #2f4252;
    color: #fff;
}
#header a, #header button {
    color: #fff;
}
#header .active a, #header a:hover {
    background-color: #374b5e;
}
#header .dropdown.open .dropdown-toggle {
    background-color: #374b5e;
}
#header .dropdown-menu {
    background-color: #2f4252;
}</style>";
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            $webUser->isGuest ? (
            ['label' => 'Вход', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Выход (' . $user->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            ),
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer id="footer">
    <div class="container">
<!--        <p class="pull-left">По вопросам технической поддержки портала обращайтесь в ЦМКО</p>-->

        <p class="pull-right">&copy; <a href="http://itykt.ru/">Центр защиты информации &quot;Север&quot;</a>, 2018</p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
