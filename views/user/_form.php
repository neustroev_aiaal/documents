<?php

use app\components\Region;
use app\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\Y;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

$roles = Y::user()->isAdmin() ? User::$ROLES_NAMES : User::$MANAGER_HANDLED_ROLES;
$roles = User::$ROLES_NAMES;
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(['id' => 'user-form']); ?>

    <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput() ?>

    <?= $form->field($model, 'role_code')->dropDownList($roles); ?>

    <?= $form->field($model, 'region_id')->dropDownList(ArrayHelper::map(Region::$LIST, 'id', 'name'), ['prompt' => 'Выберите район (улус)']); ?>

    <?= $form->field($model, 'status')->checkbox(['label' => 'Активен']); ?>

    <?= $form->field($model, 'note')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
