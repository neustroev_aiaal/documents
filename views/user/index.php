<?php

use yii\helpers\Html;
use app\models\User;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить пользователя', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'status',
            'username',
            [
                'attribute' => 'role_code',
                'value' => function(User $model, $key, $index, $column) {
                    return $model->getRoleName();
                },
            ],
//            'org_id',
            //'hash',
            //'auth_key',
            //'created',
            //'force_logout',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
