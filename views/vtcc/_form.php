<?php

use app\models\Otcc;
use app\models\Peripheral;
use app\models\Schools;
use app\models\Software;
use unclead\multipleinput\MultipleInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Vtcc */
/* @var $form yii\widgets\ActiveForm */
$canEdit = Schools::canBeEdited(\app\components\Y::user());
?>

<div class="vtcc-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php if (!$canEdit): ?>
        <?= $form->field($model, 'org_id')->dropDownList(ArrayHelper::map(Schools::getListUs(\Yii::$app->user->id), 'id', 'short_name_org')); ?>
    <?php
    endif
    ?>
    <?php if ($canEdit): ?>
        <?= $form->field($model, 'org_id')->dropDownList(ArrayHelper::map(Schools::getList(), 'id', 'short_name_org'), ['prompt' => 'Выберите школу']); ?>
    <?php
    endif
    ?>
    <?= $form->field($model, 'pc_name', ['template' => "{label}\n<div class=\"input-group\">{input}\n<span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-question-sign\" data-container=\"body\" data-toggle=\"popover\" style='cursor: pointer' data-placement=\"right\" data-content=\"Наименование компьютера. Мой компьютер->Свойства->Имя компьютера\"></span></span></div>\n{hint}\n{error}"])->textInput(['maxlength' => true, 'placeholder' => 'comp']) ?>

    <?= $form->field($model, 'pc_type', ['template' => "{label}\n<div class=\"input-group\">{input}\n<span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-question-sign\" data-container=\"body\" data-toggle=\"popover\" style='cursor: pointer' data-placement=\"right\" data-content=\"Тип вашего компьютера: Персональный компьютер, Ноутбук, Моноблок\"></span></span></div>\n{hint}\n{error}"])->dropDownList(Otcc::$TYPES, ['prompt' => 'Выберите тип']); ?>

    <?= $form->field($model, 'number_room', ['template' => "{label}\n<div class=\"input-group\">{input}\n<span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-question-sign\" data-container=\"body\" data-toggle=\"popover\" style='cursor: pointer' data-placement=\"right\" data-content=\"Кабинет где находиться компьютер для ЕГЭ/ОГЭ\"></span></span></div>\n{hint}\n{error}"])->textInput(['maxlength' => true, 'placeholder' => '123']) ?>

    <?= $form->field($model, 'data_software')->widget(MultipleInput::className(), [
        'max' => 4,
        'columns' => [
            [
                'name' => 'type_soft',
                'type' => 'dropDownList',
                'title' => 'Тип программного обеспечения',
                'defaultValue' => 0,
                'items' => Otcc::$SOFT
            ],
            [
                'name' => 'name',
                'title' => 'Наименование программ',
                'enableError' => true,
                'options' => [
                    'class' => 'input-priority'
                ]
            ],
            [
                'name' => 'serial_number',
                'title' => 'Номер лицензии',
                'enableError' => true,
                'options' => [
                    'class' => 'input-priority'
                ]
            ]
        ]
    ]);
    ?>

    <?= $form->field($model, 'data')->widget(MultipleInput::className(), [
        'max' => 6,
        'columns' => [
            [
                'name' => 'peripheral',
                'type' => 'dropDownList',
                'title' => 'Переферийное устройство',
                'defaultValue' => 1,
                'items' => ArrayHelper::map(Peripheral::getList(), 'id', 'name')
            ],
            [
                'name' => 'model',
                'title' => 'Модель',
                'enableError' => true,
                'options' => [
                    'class' => 'input-priority'
                ]
            ],
            [
                'name' => 'serial_number',
                'title' => 'Заводской номер',
                'enableError' => true,
                'options' => [
                    'class' => 'input-priority'
                ]
            ]
        ]
    ]);
    ?>
    <div class="form-group">
        <input type='hidden' id="save_and_add" name='save_and_add' value=''>
    </div>
    <div class="form-group">
        <input type='hidden' id="next" name='next' value=''>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        <button type='submit' class="btn btn-info" id='addInput'>Сохранить и добавить новый</button>
        <button type='submit' class="btn btn-danger" id='btn_next'>Завершить</button>
    </div>

    <?php ActiveForm::end(); ?>

</div>
