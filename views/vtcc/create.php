<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Vtcc */

$this->title = 'Создание ВТСС';
$this->params['breadcrumbs'][] = ['label' => 'ВТСС', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vtcc-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
