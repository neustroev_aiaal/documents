<?php

use app\models\Schools;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Vtcc */

$this->title = $model->id;
$canEdit = Schools::canBeEdited(\app\components\Y::user());
$this->params['breadcrumbs'][] = ['label' => 'ВТСС', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vtcc-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php
        if ($canEdit):
        ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?php
        endif
        ?>
    </p>
    <?php
    if ($canEdit):
        ?>
        <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'org_id',
                'value' => Html::a($model->schools->short_name_org, ['schools/view', 'id' => $model->schools->id]),
                'format' => 'raw',
            ],
            'pc_name',
            'number_room',
            [
                'attribute' => 'pc_type',
                'value' => $model->getTypeName()
            ],
            [
                'attribute' => 'data',
                'format' => 'raw',
            ],
            [
                'attribute' => 'data_software',
                'format' => 'raw',
            ],
        ],
    ]) ?>
    <?php
    endif
    ?>
    <?php
    if (!$canEdit):
        ?>
        <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'org_id',
                'value' => Html::a($model->schools->short_name_org, ['schools/view', 'id' => $model->schools->id]),
                'format' => 'raw',
            ],
            'pc_name',
            'number_room',
            [
                'attribute' => 'pc_type',
                'value' => $model->getTypeName()
            ],
        ],
    ]) ?>
    <?php
    endif
    ?>

</div>
