<?php

use app\models\Otcc;
use app\models\Schools;
use app\models\Vtcc;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VtccSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ВТСС';
$canEdit = Schools::canBeEdited(\app\components\Y::user());
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vtcc-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php
    if ($canEdit):
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute'=>'org_id',
                'value'=>'schools.short_name_org',
                'filter'=>ArrayHelper::map(Schools::getList(), 'id', 'short_name_org'),
            ],
            [
                'attribute' => 'pc_name',
                'format' => 'raw',
                'value' => function (Vtcc $model) {
                    return Html::a($model->pc_name, ['view', 'id' => $model->id]);
                }
            ],
            [
                'attribute' => 'pc_type',
                'value' => function(Vtcc $model, $key, $index, $column) {
                    return $model->getTypeName();
                },
                'filter' => Vtcc::$TYPES
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php
    endif
    ?>
    <?php
    if (!$canEdit):
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'pc_name',
                'format' => 'raw',
                'value' => function (Vtcc $model) {
                    return Html::a($model->pc_name, ['view', 'id' => $model->id]);
                }
            ],
            [
                'attribute' => 'pc_type',
                'value' => function(Vtcc $model, $key, $index, $column) {
                    return $model->getTypeName();
                },
                'filter' => Vtcc::$TYPES
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}'],
        ],
    ]); ?>
    <?php
    endif
    ?>
</div>
