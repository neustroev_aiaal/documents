<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NordDogovor */

$this->title = 'Update Nord Dogovor: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Nord Dogovors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="nord-dogovor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
