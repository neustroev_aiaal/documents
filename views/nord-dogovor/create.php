<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\NordDogovor */

$this->title = 'Create Nord Dogovor';
$this->params['breadcrumbs'][] = ['label' => 'Nord Dogovors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nord-dogovor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
