<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Nord */

$this->title = 'Create Nord';
$this->params['breadcrumbs'][] = ['label' => 'Nords', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nord-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
