<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Dogovor */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Организация', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dogovor-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    <div class="row">
        <div class="col-xs-6">
            <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </div>
        <div class="col-xs-6">
            <?= Html::a('Генерировать договор для смарт', ['generate-smart', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
            <?= Html::a('Генерировать договор для егэ', ['generate-ege', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
        </div>
    </div>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'dogovor_id',
            'date_public',
            'full_name_org',
            [
                'attribute' => 'position_id',
                'value' => $model->getTypeName()
            ],
            'fio',
            [
                'attribute' => 'osnovanie',
                'value' => $model->getOsnovanieName()
            ],
            'date_begin',
            'date_end',
            'short_name_org',
            'inn',
            'kpp',
            'address_org',
            'pc',
            'bank',
            'kc',
            'bik',
            'tel',
            'email:email',
        ],
    ]) ?>

</div>
