<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Dogovor */

$this->title = 'Изменение данных организации: '.$model->short_name_org.'';
$this->params['breadcrumbs'][] = ['label' => 'Организация', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменение';
?>
<div class="dogovor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
