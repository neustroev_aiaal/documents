<?php

use app\models\Dogovor;
use app\models\NordDogovor;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Dogovor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dogovor-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dogovor_id')->textInput() ?>

    <div class="row">
        <div class="col-xs-8">
            <?= $form->field($model, 'full_name_org')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-4">
            <?= $form->field($model, 'ending_name_org', ['template'=>"{label}\n<div class=\"input-group\"><span class=\"input-group-addon\">именуем</span>{input}\n</div>\n{hint}\n{error}"])->dropDownList(Dogovor::$ENDING_NAME_ORG); ?>
        </div>
    </div>

    <?= $form->field($model, 'short_name_org')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address_org')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tel')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date_public')->textInput(['placeholder' => 'дд.мм.гггг'])
//    widget(
//    'trntv\yii\datetime\DateTimeWidget',
//    [
//        'phpDatetimeFormat' => 'yyyy-MM-dd',
//        'clientOptions' => [
//            'allowInputToggle' => false,
//            'sideBySide' => true,
//            'locale' => 'ru-ru',
//            'widgetPositioning' => [
//               'horizontal' => 'auto',
//               'vertical' => 'auto'
//            ]
//        ]
//    ]
//)
    ; ?>

    <?= $form->field($model, 'position_id')->dropDownList(Dogovor::$TYPES,['prompt' => 'Выберите тип']); ?>

    <div class="row">
        <div class="col-xs-8">
            <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-4">
            <?= $form->field($model, 'ending_fio', ['template'=>"{label}\n<div class=\"input-group\"><span class=\"input-group-addon\">действующ</span>{input}\n</div>\n{hint}\n{error}"])->dropDownList(Dogovor::$ENDING_FIO); ?>
        </div>
    </div>

    <?= $form->field($model, 'osnovanie')->dropDownList(Dogovor::$OSNOVANIE); ?>

    <?= $form->field($model, 'spec_nord')->dropDownList(ArrayHelper::map(NordDogovor::getList(), 'id', 'fio')); ?>

    <?= $form->field($model, 'date_begin')->textInput(['placeholder' => 'дд.мм.гггг']) ?>

    <?= $form->field($model, 'date_end')->textInput(['placeholder' => 'дд.мм.гггг']) ?>

    <?= $form->field($model, 'inn')->textInput() ?>

    <?= $form->field($model, 'kpp')->textInput() ?>

    <?= $form->field($model, 'pc')->textInput() ?>

    <?= $form->field($model, 'bank')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kc')->textInput() ?>

    <?= $form->field($model, 'bik')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
