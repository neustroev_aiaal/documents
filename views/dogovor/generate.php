<?php

use app\models\Nord;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\Dogovor */
/* @var $form yii\widgets\ActiveForm */

$this->title = $model->short_name_org;
$this->params['breadcrumbs'][] = ['label' => 'Организации', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;



?>
<div class="schools-view">
    <h3>Файл успешно генерировано</h3>
    <?= Html::a('Скачать', ['/dogovor/download?id='.$model->id.''], ['class'=>'btn btn-success']) ?>
</div>
