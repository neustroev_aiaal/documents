<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DogovorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dogovor-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'dogovor_id') ?>

    <?= $form->field($model, 'date_public') ?>

    <?= $form->field($model, 'full_name_org') ?>

    <?= $form->field($model, 'position_id') ?>

    <?= $form->field($model, 'fio') ?>


    <?php // echo $form->field($model, 'date_begin') ?>

    <?php // echo $form->field($model, 'date_end') ?>

    <?php // echo $form->field($model, 'short_name_org') ?>

    <?php // echo $form->field($model, 'inn') ?>

    <?php // echo $form->field($model, 'kpp') ?>

    <?php // echo $form->field($model, 'address_org') ?>

    <?php // echo $form->field($model, 'pc') ?>

    <?php // echo $form->field($model, 'bank') ?>

    <?php // echo $form->field($model, 'kc') ?>

    <?php // echo $form->field($model, 'bik') ?>

    <?php // echo $form->field($model, 'tel') ?>

    <?php // echo $form->field($model, 'email') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
