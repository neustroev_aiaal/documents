<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Peripheral */

$this->title = 'Create Peripheral';
$this->params['breadcrumbs'][] = ['label' => 'Peripherals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="peripheral-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
