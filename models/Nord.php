<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "doc_nord".
 *
 * @property string $id
 * @property string $fio
 * @property string $spec
 */
class Nord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'doc_nord';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fio', 'spec'], 'required'],
            [['fio', 'spec'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'ФИО',
            'spec' => 'Специальность',
        ];
    }

    public static function get($id) {
        return self::findOne($id);
    }

    public static function getList() {
        return static::find()->all();
    }

    public static function getNordList() {
        return static::find()->where(['type'=>0])->all();
    }
}
