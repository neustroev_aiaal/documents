<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "doc_schools".
 *
 * @property string $id
 * @property string $name_org
 * @property string $name_org_rod
 * @property string $short_name_org
 * @property string $address
 * @property string $school_director
 * @property string $date
 * @property string $date_create
 * @property int $antivirus_id
 * @property int $vipnet_id
 * @property string $number_protokol
 * @property string $number_techpassport
 * @property string $number_certificate
 * @property string $number_finale
 * @property int $user_count
 * @property int $iis_id
 * @property Security security
 * @property int spec_nord_id
 * @property int project_id
 * @property string $spec
 * @property string $spec_fio
 * @property string $position_director
 * @property int user_id
 * @property int inn
 * @property int region_id
 * @property string $note
 * @property Boolean $plan_room
 * @property Boolean $plan_building
 */
class Schools extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'doc_schools';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inn', 'name_org', 'address', 'region_id', 'position_director', 'school_director', 'spec', 'spec_fio', 'short_name_org', 'name_org_rod'], 'required'],
            [['inn', 'spec_nord_id', 'project_id', 'region_id'], 'integer'],
            [['inn'], 'string', 'max' => 12],
            [['date', 'date_create', 'plan_room', 'plan_building'], 'safe'],
            [['note'], 'string', 'max' => 500],
            [['name_org', 'address', 'school_director', 'number_protokol', 'number_techpassport', 'number_certificate', 'number_finale', 'spec', 'position_director', 'spec_fio', 'short_name_org', 'name_org_rod'], 'string', 'max' => 255],
        ];
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->date = date('Y-m-d', strtotime($this->date));
            $this->date_create = date('Y-m-d', strtotime($this->date_create));
            return true;
        }
        return false;
    }

    public function afterFind() {
        parent::afterFind();
        $this->date = date('d.m.Y', strtotime($this->date));
        $this->date_create = date('d.m.Y', strtotime($this->date_create));
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Проект',
            'region_id' => 'Район (улус)',
            'inn' => 'ИНН организации',
            'name_org' => 'Полное наименование школы',
            'name_org_rod' => 'Полное наименование школы в родительном падеже (кого? чего?)',
            'short_name_org' => 'Краткое наименование школы',
            'address' => 'Адрес',
            'position_director' => 'Должность руководителя',
            'school_director' => 'ФИО руководителя',
            'antivirus_id' => 'Антивирус',
            'number_protokol' => 'Номер протокола',
            'number_techpassport' => 'Номер техпаспорта',
            'number_certificate' => 'Номер аттестата',
            'number_finale' => 'Номер заключения',
            'spec' => 'Должность специалиста',
            'spec_fio' => 'ФИО специалиста',
            'spec_nord_id' => 'Сотрудник ЦЗИ Север',
            'date' => 'Дата утверждения документа',
            'date_create' => 'Дата разработки документа',
            'user_id' => 'Пользователь',
            'note' => 'Примечание',
            'plan_room' => 'Схема кабинет',
            'plan_building' => 'Схема здания/этажа'
        ];
    }

    public function getSecurity() {
        return $this->hasOne(Security::className(), ['id' => 'id']);
    }

    public function getSecurityIis($id) {
        if($id==null){
            return null;
        }
        return Security::findOne($id)->name;
    }

    public static function getList(){
        return static::find()->all();
    }

    public static function getListUs($user_id){
        return static::find()->where(['user_id'=>$user_id])->all();
    }

    public function getNord() {
        return $this->hasOne(Nord::className(), ['id' => 'spec_nord_id']);
    }

    public function getProjects() {
        return $this->hasOne(Projects::className(), ['id' => 'project_id']);
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function canBeEdited(User $user) {
        return $user->isAdminOrManager();
    }

    public function getMonth($month){
        $name = null;
        switch ($month){
            case 1:
                $name='января';
                break;
            case 2:
                $name='февраля';
                break;
            case 3:
                $name='марта';
                break;
            case 4:
                $name='апреля';
                break;
            case 5:
                $name='мая';
                break;
            case 6:
                $name='июня';
                break;
            case 7:
                $name='июля';
                break;
            case 8:
                $name='августа';
                break;
            case 9:
                $name='сентября';
                break;
            case 10:
                $name='октября';
                break;
            case 11:
                $name='ноября';
                break;
            case 12:
                $name='декабря';
                break;
        }
        return $name;
    }

    function translit($s) {
        $s = (string) $s; // преобразуем в строковое значение
        $s = strip_tags($s); // убираем HTML-теги
        $s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
        $s = preg_replace("/\s+/", ' ', $s); // удаляем повторяющие пробелы
        $s = trim($s); // убираем пробелы в начале и конце строки
        $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
        $s = strtr($s, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
        $s = preg_replace("/[^0-9a-z-_ ]/i", "", $s); // очищаем строку от недопустимых символов
        $s = str_replace(" ", "_", $s); // заменяем пробелы знаком минус
        return $s; // возвращаем результат
    }

    public function right_letter($str){
        $array = array( "q"=>"й", "w"=>"ц", "e"=>"у", "r"=>"к",
            "t"=>"е", "y"=>"н", "u"=>"г", "i"=>"ш", "o"=>"щ",
            "p"=>"з", "["=>"х", "]"=>"ъ", "a"=>"ф", "s"=>"ы",
            "d"=>"в", "f"=>"а", "g"=>"п", "h"=>"р", "j"=>"о",
            "k"=>"л", "l"=>"д", ";"=>"ж", "'"=>"э", "z"=>"я",
            "x"=>"ч", "c"=>"с", "v"=>"м", "b"=>"и", "n"=>"т",
            "m"=>"ь", ","=>"б", "."=>"ю", "{"=>"х", "}"=>"ъ",
            ":"=>"ж", '"'=>"э", "<"=>"б", ">"=>"ю");
        $right_letter = strtr($str,$array);
        return $right_letter;
    }

    public static function get($id) {
        return self::findOne($id);
    }
}
