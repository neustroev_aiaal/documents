<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "doc_nord_dogovor".
 *
 * @property string $id
 * @property string $fio
 * @property string $spec
 * @property string $note
 */
class NordDogovor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'doc_nord_dogovor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fio', 'spec', 'note'], 'required'],
            [['fio', 'spec', 'note'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'Fio',
            'spec' => 'Spec',
            'note' => 'Note',
        ];
    }

    public static function getList() {
        return static::find()->all();
    }

    public static function get($id) {
        return static::findOne($id);
    }
}
