<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "doc_otcc".
 *
 * @property string $id
 * @property int $org_id
 * @property string $pc_name
 * @property string $number_room
 * @property int $pc_type
 * @property resource $data
 * @property resource $data_software
 * @property int $user_id
 * @property resource $data_iis
 * @property string $ip_address
 * @property string $mac_address
 * @property resource $os
 * @property string $note
 */
class Otcc extends \yii\db\ActiveRecord
{

    const TYPE_PC = 0;
    const TYPE_NOTEBOOK = 1;
    const TYPE_CANDLESTICK = 2;

    const SOFT_OS = 1;
    const SOFT_IIS = 2;
    const SOFT_KIIS = 3;

    public static $TYPES = [
        self::TYPE_PC => 'ПК',
        self::TYPE_NOTEBOOK => 'Ноутбук',
        self::TYPE_CANDLESTICK => 'Моноблок',
    ];

    public static $SOFT = [
        self::SOFT_OS => 'Операционная система',
        self::SOFT_IIS => 'СЗИ',
        self::SOFT_KIIS => 'СКЗИ',
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'doc_otcc';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['org_id','pc_name', 'pc_type', 'data', 'number_room', 'ip_address', 'mac_address'], 'required'],
            [['org_id', 'pc_type'], 'integer'],
            [['data', 'data_software', 'data_iis'], 'string'],
            [['pc_name', 'number_room', 'ip_address', 'mac_address'], 'string', 'max' => 255],
            [['note'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'org_id' => 'Организация',
            'pc_name' => 'Сетевое имя компьютера',
            'ip_address' => 'IP адрес',
            'mac_address' => 'MAC адрес',
            'pc_type' => 'Тип АРМ',
            'number_room' => 'Номер кабинета',
            'data' => 'Данные',
            'data_software' => 'ПО',
            'user_id' => 'Пользователь',
            'os' => 'Операционная система',
            'data_iis' => 'ПО по сертификацию ФСТЭК',
            'note' => 'Примечание'
        ];
    }

    public function getTypeName() {
        return static::$TYPES[$this->pc_type];
    }

    public function getSchools() {
        return $this->hasOne(Schools::className(), ['id' => 'org_id']);
    }

    public static function getOtcc($org_id){
        return self::find()->where(['org_id'=>$org_id])->all();
    }
}
