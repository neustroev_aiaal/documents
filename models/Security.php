<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "doc_security".
 *
 * @property string $id
 * @property string $name
 * @property string $mark_num
 * @property string $certificate
 * @property int $type
 */
class Security extends \yii\db\ActiveRecord
{

    const TYPE_VIPNET = 0;
    const TYPE_ANTIVIRUS = 1;
    const TYPE_DALLAS = 2;
    const TYPE_VIPNET_SAFEBOOT = 3;
    const TYPE_VIPNET_IDS = 4;

    public static $TYPES = [
        self::TYPE_VIPNET => 'Випнет',
        self::TYPE_ANTIVIRUS => 'Антивирус',
        self::TYPE_DALLAS => 'СЗИ',
        self::TYPE_VIPNET_SAFEBOOT => 'SAFEBOOT',
        self::TYPE_VIPNET_IDS => 'IDS',
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'doc_security';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'certificate', 'type'], 'required'],
            [['type'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['certificate'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'certificate' => 'Сертификаты соответствия',
            'type' => 'Тип',
        ];
    }

    public function getTypeName() {
        return static::$TYPES[$this->type];
    }

    public static function getType($id) {
        return self::find()->where(['type'=>$id])->one();
    }

    public static function getList($type) {
        return static::find()->where(['type'=>$type])->all();
    }

    public static function getAllList() {
        return static::find()->all();
    }

    public static function getSecurity($id){
        return static::findOne($id);
    }

    public static function getSecurityName($id) {
        return static::findOne($id)->name;
    }

    public static function getSecurityCertificate($id) {
        return static::findOne($id)->certificate;
    }

    public static function getSecurityType($id){
        return static::findOne($id)->type;
    }

    public static function getSecurityAllType($type){
        return static::find()->where(['type'=>$type])->all();
    }
}
