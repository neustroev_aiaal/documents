<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Vtcc;

/**
 * VtccSearch represents the model behind the search form of `app\models\Vtcc`.
 */
class VtccSearch extends Vtcc
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'org_id'], 'integer'],
            [['pc_name', 'pc_type', 'data'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Vtcc::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'org_id' => $this->org_id,
        ]);

        $query->andFilterWhere(['like', 'pc_name', $this->pc_name])
            ->andFilterWhere(['like', 'pc_type', $this->pc_type])
            ->andFilterWhere(['like', 'data', $this->data]);

        return $dataProvider;
    }
}
