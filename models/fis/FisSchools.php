<?php

namespace app\models\fis;

use app\models\Peripheral;
use app\models\Projects;
use app\models\Schools;
use app\models\Security;
use app\models\User;
use PhpOffice\PhpWord\TemplateProcessor;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Yii;
use ZipArchive;

/**
 * This is the model class for table "fis_schools".
 *
 * @property string $id
 * @property int $region_id
 * @property string $name_org
 * @property string $name_org_rod
 * @property string $short_name_org
 * @property string $inn
 * @property string $school_director
 * @property string $position_director
 * @property string $date
 * @property string $date_create
 * @property string $address
 * @property string $number_protokol
 * @property string $number_techpassport
 * @property string $number_certificate
 * @property string $number_finale
 * @property string $spec
 * @property string $spec_fio
 * @property int $project_id
 * @property int $user_id
 * @property string $number_contract
 * @property string $date_contract
 * @property string $note
 * @property Boolean $plan_room
 * @property Boolean $plan_building
 * @property string $reg_number
 * @property string $date_certificate_vipnet
 */
class FisSchools extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fis_schools';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['region_id', 'project_id', 'user_id'], 'integer'],
            [['date', 'date_create', 'date_contract', 'plan_room', 'plan_building', 'date_certificate_vipnet'], 'safe'],
            [['name_org', 'short_name_org', 'school_director', 'position_director', 'address', 'spec', 'spec_fio', 'region_id', 'project_id','name_org_rod', 'date_contract', 'number_contract'], 'required'],
            [['name_org', 'name_org_rod', 'short_name_org', 'inn', 'school_director', 'position_director', 'address', 'number_protokol', 'number_techpassport', 'number_certificate', 'number_finale', 'spec', 'spec_fio', 'number_contract', 'reg_number'], 'string', 'max' => 255],
            [['note'], 'string', 'max' => 500],
        ];
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->date = date('Y-m-d', strtotime($this->date));
            $this->date_create = date('Y-m-d', strtotime($this->date_create));
            $this->date_contract = date('Y-m-d', strtotime($this->date_contract));
            $this->date_certificate_vipnet = date('Y-m-d', strtotime($this->date_certificate_vipnet));
            return true;
        }
        return false;
    }

    public function afterFind() {
        parent::afterFind();
        $this->date = date('d.m.Y', strtotime($this->date));
        $this->date_create = date('d.m.Y', strtotime($this->date_create));
        $this->date_contract = date('d.m.Y', strtotime($this->date_contract));
        $this->date_certificate_vipnet = date('d.m.Y', strtotime($this->date_certificate_vipnet));
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Проект',
            'region_id' => 'Район (улус)',
            'inn' => 'ИНН организации',
            'name_org' => 'Полное наименование школы',
            'name_org_rod' => 'Полное наименование школы в родительном падеже (кого? чего?)',
            'short_name_org' => 'Краткое наименование школы',
            'address' => 'Адрес',
            'position_director' => 'Должность руководителя',
            'school_director' => 'ФИО руководителя',
            'antivirus_id' => 'Антивирус',
            'number_protokol' => 'Номер протокола',
            'number_techpassport' => 'Номер техпаспорта',
            'number_certificate' => 'Номер аттестата',
            'number_finale' => 'Номер заключения',
            'spec' => 'Должность специалиста',
            'spec_fio' => 'ФИО специалиста',
            'spec_nord_id' => 'Сотрудник ЦЗИ Север',
            'date' => 'Дата утверждения документа',
            'date_create' => 'Дата разработки документа',
            'user_id' => 'Пользователь',
            'number_contract' => 'Номер договора',
            'date_contract' => 'Дата договора',
            'note' => 'Примечание',
            'plan_room' => 'Схема кабинет',
            'plan_building' => 'Схема здания/этажа',
            'date_certificate_vipnet' => 'Дата сертификата тех поддержки',
            'reg_number' => 'Регистрационная информация'
        ];
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function getList(){
        return static::find()->all();
    }

    public static function getListUs($user_id){
        return static::find()->where(['user_id'=>$user_id])->all();
    }

    private function eshkere($otcc, $templateProcessor)
    {
        $count_dallas = 0;
        $count_vipnet = 0;
        foreach ($otcc as $item1) {
            $json_soft_data = json_decode($item1->data_iis, true);
            if ($json_soft_data != null) {
                foreach ($json_soft_data as $json_item) {
                    if (Security::getSecurityType($json_item['type_soft']) == 2) {
                        $count_dallas++;
                    }
                    if (Security::getSecurityType($json_item['type_soft']) == 0) {
                        $count_vipnet++;
                    }
                }
                if ($count_dallas > 1)
                    $templateProcessor->cloneRow('dallas_name', $count_dallas);
                if ($count_vipnet > 1)
                    $templateProcessor->cloneRow('vipnet_name', $count_vipnet);
            }
        }
        $ant = 1;
        $dal = 1;
        $vip = 1;
        foreach ($otcc as $item1) {
            $json_soft_data = json_decode($item1->data_iis, true);
            if (count($json_soft_data) > 0)
                foreach ($json_soft_data as $json_item) {
                    if (Security::getSecurityType($json_item['type_soft']) == 2) {
                        if ($count_dallas > 1) {
                            $templateProcessor->setValue('dallas_name#' . $dal, htmlspecialchars(Security::getSecurityName($json_item['type_soft']))); // Наименование далласа
                            $templateProcessor->setValue('dallas_count#' . $dal, htmlspecialchars(1)); // Количество далласа
                            $templateProcessor->setValue('dallas_serial_number#' . $dal, htmlspecialchars($json_item['serial_number'])); // Серийный номер далласа
                            $templateProcessor->setValue('dallas_mark#' . $dal, htmlspecialchars($json_item['mark_number'])); // Знак соответсвия далласа
                            $templateProcessor->setValue('dallas_certificate#' . $dal, htmlspecialchars(Security::getSecurityCertificate($json_item['type_soft']))); // Сертификат далласа
                            $templateProcessor->setValue('dallas_number_room#' . $dal, htmlspecialchars($item1->number_room)); // Номер кабинета
                            $dal++;
                        } else {
                            $templateProcessor->setValue('dallas_name', htmlspecialchars(Security::getSecurityName($json_item['type_soft']))); // Наименование далласа
                            $templateProcessor->setValue('dallas_count', htmlspecialchars(1)); // Количество далласа
                            $templateProcessor->setValue('dallas_serial_number', htmlspecialchars($json_item['serial_number'])); // Серийный номер далласа
                            $templateProcessor->setValue('dallas_mark', htmlspecialchars($json_item['mark_number'])); // Знак соответсвия далласа
                            $templateProcessor->setValue('dallas_certificate', htmlspecialchars(Security::getSecurityCertificate($json_item['type_soft']))); // Сертификат далласа
                            $templateProcessor->setValue('dallas_number_room', htmlspecialchars($item1->number_room)); // Номер кабинета
                        }
                    }
                    if (Security::getSecurityType($json_item['type_soft']) == 0) {
                        if ($count_vipnet > 1) {
                            $templateProcessor->setValue('vipnet_name#' . $vip, htmlspecialchars(Security::getSecurityName($json_item['type_soft']))); // Наименование антивируса
                            $templateProcessor->setValue('vipnet_count#' . $vip, htmlspecialchars(1)); // Количество антивируса
                            $templateProcessor->setValue('vipnet_serial_number#' . $vip, htmlspecialchars($json_item['serial_number'])); // Серийный номер антивируса
                            $templateProcessor->setValue('vipnet_mark#' . $vip, htmlspecialchars($json_item['mark_number'])); // Знак соответсвия антивируса
                            $templateProcessor->setValue('vipnet_certificate#' . $vip, htmlspecialchars(Security::getSecurityCertificate($json_item['type_soft']))); // Сертификат антивируса
                            $templateProcessor->setValue('vipnet_number_room#' . $vip, htmlspecialchars($item1->number_room)); // Номер кабинета
                            $vip++;
                        } else {
                            $templateProcessor->setValue('vipnet_name', htmlspecialchars(Security::getSecurityName($json_item['type_soft']))); // Наименование антивируса
                            $templateProcessor->setValue('vipnet_count', htmlspecialchars(1)); // Количество антивируса
                            $templateProcessor->setValue('vipnet_serial_number', htmlspecialchars($json_item['serial_number'])); // Серийный номер антивируса
                            $templateProcessor->setValue('vipnet_mark', htmlspecialchars($json_item['mark_number'])); // Знак соответсвия антивируса
                            $templateProcessor->setValue('vipnet_certificate', htmlspecialchars(Security::getSecurityCertificate($json_item['type_soft']))); // Сертификат антивируса
                            $templateProcessor->setValue('vipnet_number_room', htmlspecialchars($item1->number_room)); // Номер кабинета
                        }
                    }
                }
        }
    }

    public function protokol($id)
    {
        $model = FisSchools::findOne($id);
        $otcc = FisOtcc::getOtcc($id);
        $project = Projects::findOne($model->project_id);
        $comp_name = "";
        $arrIpAddress = array();
        $arrMacAddress = array();
        $arrNumberRoom = array();
        foreach ($otcc as $item1) {
            $comp_name = $item1->pc_name;
            array_push($arrIpAddress, $item1->ip_address);
            array_push($arrMacAddress, $item1->mac_address);
            array_push($arrNumberRoom, $item1->number_room);
        }
        $templateProcessorProtokol = new TemplateProcessor($this->getDir() . "/documents/templates/fis/protokol.docx");
        $date = date_parse($model->date);
        $date_create = date_parse($model->date_create);
        $templateProcessorProtokol->setValue('date_public', htmlspecialchars("«" . $date['day'] . "» " . Schools::getMonth($date['month']) . " " . $date['year']));
        $templateProcessorProtokol->setValue('date_public#1', htmlspecialchars($date['day'] . "/" . $date['month'] . "/" . $date['year']));
        $templateProcessorProtokol->setValue('date_create', htmlspecialchars("«" . $date_create['day'] . "» " . Schools::getMonth($date_create['month']) . " " . $date_create['year']));
        $templateProcessorProtokol->setValue('name_org', htmlspecialchars($model->name_org_rod)); //Наименование организации в родительном падеже
        $templateProcessorProtokol->setValue('short_name', htmlspecialchars($model->short_name_org)); // Краткое имя организации
        $templateProcessorProtokol->setValue('number_protokol', htmlspecialchars($model->number_protokol)); //Номер документа протокола
//        $templateProcessorProtokol->setValue('fio_nord', htmlspecialchars(preg_replace('#(.*)\s+(.).*\s+(.).*#usi', '$1', Nord::get($model->spec_nord_id)->fio))); // Фамилия кто составил техпаспорт
        $templateProcessorProtokol->setValue('address_org', htmlspecialchars($model->address)); // Адрес организации
//        $templateProcessorProtokol->setValue('full_name', htmlspecialchars($project->full_name)); // Полное наименование заказчика
//        $templateProcessorProtokol->setValue('short_name', htmlspecialchars($project->short_name)); // Краткое наименование заказчика
        $templateProcessorProtokol->setValue('doc_name', htmlspecialchars($project->name_doc)); // Номер документа
        $templateProcessorProtokol->setValue('comp_name', htmlspecialchars($comp_name)); // Имя компьютера
        $templateProcessorProtokol->setValue('number_room', htmlspecialchars(implode(", ", $arrNumberRoom)));
        FisSchools::eshkere($otcc, $templateProcessorProtokol);
        $templateProcessorProtokol->setValue('ip_address', htmlspecialchars(implode(", ", $arrIpAddress)));
        $templateProcessorProtokol->setValue('mac_address', htmlspecialchars(implode(", ", $arrMacAddress)));
//        $templateProcessorProtokol->setValue('fio_nord#1', htmlspecialchars(preg_replace('#(.*)\s+(.).*\s+(.).*#usi', '$2.$3. $1', Nord::get($model->spec_nord_id)->fio))); // ФИО кто составил техпаспорт

        $filename = $this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/protokol_" . Schools::translit($model->short_name_org) . ".docx";
        $templateProcessorProtokol->saveAs($filename);
    }

    public function tech_passport($id)
    {
        $model =FisSchools::findOne($id);
        $otcc = FisOtcc::getOtcc($id);
        $project = Projects::findOne($model->project_id);
        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/tech_passport.docx");
        // Variables on different parts of document
        $date = date_parse($model->date);
        $date_create = date_parse($model->date_create);
        $templateProcessor->setValue('date_public', htmlspecialchars("«" . $date['day'] . "» " . Schools::getMonth($date['month']) . " " . $date['year']));
        $templateProcessor->setValue('date_create', htmlspecialchars("«" . $date_create['day'] . "» " . Schools::getMonth($date_create['month']) . " " . $date_create['year']));
        $templateProcessor->setValue('position_org', htmlspecialchars($model->position_director)); //Должность руководителя
        $templateProcessor->setValue('fio_org', htmlspecialchars(preg_replace('#(.*)\s+(.).*\s+(.).*#usi', '$2.$3. $1', $model->school_director))); //ФИО руководителя
        $templateProcessor->setValue('date_public', htmlspecialchars($model->date)); //Дата подписания
        $templateProcessor->setValue('name_org', htmlspecialchars($model->name_org_rod)); //Наименование организации в родительном падеже
        $templateProcessor->setValue('number_passport', htmlspecialchars($model->number_techpassport)); //Номер документа техпаспорта
        $templateProcessor->setValue('name_org#1', htmlspecialchars($model->name_org)); //Наименование организации
        $templateProcessor->setValue('fio_spec_org', htmlspecialchars(preg_replace('#(.*)\s+(.).*\s+(.).*#usi', '$1 $2.$3.', $model->spec_fio))); // ФИО специалиста
//        $templateProcessor->setValue('fio_nord', htmlspecialchars(preg_replace('#(.*)\s+(.).*\s+(.).*#usi', '$1', Nord::get($model->spec_nord_id)->fio))); // Фамилия кто составил техпаспорт
        $templateProcessor->setValue('short_name_org', htmlspecialchars($model->short_name_org)); // Краткое имя организации
        $templateProcessor->setValue('address_org', htmlspecialchars($model->address)); // Адрес организации
        $templateProcessor->setValue('count_comp', htmlspecialchars(count($otcc))); // Количество компов
        $templateProcessor->setValue('start_project', htmlspecialchars($project->date_start_project)); // Номер документа
        $templateProcessor->setValue('end_project', htmlspecialchars($project->date_end_project)); // Дата документа
        //Таблица ОТСС
        $arrNumberRoom = array();
        $templateProcessor->cloneRow('comp_name_otcc', count($otcc));
        $a = 1;
        foreach ($otcc as $item1) {
            $templateProcessor->setValue('comp_name_otcc#' . $a . '', htmlspecialchars($item1->pc_name)); // Имя компа (ОТСС)
            $templateProcessor->setValue('number_room_otcc#' . $a . '', htmlspecialchars($item1->number_room));
            array_push($arrNumberRoom, $item1->number_room);
            $json_ottc_data = json_decode($item1->data, true);
            $templateProcessor->cloneRow('peripheral_otcc#' . $a . '', count($json_ottc_data));
            $i = 1;
            foreach ($json_ottc_data as $json_item) {
                $templateProcessor->setValue('count_otcc#' . $a . '#' . $i . '', htmlspecialchars($i));
                $templateProcessor->setValue('peripheral_otcc#' . $a . '#' . $i . '', htmlspecialchars(Peripheral::getPeripheralName($json_item['peripheral'])));
                $templateProcessor->setValue('model_otcc#' . $a . '#' . $i . '', htmlspecialchars($json_item['model']));
                $templateProcessor->setValue('serial_number_otcc#' . $a . '#' . $i . '', htmlspecialchars($json_item['serial_number']));
                $i++;
            }
            $a++;
        }
        //Таблица ВТСС
//        $templateProcessor->cloneRow('comp_name_vtcc', count($vtcc));
//        $j = 1;
//        foreach ($vtcc as $item) {
//            $templateProcessor->setValue('comp_name_vtcc#' . $j . '', htmlspecialchars($item->pc_name));
//            $templateProcessor->setValue('number_room_vtcc#' . $j . '', htmlspecialchars($item->number_room)); // Номер кабинета
//            $json_vttc_data = json_decode($item->data, true);
//            $templateProcessor->cloneRow('peripheral_vtcc#' . $j . '', count($json_vttc_data));
//            $o = 1;
//            foreach ($json_vttc_data as $json_item) {
//                $templateProcessor->setValue('count_vtcc#' . $j . '#' . $o . '', htmlspecialchars($o));
//                $templateProcessor->setValue('peripheral_vtcc#' . $j . '#' . $o . '', htmlspecialchars(Peripheral::getPeripheralName($json_item['peripheral'])));
//                $templateProcessor->setValue('model_vtcc#' . $j . '#' . $o . '', htmlspecialchars($json_item['model']));
//                $templateProcessor->setValue('serial_number_vtcc#' . $j . '#' . $o . '', htmlspecialchars($json_item['serial_number']));
//                $o++;
//            }
//            $j++;
//        }
        //software
//        $countRowSoftware = 0;
//        foreach ($otcc as $item1) {
//            $json_soft_data = json_decode($item1->data_software, true);
//            foreach ($json_soft_data as $json_item) {
//                if ($json_item['type_soft'] == 3) {
//                    $countRowSoftware++;
//                }
//            }
//        }
//        $templateProcessor->cloneRow('type_soft', $countRowSoftware);
//        $s = 1;
        foreach ($otcc as $item1) {
            if($item1->os!=null) {
                $json_os = json_decode($item1->os, true);
                foreach ($json_os as $json_item) {
                    $templateProcessor->setValue('name_os', htmlspecialchars($json_item['os_name']));
                    $templateProcessor->setValue('version_os', htmlspecialchars($json_item['os_version']));
                    $templateProcessor->setValue('build_os', htmlspecialchars($json_item['os_build']));
                }
            }
        }
//            $json_soft_data = json_decode($item1->data_software, true);
//            foreach ($json_soft_data as $json_item) {
//                if ($json_item['type_soft'] == 3) {
//                    $templateProcessor->setValue('count_soft#' . $s . '', htmlspecialchars($s+1)); // Имя компа (ОТСС)
//                    $templateProcessor->setValue('type_soft#' . $s . '', htmlspecialchars(Otcc::$SOFT[$json_item['type_soft']]));
//                    $templateProcessor->setValue('name_soft#' . $s . '', htmlspecialchars($json_item['name']));
//                    $templateProcessor->setValue('serial_number_soft#' . $s . '', htmlspecialchars($json_item['serial_number']));
//                    $templateProcessor->setValue('name_comp_soft#' . $s . '', htmlspecialchars($item1->pc_name));
//                    $s++;
//                }
//            }
//        }
        $templateProcessor->setValue('number_room', htmlspecialchars(implode(", ", $arrNumberRoom)));

        //СЗИ
        FisSchools::eshkere($otcc, $templateProcessor);


//        //antivirus
//        $antivirus = Security::getSecurity($model->antivirus_id);
//        $templateProcessor->setValue('antivirus_name', htmlspecialchars($antivirus->name)); // Наименование антивируса
//        $templateProcessor->setValue('antivirus_count', htmlspecialchars($model->count_antiviruses)); // Количество антивируса
//        $templateProcessor->setValue('antivirus_serial_number', htmlspecialchars($model->serial_number_antiviruses)); // Серийный номер антивируса
//        $templateProcessor->setValue('antivirus_mark', htmlspecialchars($model->mark_num_antiviruses)); // Знак соответсвия антивируса
//        $templateProcessor->setValue('antivirus_certificate', htmlspecialchars($antivirus->certificate)); // Сертификат антивируса
//        //vipnet
//        $vipnet = Security::getType(0);
//        $templateProcessor->setValue('vipnet_name', htmlspecialchars($vipnet->name)); // Наименование випнета
//        $templateProcessor->setValue('vipnet_count', htmlspecialchars($model->count_vipnets)); // Количество випнета
//        $templateProcessor->setValue('vipnet_serial_number', htmlspecialchars($model->serial_number_vipnets)); // Серийный номер випнета
//        $templateProcessor->setValue('vipnet_mark', htmlspecialchars($model->mark_num_vipnets)); // Знак соответсвия випнета
//        $templateProcessor->setValue('vipnet_certificate', htmlspecialchars($vipnet->certificate)); // Сертификат випнета

        if (!is_dir($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "")) {
            mkdir($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "", 0777, true);
        }

        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/tech_passport_" . Schools::translit($model->short_name_org) . ".docx");
    }

    public function finale($id)
    {
        $model = FisSchools::findOne($id);
        $otcc = FisOtcc::getOtcc($id);
        $project = Projects::findOne($model->project_id);
        $arrNumberRoom = array();
        $templateProcessorFinale = new TemplateProcessor($this->getDir() . "/documents/templates/fis/finale.docx");
        $date = date_parse($model->date);
        $date_create = date_parse($model->date_create);
        $templateProcessorFinale->setValue('date_public', htmlspecialchars("«" . $date['day'] . "» " . Schools::getMonth($date['month']) . " " . $date['year']));
        $templateProcessorFinale->setValue('date_create', htmlspecialchars("«" . $date_create['day'] . "» " . Schools::getMonth($date_create['month']) . " " . $date_create['year']));
        $templateProcessorFinale->setValue('name_org', htmlspecialchars($model->name_org_rod)); //Наименование организации в родительном падеже
        $templateProcessorFinale->setValue('short_name', htmlspecialchars($model->short_name_org)); // Краткое имя организации
        $templateProcessorFinale->setValue('number_finale', htmlspecialchars($model->number_finale)); //Номер документа заключения
        $templateProcessorFinale->setValue('number_protokol', htmlspecialchars($model->number_protokol)); //Номер документа протокола
//        $templateProcessorFinale->setValue('fio_nord', htmlspecialchars(preg_replace('#(.*)\s+(.).*\s+(.).*#usi', '$1', Nord::get($model->spec_nord_id)->fio))); // Фамилия кто составил техпаспорт
        $templateProcessorFinale->setValue('address_org', htmlspecialchars($model->address)); // Адрес организации
        $templateProcessorFinale->setValue('doc_name', htmlspecialchars($project->name_doc)); // Номер документа
        $templateProcessorFinale->setValue('doc_date', htmlspecialchars($model->date)); // Дата документа
        foreach ($otcc as $item1) {
            array_push($arrNumberRoom, $item1->number_room);
        }
        $templateProcessorFinale->setValue('number_room', htmlspecialchars(implode(", ", $arrNumberRoom)));

        FisSchools::eshkere($otcc, $templateProcessorFinale);
//        $templateProcessorFinale->setValue('fio_nord#1', htmlspecialchars(preg_replace('#(.*)\s+(.).*\s+(.).*#usi', '$2.$3. $1', Nord::get($model->spec_nord_id)->fio))); // ФИО кто составил техпаспорт
        $templateProcessorFinale->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/finale_" . Schools::translit($model->short_name_org) . ".docx");
    }

    public function akt($id)
    {
        $model = FisSchools::findOne($id);
        $otcc = FisOtcc::getOtcc($id);
        $templateProcessorAkt = new TemplateProcessor($this->getDir() . "/documents/templates/fis/akt.docx");
        $templateProcessorAkt->setValue('name_org', htmlspecialchars($model->name_org_rod)); //Наименование организации в родительном падеже
        $templateProcessorAkt->setValue('name_org#1', htmlspecialchars($model->name_org)); //Наименование организации
        $templateProcessorAkt->setValue('address', htmlspecialchars($model->address)); //Наименование организации
        $date = date_parse($model->date);
        $date_contract = date_parse($model->date_contract);
        $templateProcessorAkt->setValue('date', htmlspecialchars(sprintf("%02d", $date['day']) . "." . sprintf("%02d", $date['month']) . "." . $date['year']));
        $templateProcessorAkt->setValue('date_contract', htmlspecialchars(sprintf("%02d", $date_contract['day']) . "." . sprintf("%02d", $date_contract['month']) . "." . $date_contract['year']));
        $templateProcessorAkt->setValue('name_spec', htmlspecialchars(preg_replace('#(.*)\s+(.).*\s+(.).*#usi', '$1 $2.$3.', $model->spec_fio))); // Фамилия кто составил техпаспорт
        $templateProcessorAkt->setValue('position', htmlspecialchars($model->spec));
        $templateProcessorAkt->setValue('number_contract', htmlspecialchars($model->number_contract));
        $arrNumberRoom = array();
        foreach ($otcc as $item1) {
            array_push($arrNumberRoom, $item1->number_room);
        }
        $templateProcessorAkt->setValue('number_room', htmlspecialchars(implode(", ", $arrNumberRoom)));
        FisSchools::eshkere($otcc, $templateProcessorAkt);

        $templateProcessorAkt->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/akt_" . Schools::translit($model->short_name_org) . ".docx");
    }

    public function certificate($id)
    {
        //TODO доделать дату документа ЦЗИ
        $model = FisSChools::findOne($id);
        $otcc = FisOtcc::getOtcc($id);
        $project = Projects::findOne($model->project_id);
        $templateProcessorCertificate = new TemplateProcessor($this->getDir() . "/documents/templates/fis/certificate.docx");
        $templateProcessorCertificate->setValue('name_org', htmlspecialchars($model->name_org_rod)); //Наименование организации в родительном падеже
        $templateProcessorCertificate->setValue('short_name', htmlspecialchars($model->short_name_org)); //Краткое наименование организации
        $templateProcessorCertificate->setValue('number_certificate', htmlspecialchars($model->number_certificate)); //Номер документа аттестата
        $templateProcessorCertificate->setValue('number_protokol', htmlspecialchars($model->number_protokol)); //Номер документа протокола
        $templateProcessorCertificate->setValue('number_finale', htmlspecialchars($model->number_finale)); //Номер документа заключения
//        $templateProcessorCertificate->setValue('fio_nord', htmlspecialchars(preg_replace('#(.*)\s+(.).*\s+(.).*#usi', '$1', Nord::get($model->spec_nord_id)->fio))); // Фамилия кто составил техпаспорт
        $templateProcessorCertificate->setValue('address_org', htmlspecialchars($model->address)); // Адрес организации
        $templateProcessorCertificate->setValue('doc_name', htmlspecialchars($project->name_doc)); // Номер документа
        $templateProcessorCertificate->setValue('doc_date', htmlspecialchars($model->date)); // Дата документа
        FisSChools::eshkere($otcc, $templateProcessorCertificate);

        $arrNumberRoom = array();
        $templateProcessorCertificate->cloneRow('count_otcc', count($otcc));
        $a = 1;
        foreach ($otcc as $item1) {
            $json_ottc_data = json_decode($item1->data, true);
            array_push($arrNumberRoom, $item1->number_room);
            $templateProcessorCertificate->cloneRow('peripheral_otcc#' . $a . '', count($json_ottc_data));
            $i = 1;
            foreach ($json_ottc_data as $json_item) {
                $templateProcessorCertificate->setValue('count_otcc#' . $a . '#' . $i . '', htmlspecialchars($i));
                $templateProcessorCertificate->setValue('peripheral_otcc#' . $a . '#' . $i . '', htmlspecialchars(Peripheral::getPeripheralName($json_item['peripheral'])));
                $templateProcessorCertificate->setValue('model_otcc#' . $a . '#' . $i . '', htmlspecialchars($json_item['model']));
                $templateProcessorCertificate->setValue('serial_number_otcc#' . $a . '#' . $i . '', htmlspecialchars($json_item['serial_number']));
                $i++;
            }
            $a++;
        }
        $templateProcessorCertificate->setValue('number_room', htmlspecialchars(implode(", ", $arrNumberRoom)));
//        $countRowSoftware = 0;
//        foreach ($otcc as $item1) {
//            $json_soft_data = json_decode($item1->data_software, true);
//            foreach ($json_soft_data as $json_item) {
//                if ($json_item['type_soft'] != 2) {
//                    $countRowSoftware++;
//                }
//            }
//        }
        foreach ($otcc as $item1) {
            if($item1->os!=null) {
                $json_os = json_decode($item1->os, true);
                foreach ($json_os as $json_item) {
                    $templateProcessorCertificate->setValue('name_os', htmlspecialchars($json_item['os_name']));
                    $templateProcessorCertificate->setValue('version_os', htmlspecialchars($json_item['os_version']));
                    $templateProcessorCertificate->setValue('build_os', htmlspecialchars($json_item['os_build']));
                }
            }
        }
//        $templateProcessorCertificate->cloneRow('type_soft', $countRowSoftware);
//        $s = 1;
//        foreach ($otcc as $item1) {
//            $json_soft_data = json_decode($item1->data_software, true);
//            foreach ($json_soft_data as $json_item) {
//                if ($json_item['type_soft'] != 2) {
//                    $templateProcessorCertificate->setValue('count_soft#' . $s . '', htmlspecialchars($s)); // Имя компа (ОТСС)
//                    $templateProcessorCertificate->setValue('type_soft#' . $s . '', htmlspecialchars(Otcc::$SOFT[$json_item['type_soft']]));
//                    $templateProcessorCertificate->setValue('name_soft#' . $s . '', htmlspecialchars($json_item['name']));
////                    $templateProcessorCertificate->setValue('serial_number_soft#' . $s . '', htmlspecialchars($json_item['serial_number']));
////                    $templateProcessorCertificate->setValue('name_comp_soft#' . $s . '', htmlspecialchars($item1->pc_name));
//                    $s++;
//                }
//            }
//        }

        $date = date_parse($model->date);
        $templateProcessorCertificate->setValue('date_public', htmlspecialchars("«" . $date['day'] . "» " . Schools::getMonth($date['month']) . " " . $date['year']));
        $year = $date['year'] + 3;
        $templateProcessorCertificate->setValue('date_public#1', htmlspecialchars("«" . $date['day'] . "» " . Schools::getMonth($date['month']) . " " . $year));
        $templateProcessorCertificate->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/certificate_" . Schools::translit($model->short_name_org) . ".docx");
    }

    public function fis_temp($model, $templateProcessor){
        $templateProcessor->setValue('name_org', htmlspecialchars($model->name_org)); //Наименование организации в родительном падеже
        $templateProcessor->setValue('short_name', htmlspecialchars($model->short_name_org)); //Наименование организации
        $templateProcessor->setValue('fio_org', htmlspecialchars(preg_replace('#(.*)\s+(.).*\s+(.).*#usi', '$2.$3. $1', $model->school_director))); // ФИО руководителя
        $templateProcessor->setValue('position_org', htmlspecialchars($model->position_director)); //Специальность руководителя
        $templateProcessor->setValue('fio_spec_org', htmlspecialchars(preg_replace('#(.*)\s+(.).*\s+(.).*#usi', '$2.$3. $1', $model->spec_fio))); // ФИО специалиста
        $templateProcessor->setValue('pos_spec_org', htmlspecialchars($model->spec)); //Должность специалиста
    }

    public function license_vipnet($id){
        $model = FisSchools::findOne($id);
        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/license_vipnet.docx");
        $templateProcessor->setValue('name_org', htmlspecialchars($model->name_org)); //Наименование организации
        $templateProcessor->setValue('address', htmlspecialchars($model->address)); //Адрес организации
        $templateProcessor->setValue('reg_number', htmlspecialchars($model->reg_number)); //Регистрационная информация
        $date = date_parse($model->date_certificate_vipnet);
        $templateProcessor->setValue('vipnet_date', htmlspecialchars(sprintf("%02d", $date['day']) . "." . sprintf("%02d", $date['month']) . "." . $date['year']));
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/license_vipnet_" . Schools::translit($model->short_name_org) . ".docx");
    }

    public function prikazy($id, $zipBool){
        $model = FisSchools::findOne($id);
        if (!is_dir($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/prikazy")) {
            mkdir($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/prikazy", 0777, true);
        }
        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/prikaz_o_vvode_deistvie_complect_ORD.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/prikazy/prikaz_o_vvode_deistvie_complect_ORD_" . Schools::translit($model->short_name_org) . ".docx");

        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/prikaz_o_naznachenii_admin_security.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/prikazy/prikaz_o_naznachenii_admin_security_" . Schools::translit($model->short_name_org) . ".docx");

        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/prikaz_o_naznachenii_otvetstvennogo_user_SKZI.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/prikazy/prikaz_o_naznachenii_otvetstvennogo_user_SKZI_" . Schools::translit($model->short_name_org) . ".docx");

        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/prikaz_o_naznachenii_otvetstvennogo_za_obrabotky_PDn.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/prikazy/prikaz_o_naznachenii_otvetstvennogo_za_obrabotky_PDn_" . Schools::translit($model->short_name_org) . ".docx");

        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/prikaz_o_predostavlenii_dostupa_k_resursam_ISPDn.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/prikazy/prikaz_o_predostavlenii_dostupa_k_resursam_ISPDn_" . Schools::translit($model->short_name_org) . ".docx");

        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/prikaz_o_sozdanii_komissii.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/prikazy/prikaz_o_sozdanii_komissii_" . Schools::translit($model->short_name_org) . ".docx");

        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/prikaz_ob_ytverjdenii_control_zone.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $templateProcessor->setValue('address', htmlspecialchars($model->address));
        $arrNumberRoom = array();
        $otcc = FisOtcc::getOtcc($id);
        foreach ($otcc as $item1) {
            array_push($arrNumberRoom, $item1->number_room);
        }
        $templateProcessor->setValue('number_room', htmlspecialchars(implode(", ", $arrNumberRoom)));
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/prikazy/prikaz_ob_ytverjdenii_control_zone_" . Schools::translit($model->short_name_org) . ".docx");

        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/prikaz_o_vvode_ISPDn.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/prikazy/prikaz_o_vvode_ISPDn_" . Schools::translit($model->short_name_org) . ".docx");

        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/prikaz_ob_ytverjdenii_matrisu_dostupa.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/prikazy/prikaz_ob_ytverjdenii_matrisu_dostupa_" . Schools::translit($model->short_name_org) . ".docx");

        if($zipBool) {
            $rootPath = realpath($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/prikazy");
            $zip = new ZipArchive();
            $zip->open($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/prikazy.zip", ZipArchive::CREATE | ZipArchive::OVERWRITE);

            // Create recursive directory iterator
            /** @var SplFileInfo[] $files */
            $files = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($rootPath),
                RecursiveIteratorIterator::LEAVES_ONLY
            );

            foreach ($files as $name => $file) {
                // Skip directories (they would be added automatically)
                if (!$file->isDir()) {
                    // Get real and relative path for current file
                    $filePath = $file->getRealPath();
                    $relativePath = substr($filePath, strlen($rootPath) + 1);

                    // Add current file to archive
                    $zip->addFile($filePath, $relativePath);
                }
            }

            // Zip archive will be created only after closing object
            $zip->close();
        } else {
            if (is_file($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/prikazy.zip")) {
                unlink($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/prikazy.zip");
            }
        }
    }

    public function other_docs($id, $zipBool){
        $model = FisSchools::findOne($id);
        $otcc = FisOtcc::getOtcc($id);
        if (!is_dir($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/other_docs")) {
            mkdir($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/other_docs", 0777, true);
        }
        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/akt_opredeleniya_yz_PDn.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/other_docs/akt_opredeleniya_yz_PDn_" . Schools::translit($model->short_name_org) . ".docx");

        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/rule_obrabotki_person_data.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $templateProcessor->setValue('address', htmlspecialchars($model->address));
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/other_docs/rule_obrabotki_person_data_" . Schools::translit($model->short_name_org) . ".docx");

        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/rule_osyshestvlenie_vnutrenne_control_obrabotki_person_data.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/other_docs/rule_osyshestvlenie_vnutrenne_control_obrabotki_person_data_" . Schools::translit($model->short_name_org) . ".docx");

        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/rule_rassmotrenie_zaprosov_sybektov_person_data.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $templateProcessor->setValue('address', htmlspecialchars($model->address));
        $arrNumberRoom = array();
        foreach ($otcc as $item1) {
            array_push($arrNumberRoom, $item1->number_room);
        }
        $templateProcessor->setValue('number_room', htmlspecialchars(implode(", ", $arrNumberRoom)));
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/other_docs/rule_rassmotrenie_zaprosov_sybektov_person_data_" . Schools::translit($model->short_name_org) . ".docx");

        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/techproccess_obrabotki_and_security_PDn.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/other_docs/techproccess_obrabotki_and_security_PDn_" . Schools::translit($model->short_name_org) . ".docx");

        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/polozhenie_po_ispolzovaniyu_SKZI.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/other_docs/polozhenie_po_ispolzovaniyu_SKZI_" . Schools::translit($model->short_name_org) . ".docx");

        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/politika_v_otnoshenii_obrabotki_person_data.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/other_docs/politika_v_otnoshenii_obrabotki_person_data_" . Schools::translit($model->short_name_org) . ".docx");

        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/PIM.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $arrNumberRoom = array();
        foreach ($otcc as $item1) {
            array_push($arrNumberRoom, $item1->number_room);
        }
        $templateProcessor->setValue('number_room', htmlspecialchars(implode(", ", $arrNumberRoom)));
        $templateProcessor->setValue('address', htmlspecialchars($model->address));
        $templateProcessor->setValue('ekz', htmlspecialchars(1));
        $date = date_parse($model->date);
        $templateProcessor->setValue('date_public', htmlspecialchars("«" . $date['day'] . "» " . Schools::getMonth($date['month']) . " " . $date['year']));
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/other_docs/PIM_1_" . Schools::translit($model->short_name_org) . ".docx");

        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/PIM.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $arrNumberRoom = array();
        foreach ($otcc as $item1) {
            array_push($arrNumberRoom, $item1->number_room);
        }
        $templateProcessor->setValue('number_room', htmlspecialchars(implode(", ", $arrNumberRoom)));
        $templateProcessor->setValue('address', htmlspecialchars($model->address));
        $templateProcessor->setValue('ekz', htmlspecialchars(2));
        $date = date_parse($model->date);
        $templateProcessor->setValue('date_public', htmlspecialchars("«" . $date['day'] . "» " . Schools::getMonth($date['month']) . " " . $date['year']));
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/other_docs/PIM_2_" . Schools::translit($model->short_name_org) . ".docx");

        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/model_ugroz.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/other_docs/model_ugroz_" . Schools::translit($model->short_name_org) . ".docx");


        if($zipBool) {
            $rootPath = realpath($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/other_docs");
            $zip = new ZipArchive();
            $zip->open($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/other_docs.zip", ZipArchive::CREATE | ZipArchive::OVERWRITE);

            // Create recursive directory iterator
            /** @var SplFileInfo[] $files */
            $files = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($rootPath),
                RecursiveIteratorIterator::LEAVES_ONLY
            );

            foreach ($files as $name => $file) {
                // Skip directories (they would be added automatically)
                if (!$file->isDir()) {
                    // Get real and relative path for current file
                    $filePath = $file->getRealPath();
                    $relativePath = substr($filePath, strlen($rootPath) + 1);

                    // Add current file to archive
                    $zip->addFile($filePath, $relativePath);
                }
            }

            // Zip archive will be created only after closing object
            $zip->close();
        } else {
            if (is_file($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/other_docs.zip")) {
                unlink($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/other_docs.zip");
            }
        }
    }

    public function manuals($id, $zipBool){
        $model = FisSchools::findOne($id);
        if (!is_dir($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/manuals")) {
            mkdir($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/manuals", 0777, true);
        }
        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/manual_admin_security.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/manuals/manual_admin_security_" . Schools::translit($model->short_name_org) . ".docx");

        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/manual_id_and_auth.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/manuals/manual_id_and_auth_" . Schools::translit($model->short_name_org) . ".docx");

        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/manual_o_poryadke_vzaimodeystviya_s_roskomnadzor.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/manuals/manual_o_poryadke_vzaimodeystviya_s_roskomnadzor_" . Schools::translit($model->short_name_org) . ".docx");

        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/manual_otvetstvennogo_user_SKZI.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/manuals/manual_otvetstvennogo_user_SKZI_" . Schools::translit($model->short_name_org) . ".docx");

        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/manual_otvetstvennogo_za_organizaciyu_obrabotki_person_data.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/manuals/manual_otvetstvennogo_za_organizaciyu_obrabotki_person_data_" . Schools::translit($model->short_name_org) . ".docx");

        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/manual_po_antivirus_security.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/manuals/manual_po_antivirus_security_" . Schools::translit($model->short_name_org) . ".docx");

        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/manual_po_control_security_PDn.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/manuals/manual_po_control_security_PDn_" . Schools::translit($model->short_name_org) . ".docx");

        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/manual_po_security_tech_sredstv.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/manuals/manual_po_security_tech_sredstv_" . Schools::translit($model->short_name_org) . ".docx");

        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/manual_po_vvody_ORD.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/manuals/manual_po_vvody_ORD_" . Schools::translit($model->short_name_org) . ".docx");

        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/manual_security_machine_nositeley_information.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/manuals/manual_security_machine_nositeley_information_" . Schools::translit($model->short_name_org) . ".docx");

        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/manual_upravlenie_dostupom.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/manuals/manual_upravlenie_dostupom_" . Schools::translit($model->short_name_org) . ".docx");

        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/manual_user.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/manuals/manual_user_" . Schools::translit($model->short_name_org) . ".docx");

        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/manual_user_SKZI.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/manuals/manual_user_SKZI_" . Schools::translit($model->short_name_org) . ".docx");

        $templateProcessor = new TemplateProcessor($this->getDir() . "/documents/templates/fis/temp_fis/manual_ypravlenie_sobutiyami_IB.docx");
        FisSchools::fis_temp($model, $templateProcessor);
        $templateProcessor->saveAs($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/manuals/manual_ypravlenie_sobutiyami_IB_" . Schools::translit($model->short_name_org) . ".docx");
        if($zipBool) {
            $rootPath = realpath($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/manuals");
            $zip = new ZipArchive();
            $zip->open($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/manuals.zip", ZipArchive::CREATE | ZipArchive::OVERWRITE);

            // Create recursive directory iterator
            /** @var SplFileInfo[] $files */
            $files = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($rootPath),
                RecursiveIteratorIterator::LEAVES_ONLY
            );

            foreach ($files as $name => $file) {
                // Skip directories (they would be added automatically)
                if (!$file->isDir()) {
                    // Get real and relative path for current file
                    $filePath = $file->getRealPath();
                    $relativePath = substr($filePath, strlen($rootPath) + 1);

                    // Add current file to archive
                    $zip->addFile($filePath, $relativePath);
                }
            }

            // Zip archive will be created only after closing object
            $zip->close();
        } else {
            if (is_file($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/manuals.zip")) {
                unlink($this->getDir() . "/documents/fis/" . Schools::translit($model->short_name_org) . "/manuals.zip");
            }
        }
    }

    function getDir()
    {
        return $_SERVER["DOCUMENT_ROOT"];
    }
}
