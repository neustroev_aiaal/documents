<?php

namespace app\models\fis;

use app\models\Schools;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\fis\FisSchools;

/**
 * FisSchoolsSearch represents the model behind the search form of `app\models\schools\FisSchools`.
 */
class FisSchoolsSearch extends FisSchools
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'region_id', 'project_id', 'user_id'], 'integer'],
            [['name_org', 'name_org_rod', 'short_name_org', 'inn', 'school_director', 'position_director', 'address', 'number_protokol', 'number_techpassport', 'number_certificate', 'number_finale', 'spec', 'spec_fio'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FisSchools::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'region_id' => $this->region_id,
            'project_id' => $this->project_id,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'name_org', $this->name_org])
            ->orFilterWhere(['like', 'name_org', Schools::right_letter(strtolower($this->name_org))])
            ->andFilterWhere(['like', 'short_name_org', $this->short_name_org])
            ->orFilterWhere(['like', 'short_name_org', Schools::right_letter(strtolower($this->short_name_org))])
            ->andFilterWhere(['like', 'address', $this->address])
            ->orFilterWhere(['like', 'address', Schools::right_letter(strtolower($this->address))]);

        return $dataProvider;
    }
}
