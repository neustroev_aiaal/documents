<?php

namespace app\models\fis;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\fis\FisOtcc;

/**
 * FisOtccSearch represents the model behind the search form of `app\models\fis\FisOtcc`.
 */
class FisOtccSearch extends FisOtcc
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'org_id', 'pc_type', 'user_id'], 'integer'],
            [['pc_name', 'number_room', 'data_software', 'data', 'data_iis', 'ip_address', 'mac_address', 'os'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FisOtcc::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'org_id' => $this->org_id,
            'pc_type' => $this->pc_type,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'pc_name', $this->pc_name])
            ->andFilterWhere(['like', 'number_room', $this->number_room])
            ->andFilterWhere(['like', 'data_software', $this->data_software])
            ->andFilterWhere(['like', 'data', $this->data])
            ->andFilterWhere(['like', 'data_iis', $this->data_iis])
            ->andFilterWhere(['like', 'ip_address', $this->ip_address])
            ->andFilterWhere(['like', 'mac_address', $this->mac_address])
            ->andFilterWhere(['like', 'os', $this->os]);

        return $dataProvider;
    }
}
