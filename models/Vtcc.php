<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "doc_vtcc".
 *
 * @property string $id
 * @property int $org_id
 * @property resource $pc_name
 * @property resource $pc_type
 * @property resource $data
 * @property string $number_room
 * @property resource $data_software
 * @property int $user_id
 */
class Vtcc extends \yii\db\ActiveRecord
{

    const TYPE_PC = 0;
    const TYPE_NOTEBOOK = 1;
    const TYPE_CANDLESTICK = 2;

    public static $TYPES = [
        self::TYPE_PC => 'ПК',
        self::TYPE_NOTEBOOK => 'Ноутбук',
        self::TYPE_CANDLESTICK => 'Моноблок',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'doc_vtcc';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['org_id', 'user_id'], 'integer'],
            [['org_id', 'pc_name', 'pc_type', 'data', 'number_room', 'data_software'], 'required'],
            [['pc_name', 'pc_type', 'data', 'number_room'], 'string'],
            [['pc_name', 'number_room'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'org_id' => 'Организация',
            'pc_name' => 'Сетевое имя компьютера',
            'number_room' => 'Номер кабинета',
            'pc_type' => 'Тип АРМ',
            'data' => 'Данные',
            'data_software' => 'ПО',
            'user_id' => 'Пользователь'
        ];
    }

    public static function getVtcc(){
        return self::find()->all();
    }

    public function getSchools() {
        return $this->hasOne(Schools::className(), ['id' => 'org_id']);
    }

    public function getTypeName() {
        return static::$TYPES[$this->pc_type];
    }

    public static function getVtccOrg($org_id){
        return self::find()->where(['org_id'=>$org_id])->all();
    }
}
