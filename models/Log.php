<?php

namespace app\models;

use app\components\Resumable;
use app\components\Y;
use app\models\fis\FisOtcc;
use app\models\fis\FisSchools;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "doc_logs".
 *
 * @property integer $id
 * @property integer $type
 * @property string $info
 * @property integer $info_id
 * @property string $created
 * @property string $ip
 * @property string $user_agent
 */
class Log extends \yii\db\ActiveRecord {

    const TYPE_LOGIN = 0;
    const TYPE_LOGOUT = 1;
    const TYPE_USER = 2;
    const TYPE_SCHOOLS = 3;
    const TYPE_OTCC = 4;
    const TYPE_VTCC = 5;
    const TYPE_FIS_SCHOOLS = 6;
    const TYPE_FIS_OTCC = 7;
    const TYPE_GENERATE_RCOI = 8;
    const TYPE_GENERATE_FIS_FRDO = 9;

    public static $TYPES = [
        self::TYPE_LOGIN => 'Вход',
        self::TYPE_LOGOUT => 'Выход',
        self::TYPE_USER => 'Пользователи',
        self::TYPE_SCHOOLS => 'Школы РЦОИ',
        self::TYPE_FIS_SCHOOLS => 'Школы ФИС ФРДО',
        self::TYPE_OTCC => 'ОТСС РЦОИ',
        self::TYPE_FIS_OTCC => 'ОТСС ФИС ФРДО',
        self::TYPE_VTCC => 'ВТСС',
        self::TYPE_GENERATE_RCOI => 'Генерация документа РЦОИ',
        self::TYPE_GENERATE_FIS_FRDO => 'Генерация документа ФИС ФРДО',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'doc_logs';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['type', 'info_id'], 'integer'],
            [['ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'type' => 'Тип',
            'info' => 'Информация',
            'info_id' => 'Идентификатор',
            'created' => 'Добавлено',
            'ip' => 'IP',
            'user_agent' => 'User-Agent'
        ];
    }

    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        $s = DIRECTORY_SEPARATOR;
        $path = Yii::getAlias('@app') . $s . 'logs' . $s . date('Y-m-d') . '.txt';
        file_put_contents($path, date('[H:i:s]') . ' type-' . $this->type . ' info_id-' . $this->info_id . ' / ' . $this->info . ' \ ' . $this->ip . ' ' . $this->user_agent .  "\n", FILE_APPEND);
    }

    public function search($params) {
        $query = static::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ],
                'attributes' => [

                ]
            ],
        ]);

        // load the search form data and validate
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // adjust the query by adding the filters
        $query->andFilterWhere(['type' => $this->type]);
        $query->andFilterWhere(['info_id' => $this->info_id]);
        $query->andFilterWhere(['ip' => $this->ip]);

        return $dataProvider;
    }

    public function getTypeName() {
        return self::$TYPES[$this->type];
    }

    /**
     * @param User $identity
     * @param int $type
     */
    public static function addLogin($identity, $type = self::TYPE_LOGIN) {
        $log = new Log();
        $log->type = $type;
        $log->info = 'username: '.$identity->username;
        $log->info_id = $identity->id;
        $log->ip = Y::request()->userIP;
        $log->user_agent = Y::request()->userAgent;
        $log->save();
    }

    /**
     * @param User $user
     */

    public static function addUser(User $user, $new) {
        $log = new Log();
        $log->type = self::TYPE_USER;
        $log->info = 'user_id: ' . Y::user()->id . ' | username: ' . $user->username . ' | ' . ($new ? 'добавил' : 'изменил');
        $log->info_id = $user->id;
        $log->ip = Y::request()->userIP;
        $log->user_agent = Y::request()->userAgent;
        $log->save();
    }

    public static function addSchools(Schools $school, $new) {
        $user = Y::user();
        $log = new Log();
        $log->type = self::TYPE_SCHOOLS;
        $log->info = 'user_id: ' . $user->id . ' | username: ' . $user->username . ' | schools: ' . $school->short_name_org . ' | ' . ($new ? 'добавил' : 'изменил');
        $log->info_id = $school->id;
        $log->ip = Y::request()->userIP;
        $log->user_agent = Y::request()->userAgent;
        $log->save();
    }

    public static function addFisSchools(FisSchools $school, $new) {
        $user = Y::user();
        $log = new Log();
        $log->type = self::TYPE_FIS_SCHOOLS;
        $log->info = 'user_id: ' . $user->id . ' | username: ' . $user->username . ' | schools: ' . $school->short_name_org . ' | ' . ($new ? 'добавил' : 'изменил');
        $log->info_id = $school->id;
        $log->ip = Y::request()->userIP;
        $log->user_agent = Y::request()->userAgent;
        $log->save();
    }

    public static function addOtcc(Otcc $otcc, $new) {
        $user = Y::user();
        $log = new Log();
        $log->type = self::TYPE_OTCC;
        $log->info = 'user_id: ' . Y::user()->id . ' | username: ' . $user->username . ' | otcc: ' . $otcc->pc_name . ' | ' . ($new ? 'добавил' : 'изменил');
        $log->info_id = $otcc->id;
        $log->ip = Y::request()->userIP;
        $log->user_agent = Y::request()->userAgent;
        $log->save();
    }

    public static function addFisOtcc(FisOtcc $otcc, $new) {
        $user = Y::user();
        $log = new Log();
        $log->type = self::TYPE_FIS_OTCC;
        $log->info = 'user_id: ' . Y::user()->id . ' | username: ' . $user->username . ' | otcc: ' . $otcc->pc_name . ' | ' . ($new ? 'добавил' : 'изменил');
        $log->info_id = $otcc->id;
        $log->ip = Y::request()->userIP;
        $log->user_agent = Y::request()->userAgent;
        $log->save();
    }

    public static function addVtcc(Vtcc $vtcc, $new) {
        $user = Y::user();
        $log = new Log();
        $log->type = self::TYPE_VTCC;
        $log->info = 'user_id: ' . Y::user()->id . ' | username: ' . $user->username . ' | vtcc: ' . $vtcc->pc_name  . ' | ' . ($new ? 'добавил' : 'изменил');
        $log->info_id = $vtcc->id;
        $log->ip = Y::request()->userIP;
        $log->user_agent = Y::request()->userAgent;
        $log->save();
    }

    public static function generateRCOI(Schools $school, $msg) {
        $user = Y::user();
        $log = new Log();
        $log->type = self::TYPE_GENERATE_RCOI;
        $log->info = 'user_id: ' . Y::user()->id . ' | username: ' . $user->username . ' | schools: ' . $school->short_name_org  . ' | ' . $msg;
        $log->info_id = $school->id;
        $log->ip = Y::request()->userIP;
        $log->user_agent = Y::request()->userAgent;
        $log->save();
    }

    public static function generateFIS(FisSchools $school, $msg) {
        $user = Y::user();
        $log = new Log();
        $log->type = self::TYPE_GENERATE_FIS_FRDO;
        $log->info = 'user_id: ' . Y::user()->id . ' | username: ' . $user->username . ' | schools: ' . $school->short_name_org  . ' | ' . $msg;
        $log->info_id = $school->id;
        $log->ip = Y::request()->userIP;
        $log->user_agent = Y::request()->userAgent;
        $log->save();
    }

//    public static function addSettings($info) {
//        $user = Y::user();
//        $log = new Log();
//        $log->type = self::TYPE_SETTINGS;
//        $log->info = 'user_id: ' . Y::user()->id . ' | nick: ' . $user->nick . ' | ' . $info;
//        $log->ip = Y::request()->userIP;
//        $log->user_agent = Y::request()->userAgent;
//        $log->save();
//    }

}
