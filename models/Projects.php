<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "doc_projects".
 *
 * @property string $id
 * @property string $ministr_name
 * @property string $address_name
 * @property string $full_name
 * @property string $short_name
 * @property string $name_doc
 * @property string $date_doc
 * @property string $date_start_project
 * @property string $date_end_project
 */
class Projects extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'doc_projects';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ministr_name', 'address_name', 'full_name', 'short_name', 'name_doc', 'date_doc', 'date_start_project', 'date_end_project'], 'required'],
            [['date_doc', 'date_start_project', 'date_end_project'], 'safe'],
            [['ministr_name', 'address_name', 'full_name', 'short_name', 'name_doc'], 'string', 'max' => 255],
        ];
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->date_doc = date('Y-m-d', strtotime($this->date_doc));
            $this->date_start_project = date('Y-m-d', strtotime($this->date_start_project));
            $this->date_end_project = date('Y-m-d', strtotime($this->date_end_project));
            return true;
        }
        return false;
    }

    public function afterFind() {
        parent::afterFind();
        $this->date_doc = date('d.m.Y', strtotime($this->date_doc));
        $this->date_start_project = date('d.m.Y', strtotime($this->date_start_project));
        $this->date_end_project = date('d.m.Y', strtotime($this->date_end_project));
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ministr_name' => 'Заказчик',
            'address_name' => 'Адрес заказчика',
            'full_name' => 'Полное наименование системы',
            'short_name' => 'Краткое наименование системы',
            'name_doc' => 'Наименование документа',
            'date_doc' => 'Дата',
            'date_start_project' => 'Начало работ по созданию проекта',
            'date_end_project' => 'Плановый срок завершения работ',
        ];
    }

    public static function getList() {
        return static::find()->all();
    }
}
