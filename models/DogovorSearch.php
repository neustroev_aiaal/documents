<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Dogovor;

/**
 * DogovorSearch represents the model behind the search form of `app\models\Dogovor`.
 */
class DogovorSearch extends Dogovor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'position_id', 'inn', 'kpp', 'pc', 'kc', 'bik', 'dogovor_id'], 'integer'],
            [['date_public', 'full_name_org', 'fio', 'osnovanie', 'date_begin', 'date_end', 'short_name_org', 'address_org', 'bank', 'tel', 'email'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Dogovor::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_public' => $this->date_public,
            'position_id' => $this->position_id,
            'date_begin' => $this->date_begin,
            'date_end' => $this->date_end,
            'inn' => $this->inn,
            'kpp' => $this->kpp,
            'pc' => $this->pc,
            'kc' => $this->kc,
            'bik' => $this->bik,
            'dogovor_id' => $this->dogovor_id
        ]);

        $query->andFilterWhere(['like', 'full_name_org', $this->full_name_org])
            ->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'osnovanie', $this->osnovanie])
            ->andFilterWhere(['like', 'short_name_org', $this->short_name_org])
            ->andFilterWhere(['like', 'address_org', $this->address_org])
            ->andFilterWhere(['like', 'bank', $this->bank])
            ->andFilterWhere(['like', 'tel', $this->tel])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
