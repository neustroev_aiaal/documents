<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "doc_peripheral".
 *
 * @property string $id
 * @property string $name
 */
class Peripheral extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'doc_peripheral';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    public static function getList() {
        return static::find()->all();
    }

    public static function getPeripheralName($id) {
        return static::findOne($id)->name;
    }
}
