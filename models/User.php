<?php

namespace app\models;

use app\components\Y;
use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "{{%users}}".
 *
 * @property integer $id
 * @property integer $status
 * @property integer $force_logout
 * @property string $username
 * @property integer $role_code
 * @property string $hash
 * @property string $auth_key
 * @property string $created
 * @property integer $region_id
 */
class User extends ActiveRecord implements IdentityInterface
{
    const AUTH_KEY_LENGTH = 64;
    const PASSWORD_LENGTH_MAX = 32;
    const PASSWORD_LENGTH_MIN = 6;
    const LOGIN_DURATION = 30; //days

    const STATUS_DISABLED = 0;
    const STATUS_ACTIVE = 1;

    const ROLE_CODE_ADMIN = 1000;
    const ROLE_CODE_MANAGER = 500;
    const ROLE_CODE_OPERATOR = 300;
//    const ROLE_CODE_VIEWER = 200;
    const ROLE_CODE_USER = 100;
    const ROLE_ADMIN = 'admin';
    const ROLE_MANAGER = 'manager';
    const ROLE_OPERATOR = 'operator';
//    const ROLE_VIEWER = 'viewer';
    const ROLE_USER = 'user';

    public static $STATUSES = [
        self::STATUS_DISABLED => 'Выключен',
        self::STATUS_ACTIVE => 'Активен'
    ];

    public static $ROLES_NAMES = [
        self::ROLE_CODE_USER => 'Пользователь',
//        self::ROLE_CODE_VIEWER => 'Просмотр',
        self::ROLE_CODE_OPERATOR => 'Оператор',
        self::ROLE_CODE_MANAGER => 'Менеджер',
        self::ROLE_CODE_ADMIN => 'root'
    ];

    public static $ROLES = [
        self::ROLE_CODE_USER => self::ROLE_USER,
//        self::ROLE_CODE_VIEWER => self::ROLE_VIEWER,
        self::ROLE_CODE_OPERATOR => self::ROLE_OPERATOR,
        self::ROLE_CODE_MANAGER => self::ROLE_MANAGER,
        self::ROLE_CODE_ADMIN => self::ROLE_ADMIN
    ];

    public static $ROLES_CODES = [
        self::ROLE_USER => self::ROLE_CODE_USER,
//        self::ROLE_VIEWER => self::ROLE_CODE_VIEWER,
        self::ROLE_OPERATOR => self::ROLE_CODE_OPERATOR,
        self::ROLE_MANAGER => self::ROLE_CODE_MANAGER,
        self::ROLE_ADMIN => self::ROLE_CODE_ADMIN
    ];

    public static $MANAGER_HANDLED_ROLES = [
//        self::ROLE_CODE_VIEWER => self::ROLE_VIEWER,
        self::ROLE_CODE_OPERATOR => self::ROLE_OPERATOR
    ];

    public $password;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%users}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['username', 'password'], 'trim'],
            [['username', 'status'], 'required'],
            [['region_id'], 'integer'],
            [['password'], 'required', 'on' => ['create']],
            [['role_code'], 'in', 'range' => self::$ROLES_CODES],
            [['password'], 'string', 'length' => [self::PASSWORD_LENGTH_MIN, self::PASSWORD_LENGTH_MAX]],
            [['username', 'note'], 'string', 'max' => 255],
            [['username'], 'unique'],
            [['role_code'], 'roleValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'username' => 'Имя',
            'created' => 'Добавлен',
            'password' => 'Пароль',
            'role_code' => 'Роль',
            'status' => 'Статус',
            'org_id' => 'Организация',
            'region_id' => 'Регион',
            'note' => 'Примечание',
        ];
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->auth_key = \Yii::$app->security->generateRandomString(static::AUTH_KEY_LENGTH);
            }
            if (!empty($this->password)) {
                $this->hash = Yii::$app->getSecurity()->generatePasswordHash($this->password);
                $this->auth_key = \Yii::$app->security->generateRandomString(static::AUTH_KEY_LENGTH);
                if (!$this->isNewRecord) {
                    $this->force_logout = 1;
                }
            }
            if (!$this->status) {
                $this->force_logout = 1;
            }
            //            $this->role_code = $this->getRoleCodeByOrg();
            return true;
        }
        return false;
    }

    public function roleValidator() {
        if (!Y::user()->isAdmin()) {
            if (Y::user()->id == $this->id && $this->isManager()) {
                //allow be manager
            } elseif (!in_array($this->role_code, array_keys(self::$MANAGER_HANDLED_ROLES))) {
                $this->addError('role_code', 'Неверная роль.');
            }
        }
    }

    public static function changeRoles($orgId, $roleCode) {
        Yii::$app->db->createCommand()
            ->update(static::tableName(),
                ['role_code' => $roleCode],
                'org_id = :orgId', [':orgId' => $orgId])
            ->execute();
    }

    //    private function getRoleCodeByOrg() {
    //        return Organization::$TYPE_USER_ROLE[$this->organization->type];
    //    }

    /**
     * @param $id
     * @return self
     */
    public static function getById($id) {
        return static::findOne($id);
    }

    public static function getByName($name) {
        return static::findOne(['username' => $name]);
    }

    public function getRoleName() {
        return static::$ROLES_NAMES[$this->role_code];
    }

    public function getStatusName() {
        return static::$STATUSES[$this->status];
    }

    public function forceLogout($remove = false) {
        $this->force_logout = ($remove ? 0 : 1);
        $this->update(['force_logout']);
    }

    //    /**
    //     * @return Organization
    //     */
    //    public function getOrganization() {
    //        return $this->hasOne(Organization::className(), ['id' => 'org_id']);
    //    }

    public static function canBeEdited(User $user) {
        return $user->isAdminOrManager();
    }

    public static function canBeEditedOperator(User $user) {
        return $user->isAdminOrManagerOrOperator();
    }

    public function isAdmin() {
        return $this->role_code == self::ROLE_CODE_ADMIN;
    }

    public function isManager() {
        return $this->role_code == self::ROLE_CODE_MANAGER;
    }

    public function isOperator() {
        return $this->role_code == self::ROLE_CODE_OPERATOR;
    }

    public function isViewer() {
        return $this->role_code == self::ROLE_CODE_VIEWER;
    }

    public function isUser() {
        return $this->role_code == self::ROLE_CODE_USER;
    }

    public function isManagerOrUser() {
        return $this->role_code <= self::ROLE_CODE_MANAGER
            && $this->role_code >= self::ROLE_CODE_USER;
    }

    public function isAdminOrManager() {
        return $this->role_code >= self::ROLE_CODE_MANAGER;
    }

    public function isAdminOrManagerOrOperator() {
        return $this->role_code >= self::ROLE_CODE_OPERATOR;
    }

    public function canEdit(User $user) {
        $result = false;
        if ($this->isAdmin()) {
            $result = true;
        } elseif ($this->isManager()) {
            $result = in_array($user->role_code, array_keys(self::$MANAGER_HANDLED_ROLES))
                || $this->id == $user->id;
        }
        return $result;
    }

    /**
     * Finds an identity by the given ID.
     * @param int $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id) {
        return static::findOne($id);
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        //        return static::findOne(['access_token' => $token]);
        //        return static::findOne(['hash' => $token]);
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return int an ID that uniquely identifies a user identity.
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey() {
        return $this->auth_key;
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return bool whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    public function validatePassword($password) {
        return Yii::$app->getSecurity()->validatePassword($password, $this->hash);
    }

    public function findByUsername($username){
        return User::find()->where(['username'=>$username])->one();
    }

}
