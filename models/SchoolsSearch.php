<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Schools;

/**
 * SchoolsSearch represents the model behind the search form of `app\models\Schools`.
 */
class SchoolsSearch extends Schools
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'spec_nord_id', 'region_id', 'inn'], 'integer'],
            [['name_org','short_name_org', 'address', 'school_director', 'number_protokol', 'number_techpassport', 'number_certificate', 'number_finale', 'spec', 'spec_fio'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Schools::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'inn' => $this->inn,
            'region_id' => $this->region_id,
        ]);
        $query->andFilterWhere(['like', 'name_org', $this->name_org])
            ->orFilterWhere(['like', 'name_org', Schools::right_letter(strtolower($this->name_org))])
            ->andFilterWhere(['like', 'short_name_org', $this->short_name_org])
            ->orFilterWhere(['like', 'short_name_org', Schools::right_letter(strtolower($this->short_name_org))])
            ->andFilterWhere(['like', 'address', $this->address])
            ->orFilterWhere(['like', 'address', Schools::right_letter(strtolower($this->address))]);

//        if($dataProvider->totalCount == 0){
//            if($this->name_org != null){
//                var_dump(Schools::right_letter($this->name_org));
//                $query->andFilterWhere(['like', 'name_org', Schools::right_letter($this->name_org)]);
//            }
//            if($this->short_name_org != null){
//                $query->andFilterWhere(['like', 'short_name_org', Schools::right_letter($this->short_name_org)]);
//            }
//            if($this->address != null){
//                $query->andFilterWhere(['like', 'address', Schools::right_letter($this->address)]);
//            }
//        }
//        var_dump($dataProvider->totalCount);die;
        return $dataProvider;
    }
}
